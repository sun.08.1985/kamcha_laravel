<?php
use App\Test;
use App\Question;
    Breadcrumbs::register('main_admin', function ($breadcrumbs) {
        $breadcrumbs->push('Консоль', route('admin'));
    });

    Breadcrumbs::for('pages', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Страницы', route('pages.index'));
    });

    Breadcrumbs::for('page_add', function ($breadcrumbs) {
        $breadcrumbs->parent('pages');
        $breadcrumbs->push('Создание страницы', route('pages.create'));
    });

    Breadcrumbs::for('page_edit', function ($breadcrumbs, $page) {
        $breadcrumbs->parent('pages');
        $breadcrumbs->push($page->title, route('pages.edit', $page));
    });

    Breadcrumbs::for('news', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Новости', route('news.index'));
    });

    Breadcrumbs::for('new_add', function ($breadcrumbs) {
        $breadcrumbs->parent('news');
        $breadcrumbs->push('Создание новости', route('news.create'));
    });

    Breadcrumbs::for('new_edit', function ($breadcrumbs, $page) {
        $breadcrumbs->parent('news');
        $breadcrumbs->push($page->title, route('news.edit', $page));
    });

    Breadcrumbs::for('users', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Пользователи', url('/admin/users/'));
    });

    Breadcrumbs::for('user_add', function ($breadcrumbs) {
        $breadcrumbs->parent('users');
        $breadcrumbs->push('Создание пользователя', route('users.create'));
    });

    Breadcrumbs::for('user_edit', function ($breadcrumbs, $user) {
        $breadcrumbs->parent('users');
        $breadcrumbs->push($user->name, route('users.edit', $user));
    });

    Breadcrumbs::for('roles', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Роли', url('/admin/roles/'));
    });

    Breadcrumbs::for('role_add', function ($breadcrumbs) {
        $breadcrumbs->parent('roles');
        $breadcrumbs->push('Создание роли', route('roles.create'));
    });

    Breadcrumbs::for('role_edit', function ($breadcrumbs, $role) {
        $breadcrumbs->parent('roles');
        $breadcrumbs->push($role->name, route('roles.edit', $role));
    });

    Breadcrumbs::for('adresses', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Адреса', url('/admin/adresses/'));
    });

//    Breadcrumbs::for('adress_add', function ($breadcrumbs) {
//        $breadcrumbs->parent('adresses');
//        $breadcrumbs->push('Создание роли', route('adresses.create'));
//    });
//
//    Breadcrumbs::for('adress_edit', function ($breadcrumbs, $role) {
//        $breadcrumbs->parent('adresses');
//        $breadcrumbs->push($role->name, route('adresses.edit', $role));
//    });

    Breadcrumbs::for('topics', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Предметы', url('/admin/topics/'));
    });

    Breadcrumbs::for('topic_add', function ($breadcrumbs) {
        $breadcrumbs->parent('topics');
        $breadcrumbs->push('Создание Предмета', route('topics.create'));
    });

    Breadcrumbs::for('topic_edit', function ($breadcrumbs, $topic) {
        $breadcrumbs->parent('topics');
        $breadcrumbs->push($topic->name, route('topics.edit', $topic));
    });

    Breadcrumbs::for('tests', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Тесты', route('tests.index'));
    });

    Breadcrumbs::for('test_add', function ($breadcrumbs) {
        $breadcrumbs->parent('tests');
        $breadcrumbs->push('Создание', route('tests.create'));
    });

    Breadcrumbs::for('test_edit', function ($breadcrumbs, $test) {
        $breadcrumbs->parent('tests');
        $breadcrumbs->push($test->title, route('tests.edit', $test));
    });

    Breadcrumbs::for('test_view', function ($breadcrumbs, $test) {
        $breadcrumbs->parent('tests');
        $breadcrumbs->push($test->title, route('tests.show', $test));
    });

    Breadcrumbs::for('questions', function ($breadcrumbs, $question, $test) {
//        dd($test);
        $breadcrumbs->parent('test_edit', $test);
//        $breadcrumbs->push('Вопросы', route('questions.index'));
    });

    Breadcrumbs::for('question_add', function ($breadcrumbs, $question) {
//        $test = Test::find($question->test_id);
//        dd($question);
        $breadcrumbs->parent('questions', null, $question);
        $breadcrumbs->push('Создание вопроса', route('questions.create', $question));
    });

    Breadcrumbs::for('question_edit', function ($breadcrumbs, $question) {
        $test = Test::find($question->test_id);
        $breadcrumbs->parent('questions', $question, $test);
        $breadcrumbs->push($question->title, route('questions.edit', $question));
    });

    Breadcrumbs::for('question_view', function ($breadcrumbs, $question) {
        $breadcrumbs->parent('questions');
        $breadcrumbs->push($question->title, route('questions.show', $question));
    });

    Breadcrumbs::for('answers', function ($breadcrumbs, $answer, $test) {
        $test = Test::find($answer->test_id);
        $breadcrumbs->parent('question_edit', $answer, $test);
    });

    Breadcrumbs::for('answer_add', function ($breadcrumbs, $answer) {
        $breadcrumbs->parent('answers', $answer, null);
        $breadcrumbs->push('Создание ответа', route('answers.create', $answer));
    });

    Breadcrumbs::for('answer_edit', function ($breadcrumbs, $answer) {
        $question = Question::find($answer->question_id);
        $test = Test::find($question->test_id);
        $breadcrumbs->parent('answers', $question, $test);
        $breadcrumbs->push($answer->title, route('answers.edit', $answer));
    });

    Breadcrumbs::for('answer_view', function ($breadcrumbs, $answer) {
        $breadcrumbs->parent('answers');
        $breadcrumbs->push($answer->title, route('answers.show', $answer));
    });

    Breadcrumbs::for('polls', function ($breadcrumbs) {
        $breadcrumbs->parent('main_admin');
        $breadcrumbs->push('Опросы', route('poll.index'));
    });

    Breadcrumbs::for('poll_add', function ($breadcrumbs) {
        $breadcrumbs->parent('polls');
        $breadcrumbs->push('Создание опроса', route('poll.create'));
    });

    Breadcrumbs::for('poll_edit', function ($breadcrumbs, $poll) {
        $breadcrumbs->parent('polls');
//        dd($poll);
        $breadcrumbs->push($poll->question, route('poll.edit', $poll));
    });

    Breadcrumbs::for('poll_view', function ($breadcrumbs, $poll) {
        $breadcrumbs->parent('polls');
        $breadcrumbs->push($poll->question, route('poll.edit', $poll));
    });

    Breadcrumbs::for('poll_remove_option', function ($breadcrumbs, $poll) {
        $breadcrumbs->parent('poll_view', $poll);
        $breadcrumbs->push('Удалить вариант');//, route('poll.options.remove', $poll->id));
    });

    Breadcrumbs::for('poll_add_option', function ($breadcrumbs, $poll) {
        $breadcrumbs->parent('poll_view', $poll);
        $breadcrumbs->push('Добавить вариант');//, route('poll.options.push', $poll->id));
    });
