<?php

use Illuminate\Http\Request;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PagesController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/api_login', 'Auth\LoginController@login_api');
Route::post('/api_register', 'Auth\RegisterController@register_api');

//Route::apiResource('/news_api', 'NewsController')->middleware('auth:api');
//Route::apiResource('/pages', 'PagesController')->middleware('auth:api');
