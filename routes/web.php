<?php

//    use App\User;

//    use App\News;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//    Route::group(['middleware' => ['web']], function () {
        Auth::routes();
//    });
Route::get('/', 'WelcomeController@index')->name('home');
    Route::post('/feedback', 'WelcomeController@feedback')->name('feedback');
Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function (){

    Route::group(['namespace' => '\Inani\Larapoll\Http\Controllers'], function(){

        Route::middleware(["role:author,editor,admin,super_admin"])->group(function () {
            Route::get('/polls', ['uses' => 'PollManagerController@index', 'as' => 'poll.index']);
            Route::get('/polls/home', ['uses' => 'PollManagerController@home', 'as' => 'poll.home']);
            Route::get('/polls/create', ['uses' => 'PollManagerController@create', 'as' => 'poll.create']);
            Route::get('/polls/{poll}', ['uses' => 'PollManagerController@edit', 'as' => 'poll.edit']);
            Route::patch('/polls/{poll}', ['uses' => 'PollManagerController@update', 'as' => 'poll.update']);
            Route::delete('/polls/{poll}', ['uses' => 'PollManagerController@remove', 'as' => 'poll.remove']);
            Route::patch('/polls/{poll}/lock', ['uses' => 'PollManagerController@lock', 'as' => 'poll.lock']);
            Route::patch('/polls/{poll}/unlock', ['uses' => 'PollManagerController@unlock', 'as' => 'poll.unlock']);
            Route::post('/polls', ['uses' => 'PollManagerController@store', 'as' => 'poll.store']);
            Route::get('/polls/{poll}/options/add', ['uses' => 'OptionManagerController@push', 'as' => 'poll.options.push']);
            Route::post('/polls/{poll}/options/add', ['uses' => 'OptionManagerController@add', 'as' => 'poll.options.add']);
            Route::get('/polls/{poll}/options/remove', ['uses' => 'OptionManagerController@delete', 'as' => 'poll.options.remove']);
            Route::delete('/polls/{poll}/options/remove', ['uses' => 'OptionManagerController@remove', 'as' => 'poll.options.remove']);
        });

        Route::post('/vote/polls/{poll}', 'VoteManagerController@vote')->name('poll.vote');
    });


    Route::get('/', 'HomeController@index')->name('admin');
//    Route::get('/admin/chart', 'HomeController@chart')->name('admin.chart');
//    Route::get('/admin/users/{user}/reset', 'UsersController@resetscore')->name('admin.score.reset')->middleware('role:user,super_user,admin,super_admin');

//    Route::group(['middleware' => ['pay,role:super_admin']], function () {
//        Route::get('/admin/users/{user}/parent/add', 'UsersController@parentScoreAdd')->name('admin.score.add');
//        Route::get('/admin/users/{user}/parent/remove', 'UsersController@parentScoreRemove')->name('admin.score.remove');
//        Route::get('/admin/users/{user}/parent/score/{score}', 'UsersController@parentScoreToChild')->name('parent.score');
//    });

//    Route::put('/admin/users/{user}/assign', 'UsersController@assigned')->name('users.assigned')->middleware('role:super_user,author,editor,admin,super_admin');
//    Route::get('/admin/users/{user}/assign', 'UsersController@assign')->name('users.assign')->middleware('role:super_user,author,editor,admin,super_admin');

//    Route::put('/exams/{test}/assigned_pupil', 'UsersController@assigned_pupil')->name('users.assigned.pupil')->middleware('role:super_user,author,editor,admin,super_admin');
//    Route::get('/exams/{test}/assign_pupil', 'UsersController@assign_pupil')->name('users.assign.pupil')->middleware('role:super_user,author,editor,admin,super_admin');

    Route::resource('/pages', 'PagesController')->middleware('role:author,editor,admin,super_admin');
    Route::resource('/news', 'NewsController')->middleware('role:author,editor,admin,super_admin');

    Route::resource('/users', 'UsersController')->except('create,destroy')->middleware('role:author,editor,admin,super_admin');

    Route::get('/profile', 'UsersController@editCurrent')->name('profile.edit')->middleware('auth');
    Route::put('/profile', 'UsersController@updateCurrent')->name('profile.update')->middleware('auth');

    Route::resource('/adress', 'AdressController')->except('create,destroy')->middleware('role:author,editor,admin,super_admin');


    Route::get('/users/new', 'UsersController@create')->middleware('role:admin,super_admin');

    Route::delete('/users/{user}', 'UsersController@destroy')->name('users.destroy')->middleware('role:admin,super_admin');
    Route::resource('/roles', 'RolesController')->middleware('role:super_admin');
    Route::resource('/topics', 'TopicController')->middleware('role:admin,super_admin');

//    Route::resource('/admin/tests', 'TestsController')->middleware('role:super_user,author,editor,admin,super_admin');

//    Route::resource('/admin/questions', 'QuestionController')->except('index,create')->middleware('role:author,editor,admin,super_admin');
//    Route::get('/admin/questions/create/{test_id}', 'QuestionController@create')->name('questions.create')->middleware('role:author,editor,admin,super_admin');

//    Route::resource('/admin/answers', 'AnswerController')->except('index,create')->middleware('role:author,editor,admin,super_admin');
//    Route::get('/admin/answers/create/{question_id}', 'AnswerController@create')->name('answers.create')->middleware('role:author,editor,admin,super_admin');

//    Route::get('/testing/exams', 'TestStartController@list')->name('exams.list')->middleware('role:super_user,admin,super_admin');
//    Route::get('/testing/exams/result/{id}', function ($id) {
//        $test = \App\TestCompleted::find($id);
//        return view('tests.result', ['test' => $test]);
//    })->name('exams.result');//->middleware('role:super_user,admin,super_admin');
//    Route::post('/testing/exams/{id}/control', 'TestStartController@control')->name('exams.control')->middleware('role:super_user,admin,super_admin');//->middleware('avaible.test');
//    Route::get('/testing/exams/{id}/start', 'TestStartController@execute')->name('exams.start')->middleware('role:super_user,admin,super_admin');//->middleware('avaible.test');
});
//=======================================================
//Route::get('clear-cache', function() {
//
//    Artisan::call('cache:clear');
//    return "Кэш очищен!";
//
//});

//Route::get('lang/{locale}', function ($locale) {
//
//    if (in_array($locale, \Config::get('app.locales'))) {
//        Session::put('locale', $locale);
//    }
//
//    return redirect()->back();
//
//});
//    Route::get('contact', function () {
//        return view('contact');
//    })->name('contact');
//    ===========================================
Route::get('{slug}', 'PagesController@showBySlug')->name('pages.slug');
Route::get('news/{slug}', 'NewsController@showBySlug')->name('news.slug');
//Route::get('contact', function () {
//    return view('contact');
//})->name('contact');


//
//Route::get('url/generate-shorten-link', 'ShortLinkController@index')->middleware('role:admin,super_admin');
//Route::post('url/generate-shorten-link', 'ShortLinkController@store')->name('generate.shorten.link.post')->middleware('role:admin,super_admin');
//
//Route::get('url/{code}', 'ShortLinkController@shortenLink')->name('shorten.link')->middleware('role:user');
