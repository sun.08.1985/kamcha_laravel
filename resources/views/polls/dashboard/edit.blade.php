@extends('layouts.admin')
@section('title')
    {{ config('app.name', 'Laravel') }}
@endsection
@section('style')
    <style>
        .errors-list {
            list-style-type: none;
        }

        .clearfix {
            clear: both;
        }

        .create-btn {
            display: block;
            width: 16%;
            float: right;
        }

        .old_options,
        #options,
        .button-add {
            list-style-type: none;
        }

        .add-input {
            width: 80%;
            display: inline-block;
            margin-right: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('content')
    <section class="admin-panel-content">

        {!! Form::open(['route' => ['poll.update', $poll->id], 'method' => 'PATCH']) !!}
        <div class="admin-panel-section admin-panel-new-quiz">
            <div class="admin-panel-section__heading">
                <h3>Редактирование опроса</h3>
                <a href="#" class="admin-panel__prorogue"> </a>
                <button type="submit" class="admin-panel__publish btn btn--blue">Опубликовать</button>
            </div>
            <div class="admin-panel-section__content admin-panel-new-quiz__area">
                <div class="input-module admin-panel-new-quiz__area-title">
                    <div class="input-module__inner">
                        <label for="in1">Вопрос</label>
                        <div class="input-module__inner-area"><input type="text" class="input" id="question" name="question" value="{{ $poll->question }}"></div>
                        <span></span>
                    </div>
                </div>

                <div class="new-quiz-list">
                    <!-- опросник с чеками-->
                    <div class="new-quiz">
                        <div class="new-quiz__question">
                            <div class="input-module">
                                <div class="input-module__inner">
                                    <label for="in1">Варианты</label>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        @foreach($poll->options as $option)
                            <div class="new-quiz__variant">
                                <a href="#" class="new-quiz__variant-more">
                                    <svg class="icon icon-more">
                                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>
                                    </svg>
                                </a>
                                {{--                            <div class="new-quiz__variant-preview">--}}
                                {{--                                <input type="checkbox">--}}
                                {{--                            </div>--}}
                                <div class="new-quiz__variant-content">
                                    <div class="input-module">
                                        <div class="input-module__inner">
                                            <div class="input-module__inner-area"><input value="{{ $option->name }}" type="text" name="options[]" type="text" class="input"></div>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="new-quiz__variant-delete" onclick='remove(this)'>
                                    <svg class="icon icon-cross">
                                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-cross"></use>
                                    </svg>
                                </a>
                            </div>
                        @endforeach


                    </div>
                    <a href="#" class="new-quiz__variant-add" id="add">
                        Добавить вариант
                    </a>
                    <br><br>



                    <div class="new-quiz">
                        <div class="new-quiz__question">
                            <div class="input-module">
                                <div class="input-module__inner">
                                    <label for="in1">Опции</label>
                                    {{--                                    <div class="input-module__inner-area"><input id="option_1" type="text" name="options[0]" class="input"></div>--}}
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="new-quiz__variant">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="starts_at">Начало:</label>
                                    <input type="datetime-local" id="starts_at" name="starts_at" class="form-control" value="{{ old('starts_at', \Carbon\Carbon::parse($poll->starts_at)->format('Y-m-d\TH:i')) }}" />
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="starts_at">Остановка:</label>
                                    <input type="datetime-local" id="ends_at" name="ends_at" class="form-control" value="{{ old('ends_at', \Carbon\Carbon::parse($poll->ends_at)->format('Y-m-d\TH:i')) }}" />
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <label>
                                <input type="checkbox" name="canVisitorsVote" value="1" {{ old('canVisitorsVote', $poll->canVisitorsVote)  == 1 ? 'checked' : ''  }}> Поддержка гостей
                            </label>
                            <label>
                                <input type="checkbox" name="close" {{ old('close', $poll->isLocked()) ? 'checked':'' }}> Заблокировать
                            </label>
                            <label>
                                <input type="checkbox" name="canVoterSeeResult" {{ old('canVoterSeeResult', $poll->showResultsEnabled()) ? 'checked':'' }}> Показывать результаты
                            </label>
                        </div>
                    </div>
                    {{--                        <div class="new-quiz__variant">--}}
                    {{--                            <a href="#" class="new-quiz__variant-more">--}}
                    {{--                                <svg class="icon icon-more">--}}
                    {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
                    {{--                                </svg>--}}
                    {{--                            </a>--}}
                    {{--                            <div class="new-quiz__variant-preview">--}}
                    {{--                                <input type="checkbox">--}}
                    {{--                            </div>--}}
                    {{--                            <div class="new-quiz__variant-content">--}}
                    {{--                                <div class="input-module">--}}
                    {{--                                    <div class="input-module__inner">--}}
                    {{--                                        <div class="input-module__inner-area"><input id="option_2" type="text" name="options[1]" class="input" placeholder="Добавить вариант"></div>--}}
                    {{--                                        <span></span>--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <a href="#" class="new-quiz__variant-add" id="add">--}}
                    {{--                                Добавить вариант «Свой ответ»--}}
                    {{--                            </a>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="new-quiz__manage">--}}
                    {{--                            <a href="#" class="new-quiz__manage-copy">--}}
                    {{--                                <svg class="icon icon-copy">--}}
                    {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-copy"></use>--}}
                    {{--                                </svg>--}}
                    {{--                            </a>--}}
                    {{--                            <a href="#" class="new-quiz__manage-delete">--}}
                    {{--                                <svg class="icon icon-trash">--}}
                    {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-trash"></use>--}}
                    {{--                                </svg>--}}
                    {{--                            </a>--}}
                    {{--                        </div>--}}
                </div>

            </div>
            {{--                <div class="admin-panel-new-quiz__area-add">--}}
            {{--                    <a href="#" class="btn btn--gray">Добавить вопрос</a>--}}
            {{--                </div>--}}
        </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection


@section('script')
    <script type="text/javascript">
        // re render requested options
            @if(old('options'))
            @foreach(array_slice(old('options'), 2) as $option)
        var e = document.createElement('li');
        e.innerHTML = "<input type='text' name='options[]' value='{{ $option }}' class='form-control add-input' placeholder='Укажите ваш вариант' /> <a class='btn btn-danger' href='#' onclick='remove(this)'><i class='fa fa-minus-circle' aria-hidden='true'></i></a>";
        document.getElementById("options").appendChild(e);
        @endforeach
        @endif

        function remove(current) {
            current.parentNode.remove()
        }
        document.getElementById("add").onclick = function() {
            var e = document.createElement('div');
            e.className = 'new-quiz__variant';


            e.innerHTML = "<a href=\"#\" class=\"new-quiz__variant-more\">\n" +
                "                                <svg class=\"icon icon-more\">\n" +
                "                                    <use xlink:href=\"/design/img/sprites/main-sprite.svg#icon-more\"></use>\n" +
                "                                </svg>\n" +
                "                            </a><div class=\"new-quiz__variant-content\">" +
                "            <div class=\"input-module\">\n" +
                "            <div class=\"input-module__inner\">\n" +
                "            <div class=\"input-module__inner-area\">" +
                "<input type='text' name='options[]' class='input' placeholder='' /> " +
                "</div>\n" +
                "            <span></span>\n" +
                "            </div>\n" +
                "</div>" +
                "            </div><a href=\"#\"  onclick=\'remove(this)\' class=\"new-quiz__variant-delete\">\n" +
                "                                <svg class=\"icon icon-cross\">\n" +
                "                                    <use xlink:href=\"/design/img/sprites/main-sprite.svg#icon-cross\"></use>\n" +
                "                                </svg>\n" +
                "                            </a>\n";
            document.getElementsByClassName("new-quiz")[0].appendChild(e);
        }
    </script>
@endsection
