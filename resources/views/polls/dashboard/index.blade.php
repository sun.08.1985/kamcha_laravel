@extends('layouts.admin')

@section('content')

    <section class="admin-panel-content">
        <form class="admin-panel-content__search">
            <input type="text" placeholder="Поиск...">
        </form>

        <div class="news-table">
            <div class="news-table__heading">
                <h3>Опросы</h3>
                <a href="{{ route('pages.create') }}" class="news-table__heading-new">
                    <svg class="icon icon-plus">
                        <use xlink:href="./img/sprites/main-sprite.svg#icon-plus"></use>
                    </svg>
                    <span>Новый</span>
                </a>
            </div>
            <div class="news-table-content">
                <table>
                    <thead>
                        <tr>
                            <td>Статус</td>
                            <td>Заголовок</td>
                            <td>Голосов</td>
    {{--                        <td>Для гостей</td>--}}
                            <td>Создан</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($polls as $poll)
                        <tr>
                            <td>
                                @if($poll->isLocked())
                                    Закрыт
                                @elseif($poll->isComingSoon())
                                    Скоро
                                @elseif($poll->isRunning())
                                    Запущен
                                @endif
                            </td>
                            <td>
                                <div class="example-inner" style="max-width: 300px;">
                                    <!-- аккордеон для админки -->
                                    <label class="admin-accordion">
                                        <input type="checkbox">
                                        <div class="admin-accordion__title">
                                            <span>{{ $poll->question }}</span>
                                        </div>
                                        <div class="admin-accordion__body">


                                            <hr>
                                            @foreach($poll->results()->inOrder() as $voting)

                                                            <div class="accordion-row">
                                                                <span>{{ $voting['option']->name }}</span><span>{{ $voting['votes'] }} ({{ intval(($voting['votes'] / (($poll->votes_count == 0 ) ? 1: $poll->votes_count)) * 100) }}%)</span>
                                                                <div class="accordion-row__line">
                                                                    <div class="line-inner" style="width: {{ intval(($voting['votes'] / (($poll->votes_count == 0 ) ? 1: $poll->votes_count)) * 100) }}%;"></div>
                                                                </div>
                                                            </div>


            {{--                                    {{ dd($voting) }}--}}
{{--                                                <div class="miniBar">--}}
{{--                                                    <div class="miniBarProgress" style="width: {{ ($voting['votes'] / (($poll->votes_count == 0 ) ? 1: $poll->votes_count)) * 100 }}%;">--}}
{{--            --}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                За вариант "{{ $voting['option']->name }}" проголосовало: {{ $voting['votes'] }} ({{ intval(($voting['votes'] / (($poll->votes_count == 0 ) ? 1: $poll->votes_count)) * 100) }}%)<br>--}}
                                            @endforeach
                                        </div>
                                    </label>
                                    <!-- аккордеон для админки -->
                                </div>
                            </td>
                            <td>{{ $poll->votes_count }}</td>
{{--                            <td>--}}
{{--                                {{ $poll->canVisitorsVote ? 'Да' : 'Нет' }}--}}
{{--                            </td>--}}
                            <td>{{ $poll->created_at }}</td>
{{--                            <td>--}}
{{--                                {{ $polls->links() }}--}}
{{--                            </td>--}}
                            <td>
                                <span class="news-table-more">
                                    <a href="#pixel" class="news-table-more__trigger">
                                        <svg class="icon icon-more">
                                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>
                                        </svg>
                                    </a>
                                    <span class="news-table-more__popup">
                                        <a href="{{ route('poll.edit', $poll->id) }}">Редактировать</a>
                                        @php $route = $poll->isLocked()? 'poll.unlock': 'poll.lock' @endphp
                                        @php $lock = $poll->isLocked()? 'Разблокировать': 'Заблокировать' @endphp
                                        <form class="lock" action="{{ route($route, $poll->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }}
                                            <a href="#" onclick="$('.lock').submit();">
                                                {{ $lock }}
                                            </a>
                                        </form>
        {{--                                <a href="#">Снять с публикации</a>--}}
                                    </span>
                                </span>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script src="/js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script>
    // Delete Confirmation
    $(".delete").on("submit", function() {
        return confirm("Удалить опрос?");
    });

    // Lock Confirmation
    $(".lock").on("submit", function() {
        return confirm("Разблокировать\\заблокировать опрос??");
    });
</script>

    <style>
        .miniBarProgress {
            background-color: #8a898a;
            height: 100%;
            position: absolute;
            top: 0rem;
            left: 0rem;
        }
        .miniBar {
            height: 0.5rem;
            border: 1px solid #8a898a;
            position: relative;
            width: -webkit-calc(100% - 2rem);
            width: -moz-calc(100% - 2rem);
            width: calc(100% - 2rem);
            margin-right: 0.5rem;
        }
    </style>

@endsection
