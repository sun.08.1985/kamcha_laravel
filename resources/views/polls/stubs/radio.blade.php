<form method="POST" action="{{ route('poll.vote', $id) }}" >
    @csrf
        <div class="mobile-poll__category aside-quiz__slide-category">Опрос</div>
        <div class="mobile-poll__title aside-quiz__slide-title">{{ $question }}</div>
        <div class="mobile-poll__descr aside-quiz__slide-descr">
            @foreach($options as $id => $name)
                <label for="options">
                    <input value="{{ $id }}" type="radio" name="options" style="appearance: auto; -webkit-appearance: radio; -moz-appearance: radio">
                    {{ $name }}
                </label><br>
            @endforeach
        </div>
        <div class="aside-quiz__slide-go">
            <input type="submit" class="mobile-poll__btn btn btn--white" value="Проголосовать">
        </div>
</form>
