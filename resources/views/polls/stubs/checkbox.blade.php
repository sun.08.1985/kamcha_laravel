<form method="POST" action="{{ route('poll.vote', $id) }}" >
    @csrf
    <div class="aside-quiz__slide-category">Опрос</div>
    <div class="aside-quiz__slide-title">{{ $question }}</div>
    <div class="aside-quiz__slide-descr">
        @foreach($options as $id => $name)
            <label for="options">
                <input value="{{ $id }}" type="checkbox" name="options[]" style="appearance: auto; -webkit-appearance: checkbox; -moz-appearance: checkbox">
                {{ $name }}
            </label><br>
        @endforeach
    </div>
    <div class="aside-quiz__slide-go">
        <input type="submit" class="btn btn--white" value="Проголосовать">
    </div>
</form>


{{--<form method="POST" action="{{ route('poll.vote', $id) }}" >--}}
{{--    @csrf--}}
{{--    <div class="panel panel-primary">--}}
{{--        <div class="panel-heading">--}}
{{--            <h3 class="panel-title">--}}
{{--                <span class="glyphicon glyphicon-arrow-right"></span> {{ $question }}--}}
{{--            </h3>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="panel-body">--}}
{{--        <ul class="list-group">--}}
{{--            @foreach($options as $id => $name)--}}
{{--                <li class="list-group-item">--}}
{{--                    <div class="radio">--}}
{{--                        <label>--}}
{{--                            <input value="{{ $id }}" type="checkbox" name="options[]">--}}
{{--                            {{ $name }}--}}
{{--                        </label>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--    <div class="panel-footer">--}}
{{--        <input type="submit" class="btn btn-primary btn" value="Проголосовать" />--}}
{{--    </div>--}}
{{--</form>--}}
