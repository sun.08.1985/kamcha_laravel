@extends('layouts.design')

@section('menu_left')
    @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
        <a class="nav__link" href="/{{ $page->slug }}">{{ $page->title }}</a>
    @endforeach
@endsection

@section('content')
    <section class="contacts">
    <div class="contacts__title">
        <h1>Контакты</h1>
    </div>
    <div class="contacts-module">
        <div class="contacts-module__title">
            <h2>Экстренные службы</h2>
        </div>
        <div class="contacts-module-phone">
            <h4>Пожарная охрана</h4>
            <a href="tel:+101">101</a>
            <p class="lighter">Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. </p>
        </div>
        <div class="contacts-module-phone">
            <h4>Полиция</h4>
            <a href="tel:+102">102</a>
            <p class="lighter">Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. </p>
        </div>
        <div class="contacts-module-phone">
            <h4>Скорая помощь</h4>
            <a href="tel:+103">103</a>
            <p class="lighter">Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. </p>
        </div>
        <div class="contacts-module-phone">
            <h4>Аварийная служба газа</h4>
            <a href="tel:+104">104</a>
            <p class="lighter">Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. </p>
        </div>
        <div class="contacts-module-phone">
            <h4>Экстренная помощь (МЧС)</h4>
            <a href="tel:+112">112</a>
            <p class="lighter">Разнообразный и богатый опыт новая модель организационной деятельности требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. </p>
        </div>
    </div>
    <div class="contacts-module" style="display: none">
        <div class="contacts-module__title">
            <h2>Госучереждения</h2>
            <div class="switcher">
                <label for="switcherId">Карта</label>
                <input type="checkbox" id="switcherId">
                <label for="switcherId">Список</label>
            </div>
        </div>
        <div class="contacts-module-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d23260.79652515375!2d27.90781025!3d43.21788755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbg!4v1591284663934!5m2!1sen!2sbg" width="100%" height="100%" frameborder="0" style="border:0;"
                    allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
</section>
@endsection
