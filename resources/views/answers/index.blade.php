@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('answers') }}
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Название</td>
                                    <td>Количество ответов</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($answers as $answer)
                                <tr>
                                    <td>{{ $answer->id }}</td>
                                    <td>{{ $answer->title }}</td>
                                    <td>
{{--                                        <a class="btn btn-primary" href="{{ route('answers.show', $answer->id) }}"><i class="fa fa-eye"></i></a>--}}
                                        <a class="btn btn-success" href="{{ route('answers.edit', $answer->id) }}"><i class="fa fa-edit"></i></a>
                                        {!! Form::open(['route' => ['answers.destroy', $answer->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                        <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-remove"></i></button>
                                        {!! Form::close() !!}

{{--                                        <a href="{{ route('answers.destroy', $answer->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
