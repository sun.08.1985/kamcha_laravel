@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('answer_add', $question) }}
                        {!! $question->description !!}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'answers.store', 'method' => 'post']) !!}
                            <fieldset>
                            <dd>
                                <label for="title">Вариант ответа</label>


                                <div class="col-md-12">

                                    <div class="after-add-more">
                                        <div class="input-group control-group">
                                            <input type="text" name="title[]" class="form-control" placeholder="" value="">
                                            @if ($errors->has('title'))
                                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                                            @endif
                                            <label for="correct">Верный ответ
                                                <input class="form-control" type="radio" name="correct" value="0" checked>
                                            </label>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-success add-more" type="button">
                                                    <i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>

                                        <div role="button" data-toggle="collapse" data-target="#collapseDescription" class="card-header" aria-expanded="false" aria-controls="collapseDescription">
                                            Пояснение (опционально)
                                            <span class="pull-right"><i class="fa fa-caret-down"></i></span>
                                        </div>

                                        <div class="collapse card-body" id="collapseDescription">
                                            <textarea class="form-control" name="description[]" rows="10">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                                            @endif
                                        </div>

{{--                                        <label for="correct">Верный ответ <input class="form-control" name="correct[]" type="checkbox" {{ old('correct') }} value="correct"></label>--}}
                                    </div>


                                    <div class="copy-fields d-none">
                                        <div class="control-group input-group" style="margin-top:10px">
                                            <input type="text" name="title[]" class="form-control" placeholder="">
                                            @if ($errors->has('title'))
                                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                                            @endif
                                            <label for="correct">Верный ответ
                                                <input class="form-control copy" type="radio" name="correct">
                                            </label>
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-danger remove" type="button"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        <div role="button" data-toggle="collapse" data-target="#collapseDescription00" class="card-header" aria-expanded="false" aria-controls="collapseDescription">
                                            Пояснение (опционально)
                                            <span class="pull-right"><i class="fa fa-caret-down"></i></span>
                                        </div>

                                        <div class="collapse card-body" id="collapseDescription00">
                                            <textarea class="form-control" name="description[]" rows="10">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                                            @endif
                                        </div>

{{--                                        <label for="correct">Верный ответ <input class="form-control" name="correct[]" type="checkbox" {{ old('correct') }} value="correct"></label>--}}

                                    </div>

                                </div>

                            </dd>
                            <input type="hidden" class="form-control" name="question_id" value="{{ $question->id }}">
                            <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">

                            <button class="btn btn-success pull-right" type="submit">Сохранить</button>
                            </fieldset>
                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('script')
    <script src="/js/tests/answers.js"></script>
@stop
