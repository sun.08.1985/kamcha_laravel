@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('answer_view', $variable) }}
                    </div>
                    <div class="card-body">
                        {!! $variable->description !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
