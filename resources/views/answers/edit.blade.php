@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('answer_edit', $answer) }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => ['answers.update', $answer->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="title">Вариант ответа</label>
                                <input type="text" class="form-control" name="title" value="{{ $answer->title }}">
                                @if ($errors->has('title'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <input type="hidden" class="form-control" name="question_id" value="{{ $answer->question->id }}">
                            <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                            <dd>
                                <div role="button" data-toggle="collapse" data-target="#collapseDescription" class="card-header" aria-expanded="false" aria-controls="collapseDescription">
                                    Пояснение (опционально)
                                    <span class="pull-right"><i class="fa fa-caret-down"></i></span>
                                </div>
                                <div class="collapse card-body" id="collapseDescription">
                                    <textarea class="form-control" name="description" rows="10">{{ $answer->description }}</textarea>
                                </div>

                                @if ($errors->has('description'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="correct">Верный ответ</label>
                                <input name="correct" type="checkbox" {{ $answer->correct ? 'checked':'' }}>
                            </dd>
                            <button class="btn btn-warning" type="submit">Изменить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('script')
    <script src="/js/tests/answers.js"></script>
@stop
