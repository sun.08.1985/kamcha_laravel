@section('content')
{{--    {{ dd($options) }}--}}
<h3>{{ $question }}</h3>
    @foreach ($options as $option)
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: {{ $option->percent }}%" aria-valuenow="{{ $option->percent }}" aria-valuemin="0" aria-valuemax="100">{{ $option->name }}</div>
        </div>
    @endforeach

@endsection
