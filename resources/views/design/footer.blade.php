<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-lg-1 order-2">
                <nav class="footer__nav">
                    <ul>
                        <li><a href="/about" class="footer__nav_item">О проекте</a></li>
                        <li><a href="/feedback" class="footer__nav_item">Обратная связь</a></li>
                        <li><a href="/faq" class="footer__nav_item">Часто задаваемые вопросы</a></li>
                        <li><a href="/police" class="footer__nav_item">Политика конфиденциальности</a></li>
                    </ul>
                </nav>
                <p class="rights">© 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены. </p>
            </div>
            <div class="col-lg-3 order-lg-2 order-1">
                <div class="social">
                    <a href="#" class="social_link social_link_ok">
                        <img src="img/ok.svg" alt="">
                    </a>
                    <a href="#" class="social_link social_link_vk">
                        <img src="img/vk.svg" alt="">
                    </a>
                    <a href="#" class="social_link social_link_facebook">
                        <img src="img/facebook.svg" alt="">
                    </a>
                    <a href="#" class="social_link social_link_twitter">
                        <img src="img/twitter.svg" alt="">
                    </a>
                </div>

            </div>
        </div>
    </div>
</footer>

@include('design.modals')
