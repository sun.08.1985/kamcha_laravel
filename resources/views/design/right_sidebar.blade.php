<div class="col-md-4 col-lg-3 d-none d-md-block">
    <div class="sidebar-right">
        <div class="sidebar__poll">
            <div class="sidebar__poll_label">
                <span>Опрос</span>
            </div>

            <div class="sidebar__poll_slick">

                @if(Inani\Larapoll\Poll::count() >= 1)
                    @foreach(Inani\Larapoll\Poll::all() as $poll)

                        <div class="alert alert-primary">
                            {{ PollWriterRemod::draw(Inani\Larapoll\Poll::find($poll->id)) }}
                        </div>

                    @endforeach
                @else
                    Активных опросов нет
                @endif

            </div>

            <div class="sidebar__poll_nav">
                <a href="#" class="arrow sidebar__poll_nav_prev"><i class="icon-left-dir"></i></a>
                <p class="sidebar__poll_nav_text">Опрос <span class="sidebar__poll_nav_show">1</span> из <span class="sidebar__poll_nav_total">{{ \Inani\Larapoll\Poll::count() }}</span></p>
                <a href="#" class="arrow sidebar__poll_nav_next"><i class="icon-right-dir"></i></a>
            </div>
        </div>

        <div class="sidebar__discussions">
            <h3 class="sidebar__discussions_title">Активно обсуждают</h3>

            <a href="#" class="sidebar__discussions_item">
                <div class="discussions_item_user">
                    <div class="discussions_item_user_avatar">
                        <img src="img/user2.jpg" alt="">
                    </div>
                    <span class="discussions_item_user_name">Максим Малыгин</span>
                </div>
                <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
            </a>

            <a href="#" class="sidebar__discussions_item">
                <div class="discussions_item_user">
                    <div class="discussions_item_user_avatar">
                        <img src="img/user.jpg" alt="">
                    </div>
                    <span class="discussions_item_user_name">Клавдия Суворова</span>
                </div>
                <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
            </a>

            <a href="#" class="sidebar__discussions_item">
                <div class="discussions_item_user">
                    <div class="discussions_item_user_avatar">
                        <img src="img/user2.jpg" alt="">
                    </div>
                    <span class="discussions_item_user_name">Максим Малыгин</span>
                </div>
                <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
            </a>

            <a href="#" class="sidebar__discussions_item">
                <div class="discussions_item_user">
                    <div class="discussions_item_user_avatar">
                        <img src="img/user2.jpg" alt="">
                    </div>
                    <span class="discussions_item_user_name">Максим Малыгин</span>
                </div>
                <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
            </a>

            <a href="#" class="sidebar__discussions_item">
                <div class="discussions_item_user">
                    <div class="discussions_item_user_avatar">
                        <img src="img/user2.jpg" alt="">
                    </div>
                    <span class="discussions_item_user_name">Максим Малыгин</span>
                </div>
                <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
            </a>

        </div>
    </div>
</div>
