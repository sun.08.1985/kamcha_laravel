<div style="display: none;" id="modal_input" class="modal_input modal_form">
    <h2 class="modal_title">Войти</h2>
    <form action="{{ route('login') }}" method="post">
        @csrf
        <label for="modal_input_mail" class="">example@mail.com</label>
        <input id="modal_input_mail" type="text" name="email" value="" placeholder="example@mail.com" required="">

        <label for="modal_input_pas" class="">Пароль</label>
        <input id="modal_input_pas" class="modal_input_pas" type="password" name="password" value="" placeholder="•••••••••••••" required="">
        <div class="modal_input_reestablish_box">
            @if (Route::has('password.request'))
                <a class="modal_input_reestablish" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif
            {{--            <a href="{{ route('re') }}" class="modal_input_reestablish">Не помню пароль</a>--}}
        </div>

        <button href="#" type="submit" class="btn_bg btn_modal">Войти</button>
        <a data-fancybox data-src="#modal_registration" href="javascript:;" class="btn_bg btn_modal">Зарегистрироваться </a>
{{--        <a href="{{ route('register') }}" class="btn_bg btn_modal">Зарегистрироваться</a>--}}

        {{--        <p class="modal_input_label">Или войдите с помощью</p>--}}
        {{--        <div class="modal_input_social">--}}
        {{--            <a href="#" class="modal_input_google modal_input_social_btn">--}}
        {{--                <img src="img/google.svg" alt="">--}}
        {{--            </a>--}}

        {{--            <a href="#" class="modal_input_ok modal_input_social_btn">--}}
        {{--                <img src="img/ok.svg" alt="">--}}
        {{--            </a>--}}

        {{--            <a href="#" class="modal_input_vk modal_input_social_btn">--}}
        {{--                <img src="img/vk.svg" alt="">--}}
        {{--            </a>--}}

        {{--            <a href="#" class="modal_input_facebook modal_input_social_btn">--}}
        {{--                <img src="img/facebook.svg" alt="">--}}
        {{--            </a>--}}

        {{--            <a href="#" class="modal_input_twitter modal_input_social_btn">--}}
        {{--                <img src="img/twitter.svg" alt="">--}}
        {{--            </a>--}}
        {{--        </div>--}}
    </form>
</div>

<div style="display: none;" id="modal_registration" class="modal_registration modal_form">
    <h2 class="modal_title">Регистрация</h2>
    <form action="{{ route('register') }}" method="post" id="form_reg">
        <input type="hidden" name="district" data-kladr-id="4100900000000">
        <input type="hidden" name="zip">
        <input type="hidden" name="region" data-kladr-id="4100000000000">

        <label for="modal_registration_name" class="">Имя</label>
        <input id="modal_registration_name" type="text" name="name" value="" placeholder="Александр" required="">

        <label for="modal_registration_phone" class="">Фамилия</label>
        <input id="modal_registration_phone" type="text" name="phone" value="" placeholder="+79999999999" required="">

        <label for="modal_registration_mail" class="">Email</label>
        <input id="modal_registration_mail" type="email" name="email" value="" placeholder="example@mail.com" required="">

        <div class="form-group">
        <label for="modal_registration_city" class="">Город</label>
        <input id="modal_registration_city" type="text" name="city" value="" placeholder="">
        </div>
        <div class="form-group">
        <label for="modal_registration_street" class="">Улица</label>
        <input id="modal_registration_street" type="text" name="street" value="" placeholder="">
        </div>
        <div class="form-group">
        <label for="modal_registration_building" class="">Дом</label>
        <input id="modal_registration_building" type="text" name="building" value="" placeholder="">
        </div>
        <label for="modal_registration_appartment" class="">Квартира</label>
        <input id="modal_registration_appartment" type="text" name="appartment" value="" placeholder="">

        <label for="modal_registration_pas" class="">Пароль</label>
        <div class="modal_registration_pas_box">
            <input id="modal_registration_pas" class="modal_registration_pas" type="password" name="password" value="" placeholder="•••••••••••••" required="">
            <a href="#" class="modal_registration_pas_eye">
                <img src="img/eye.svg" alt="">
            </a>
        </div>
        <label for="modal_registration_pas_conf" class="">Подтверждение пароля</label>
        <div class="modal_registration_pas_box">
            <input id="modal_registration_pas_conf" class="modal_registration_pas" type="password" name="password_confirmation" value="" placeholder="•••••••••••••" required="">
            <a href="#" class="modal_registration_pas_eye">
                <img src="img/eye.svg" alt="">
            </a>
        </div>
        <a data-fancybox data-src="#modal_registration" href="javascript:;" class="btn_bg btn_modal">Зарегистрироваться </a>
{{--        <a href="#" class="btn_bg btn_modal">Зарегистрироваться</a>--}}

        <p class="modal_input_label">Уже зарегистрированы?</p>

        <a class="btn_bg btn_modal" data-fancybox data-src="#modal_input" href="javascript:;" shref="{{ route('login') }}">Войти</a>
{{--        <a href="#" class="btn_bg btn_modal">Войти</a>--}}

        <p class="modal_input_confid">Регистрируясь, вы принимаете условия <a href="#">Политики конфиденциальности</a></p>

    </form>
</div>

<div style="display: none;" id="modal_notification" class="modal_notification modal_form">
    <h2 class="modal_title">Уведомления</h2>
    <div class="modal_notification_nav">
        <span class="modal_notification_nav_quan">12 новых</span>
        <a href="#" class="modal_notification_nav_read">Отметить все как прочитанные</a>
    </div>
    <div class="modal_notification_item">
        <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
        <a href="#" class="infolabel__box_close">
            <img src="img/close.svg" alt="" class="close_light">
            <img src="img/close_dark.svg" alt="" class="close_dark">
        </a>
    </div>
    <div class="modal_notification_item">
        <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
        <a href="#" class="infolabel__box_close">
            <img src="img/close.svg" alt="" class="close_light">
            <img src="img/close_dark.svg" alt="" class="close_dark">
        </a>
    </div>
    <div class="modal_notification_item">
        <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
        <a href="#" class="infolabel__box_close">
            <img src="img/close.svg" alt="" class="close_light">
            <img src="img/close_dark.svg" alt="" class="close_dark">
        </a>
    </div>
    <div class="modal_notification_item notification_read">
        <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
        <a href="#" class="infolabel__box_close">
            <img src="img/close.svg" alt="" class="close_light">
            <img src="img/close_dark.svg" alt="" class="close_dark">
        </a>
    </div>
    <div class="modal_notification_item notification_read">
        <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
        <a href="#" class="infolabel__box_close">
            <img src="img/close.svg" alt="" class="close_light">
            <img src="img/close_dark.svg" alt="" class="close_dark">
        </a>
    </div>

</div>

{{--<div style="display: none;" id="modal_burger" class="modal_burger modal_form">--}}
{{--    <a data-fancybox data-src="#modal_input" href="javascript:;" class="btn_bg btn_modal">Войти</a>--}}
{{--    <nav class="modal_burger_main_nav">--}}
{{--        <ul class="">--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Популярное</a></li>--}}
{{--            <li class=""><a class="modal_burger-nav_link" href="#">Свежее</a></li>--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Опросы</a></li>--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Рядом</a></li>--}}
{{--        </ul>--}}
{{--    </nav>--}}
{{--    <nav class="modal_burger_main_nav modal_footer_nav">--}}
{{--        <ul class="">--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Популярное</a></li>--}}
{{--            <li class=""><a class="modal_burger-nav_link" href="#">Свежее</a></li>--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Опросы</a></li>--}}
{{--            --}}{{--            <li class=""><a class="modal_burger-nav_link" href="#">Рядом</a></li>--}}
{{--        </ul>--}}
{{--    </nav>--}}
{{--    <div class="social">--}}
{{--        <a href="#" class="social_link social_link_ok">--}}
{{--            <img src="img/ok.svg" alt="">--}}
{{--        </a>--}}
{{--        <a href="#" class="social_link social_link_vk">--}}
{{--            <img src="img/vk.svg" alt="">--}}
{{--        </a>--}}
{{--        <a href="#" class="social_link social_link_facebook">--}}
{{--            <img src="img/facebook.svg" alt="">--}}
{{--        </a>--}}
{{--        <a href="#" class="social_link social_link_twitter">--}}
{{--            <img src="img/twitter.svg" alt="">--}}
{{--        </a>--}}
{{--    </div>--}}

{{--    <p class="rights">© 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены. </p>--}}

{{--</div>--}}


<link href="/css/jquery.fias.min.css" rel="stylesheet">
<script src="/js/jquery.fias.min.js" type="text/javascript"></script>
{{--    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
<script>
    // Form example
    (function () {
        var $container = $('#form_reg');//document.getElementById('form_reg'));

        var $tooltip = $('#tooltip');

        var $zip = $container.find('[name="zip"]'),
            $region = $container.find('[name="region"]'),
            $district = $container.find('[name="district"]'),
            $city = $container.find('[name="city"]'),
            $street = $container.find('[name="street"]'),
            $building = $container.find('[name="building"]');

        $()
            .add($region)
            .add($district)
            .add($city)
            .add($street)
            .add($building)
            .fias({
                parentInput: $container.find('.form-group'),
                verify: true,
                select: function (obj) {
                    if (obj.zip) $zip.val(obj.zip);//Обновляем поле zip
                    setLabel($(this), obj.type);
                    $tooltip.hide();
                },
                check: function (obj) {
                    var $input = $(this);

                    if (obj) {
                        setLabel($input, obj.type);
                        $tooltip.hide();
                    }
                    else {
                        showError($input, 'Ошибка');
                    }
                },
                checkBefore: function () {
                    var $input = $(this);

                    if (!$.trim($input.val())) {
                        $tooltip.hide();
                        return false;
                    }
                }
            });

        $region.fias('type', $.fias.type.region);
        $district.fias('type', $.fias.type.district);
        $city.fias('type', $.fias.type.city);
        $street.fias('type', $.fias.type.street);
        $building.fias('type', $.fias.type.building);

        $district.fias('withParents', true);
        $city.fias('withParents', true);
        $street.fias('withParents', true);

        // Отключаем проверку введённых данных для строений
        $building.fias('verify', false);

        // Подключаем плагин для почтового индекса
        $zip.fiasZip($container);

        function setLabel($input, text) {
            text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
            $input.parent().find('label').text(text);
        }

        function showError($input, message) {
            $tooltip.find('span').text(message);

            var inputOffset = $input.offset(),
                inputWidth = $input.outerWidth(),
                inputHeight = $input.outerHeight();

            var tooltipHeight = $tooltip.outerHeight();
            var tooltipWidth = $tooltip.outerWidth();

            $tooltip.css({
                left: (inputOffset.left + inputWidth - tooltipWidth) + 'px',
                top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
            });

            $tooltip.fadeIn();
        }

        function hideFreeVersion() {
            $('li[title="Бесплатная версия kladr-api.ru"]').remove();
            setTimeout(function () {
                $('li[title="Бесплатная версия kladr-api.ru"]').remove();
            }, 200);
        }

        $city.on('keyup', function () {
            hideFreeVersion();
        });
        $street.on('keyup', function () {
            hideFreeVersion();
        });
        $building.on('keyup', function () {
            hideFreeVersion();
        });

    })();
</script>
<style>
    /* Устанавливаем свой шрифт для выпадающего списка*/
    #kladr_autocomplete a,
    #kladr_autocomplete strong{
        font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
        font-size: 15px;
    }

    #kladr_autocomplete small {
        font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
        font-size: 13px;
    }

    /* Добавляем скругления и тень у выпадающего списка*/
    #kladr_autocomplete ul {
        border-radius: 0 0 5px 5px;
        border: 1px solid #ded7f9;
        overflow: hidden;
        background: #fff;
        -webkit-box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
        box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
        z-index: 1;
    }

    /* Прописываем стили для тултипа с сообщением об ошибке*/
    .tooltip {
        position: absolute;
        top: 16px;
        left: 360px;
        color: #b94a48;
        padding: 8px 10px;
        border-radius: 5px;
        border: 1px solid #eed3d7;
        background-color: #f2dede;
        opacity: 0.8;
        font-size: 14px;
        z-index: 100000;
    }
</style>
