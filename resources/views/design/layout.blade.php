@include('design.header')

<section class="content">
    <div class="content_opacity">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="infolabel__box_animate">
                    <div class="infolabel__box">
                        <div class="infolabel__box_text">
                            <p>На следующей неделе ожидается похолодание. Одевайтесь теплее.</p>
                        </div>
                        <a href="#" class="infolabel__box_close">
                            <img src="/img/close.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="row flex-wrap">

                @include('design.left_sidebar')
                @yield('content')
                @include('design.right_sidebar')
                @yield('more_news')
            </div>

        </div>
    </div>
</section>

<!-- Javascript -->

<script type="text/javascript" src="/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<script type="text/javascript" src="/js/main.js"></script>

<script src="//cdn.gravitec.net/storage/acf2a6c6c99ff086b9da82bc01f2c30b/client.js" async></script>

@yield('script')

@include('design.footer')


</body>
</html>
