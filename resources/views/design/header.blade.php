<!DOCTYPE html>

<!--[if !IE]><!--> <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <!--<![endif]-->
<head>
    <title>@yield('title')</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="Тутушин Дмитрий">

    <link rel="shortcut icon" href="/img/favicon.ico">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/css/slick.css">


    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="/css/main.css">


</head>

<body>

<header id="header" class="header">
    <div class="container">
        <div class="row">
            <div class="col-4 col-sm-3 col-md-3">
                <div class="header__logo_box">
                    <a href="/" class="header__logo">{{ config('app.name', 'Laravel') }}</a>
                    <a href="/" class="header__logo header__logo_mobile">{{ config('app.name', 'Laravel') }}</a>
                </div>
            </div>
            <div class="col-3 col-sm-2 d-md-none">
                <a data-fancybox data-src="#modal_notification" href="javascript:;" class="header__notification"> <img src="img/notification.svg" alt="">
                    <span class="header__notification_quantity">12</span>
                </a>
            </div>
            <div class="col-5 col-sm-3 offset-sm-4 d-md-none">
                <div class="header__mobile_icon">
                    <div class="header__mobile_searsh_box">
                        <a class="header__mobile_searsh" data-toggle="collapse" href="#collapsesearch" role="button" aria-expanded="false" aria-controls="collapsesearch">
                            <img src="img/search_mobile.svg" alt="" class="header__mobile_searsh_img">
                        </a>
                    </div>
                    <div class="header__mobile_menu_box">
                        <a class="header__mobile_menu" data-toggle="collapse" href="#collapseburger" role="button" aria-expanded="false" aria-controls="collapseburger">
                            <span class="mobile_burger"></span>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xl-3 d-none d-md-block offset-md-1 offset-lg-0">
                <div class="header__search">
                    <input type="text" placeholder="Поиск" id="search_input">
                    <a href="#" class="header__search_btn">
                        <img src="img/search.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-2 col-xl-3 offset-md-1 offset-lg-2 offset-xl-3 d-none d-md-block">
                <div class="header__comein_box">
                    {{--                    <a data-fancybox data-src="#modal_input" href="javascript:;" class="header__comein">Войти</a>--}}
                    @auth
                        <a class="header__comein mr-3" href="{{ route('admin') }}">Личный кабинет</a>

                        <a class="header__comein" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a class="header__comein" data-fancybox data-src="#modal_input" href="javascript:;" shref="{{ route('login') }}">Вход</a>

                        {{--                        @if (Route::has('register'))--}}
                        {{--                            <a class="header__comein ml-3" href="{{ route('register') }}">Регистрация</a>--}}
                        {{--                        @endif--}}
                    @endauth
                </div>
            </div>
            <div class="col-12">
                <div class="collapse search-mob" id="collapsesearch">
                    <div class="header__search">
                        <input type="text" placeholder="Поиск">
                        <a href="#" class="header__search_btn">
                            <img src="img/search.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div id="collapseburger" class="collapse burger-mob">
                    @auth
                        <a class="btn_bg btn_modal btn-block mr-3" href="{{ route('admin') }}">Личный кабинет</a>

                        <a class="btn_bg btn_modal btn-block" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a data-fancybox data-src="#modal_input" href="javascript:;" class="btn_bg btn_modal btn-block">Войти</a>

                        {{--                        @if (Route::has('register'))--}}
                        {{--                            <a class="header__comein ml-3" href="{{ route('register') }}">Регистрация</a>--}}
                        {{--                        @endif--}}
                    @endauth


                    <nav class="modal_burger_main_nav">
                        <ul class="">
                            @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
                                <li><a class="modal_burger-nav_link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                            @endforeach
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Популярное</a></li>--}}
                            <li class=""><a class="modal_burger-nav_link" href="/">Свежее</a></li>
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Опросы</a></li>--}}
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Рядом</a></li>--}}
                        </ul>
                    </nav>
                    <nav class="modal_burger_main_nav modal_footer_nav d-none">
                        <ul class="">
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Популярное</a></li>--}}
                            @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page)
                                <li><a class="modal_burger-nav_link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                            @endforeach
                            <li class=""><a class="modal_burger-nav_link" href="/">Свежее</a></li>
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Опросы</a></li>--}}
                            {{--                            <li class=""><a class="modal_burger-nav_link" href="#">Рядом</a></li>--}}
                        </ul>
                    </nav>
                    <div class="social">
                        <a href="#" class="social_link social_link_ok">
                            <img src="img/ok.svg" alt="">
                        </a>
                        <a href="#" class="social_link social_link_vk">
                            <img src="img/vk.svg" alt="">
                        </a>
                        <a href="#" class="social_link social_link_facebook">
                            <img src="img/facebook.svg" alt="">
                        </a>
                        <a href="#" class="social_link social_link_twitter">
                            <img src="img/twitter.svg" alt="">
                        </a>
                    </div>

                    <p class="rights">© 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены. </p>

                </div>


            </div>
        </div>
    </div>
    </div>
</header>
