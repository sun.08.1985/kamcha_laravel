<div class="col-lg-3">
    <div class="sidebar-menu">
        <nav class="main-nav navbar-right navbar-expand-lg" role="navigation">
            <ul class="nav navbar-nav">
                {{--                                <li class="nav-item"><a class="main-nav_link" href="#">Популярное</a></li>--}}
                @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
                    <li class="nav-item"><a class="main-nav_link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                @endforeach
                <li class="nav-item"><a class="main-nav_link" href="#">Свежее</a></li>
                {{--                                <li class="nav-item"><a class="main-nav_link" href="#">Опросы</a></li>--}}
                {{--                                <li class="nav-item"><a class="main-nav_link" href="#">Рядом</a></li>--}}
                {{--                                <li class="nav-item mobile-collaps"><a class="main-nav_link" href="#">Голосования</a></li>--}}
            </ul>
        </nav>
    </div>
</div>
