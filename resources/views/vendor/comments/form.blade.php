@php
    $count = $model->commentsWithChildrenAndCommenter()->count();
@endphp
<div class="article-comments__count">
    <h4>{{ $count . ' ' . Lang::choice('комментарий|комментария|комментариев', $count, [], 'ru') }} </h4>
</div>

<form method="POST" action="{{ route('comments.store') }}">
    <div class="article-comments-add">
        <div class="avatar" style="background-size: cover; background-image: url(/uploads/avatars/{{ Auth::user()->avatar }})">
            <span>{{ (Auth::user()->avatar == 'default.jpg')? Str::limit(Auth::user()->name, 1, ''): '' }}</span>
        </div>
            @csrf
            <input type="hidden" name="commentable_encrypted_key" value="{{ $model->getEncryptedKey() }}"/>
            @error('error')
                <span style="color: red; font-weight: bolder">{{ $message }}</span>
            @enderror
            @if(Session::has('error'))
                <span style="color: red; font-weight: bolder">{{ Session::get('error') }}</span>
                {{ Session::forget('error') }}
            @endif
            <textarea placeholder="Оставить комментарий" class="autoheight__inner" name="message">{{ old('message') }}</textarea>
            <button class="btn btn--blue" type="submit">Отправить</button>
    </div>
</form>


{{--<div class="card">--}}
{{--    <div class="card-body">--}}

{{--            <input type="hidden" name="commentable_encrypted_key" value="{{ $model->getEncryptedKey() }}"/>--}}

{{--            <div class="form-group">--}}
{{--                <label for="message">Enter your message here:</label>--}}
{{--                <textarea class="form-control @if($errors->has('message')) is-invalid @endif" name="message"--}}
{{--                          rows="3"></textarea>--}}
{{--                <div class="invalid-feedback">--}}
{{--                    Your message is required.--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">Submit</button>--}}
{{--        </form>--}}
{{--    </div>--}}
{{--</div>--}}
<br/>
