{{--@auth()--}}
    <div class="comment" id="comment-{{ $comment->id }}" >
        <div class="comment-info">
            <div class="avatar comment-info__avatar" style="background-size: cover; background-image: url(/uploads/avatars/{{ $comment->commenter->avatar }})">
                <span>{{ ($comment->commenter->avatar == 'default.jpg')? Str::limit($comment->commenter->name, 1, ''): '' }}</span>
            </div>
            <a href="#" class="comment-info__name" onclick="$('[name=message]').val('{{ $comment->commenter->name }}, ' + $('[name=message]').val()).focus()">{{ $comment->commenter->name }}</a>
            <time class="comment-info__time">{{ $comment->created_at->diffForHumans() }}</time>
        </div>
        <div class="comment__content">{!! $comment->comment!!}
            <div style="text-align: right; font-size: small">
                @can('comments.vote', $comment)

                    <form action="{{route('comments.vote', $comment->id)  }}" method="POST"
                          style="display: inline-block">
                        @csrf
                        <input type="hidden" name="vote" value="0"/>
                        <button type="submit">
                            -1
                        </button>
                    </form>
                @endcan
                Рейтинг: {{$comment->rating()}}
                @can('comments.vote', $comment)

                <form action="{{route('comments.vote', $comment->id)  }}" method="POST"
                      style="display: inline-block">
                    @csrf
                    <input type="hidden" name="vote" value="1"/>
                    <button type="submit">
                        +1
                    </button>
                </form>
                @endcan
            </div>
        </div>
        @can('comments.delete', $comment)
            <a href="#"
               onclick="event.preventDefault();document.getElementById('comment-delete-form-{{ $comment->id }}').submit();"
               class=""><strong style="color: red; text-align: right">Удалить</strong></a>
            <form id="comment-delete-form-{{ $comment->id }}"
                  action="{{route('comments.delete', $comment->id)  }}" method="POST" style="display: none;">
                @method('DELETE')
                @csrf
            </form>
        @endcan
    </div>
{{--@endauth--}}


{{--@if(isset($reply) && $reply === true)--}}
{{--    <div id="comment-{{ $comment->id }}" class="media">--}}
{{--        @else--}}
{{--            <li id="comment-{{ $comment->id }}" class="media">--}}
{{--                @endif--}}
{{--                <img class="mr-3" src="https://www.gravatar.com/avatar/{{ md5($comment->commenter->email) }}.jpg?s=64"--}}
{{--                     alt="{{ $comment->commenter->name }} Avatar">--}}
{{--                <div class="media-body">--}}
{{--                    <h5 class="mt-0 mb-1">--}}
{{--                        {{ $comment->commenter->name }}--}}
{{--                        <small class="text-muted">- {{ $comment->created_at->diffForHumans() }}</small>--}}
{{--                    </h5>--}}
{{--                    <div style="white-space: pre-wrap;">--}}
{{--                        {!! $comment->comment!!}--}}
{{--                    </div>--}}

{{--                    <p>--}}
{{--@if(Auth::user()->isAdmin())--}}
{{--                        <button data-toggle="modal" data-target="#reply-modal-{{ $comment->id }}"--}}
{{--                                class="btn btn--green small">Ответить--}}
{{--                        </button>--}}
{{--                        @can('comments.edit', $comment)--}}
{{--                            <button data-toggle="modal" data-target="#comment-modal-{{ $comment->id }}"--}}
{{--                                    class="btn btn--green small">Редактировать--}}
{{--                            </button>--}}
{{--                        @endcan--}}

{{--@endif--}}
{{--                    </p>--}}



{{--                        @include('comments::components.comment.forms')--}}
{{--                        <br/>--}}

{{--                        @foreach($comment->allChildrenWithCommenter as $child)--}}
{{--                            @include('comments::components.comment.comment', [--}}
{{--                                    'comment' => $child,--}}
{{--                                    'reply' => true--}}
{{--                                ])--}}
{{--                        @endforeach--}}
{{--                </div>--}}

{{--    {!! isset($reply) && $reply === true ? '</div>' : '</li>' !!}--}}
