@extends('layouts.admin')

@section('content')

    <section class="admin-panel-content">
        {{--        <form class="admin-panel-content__search">--}}
        {{--            <input type="text" placeholder="Поиск...">--}}
        {{--        </form>--}}
        {{--        <div class="news-table">--}}
        {{--            <div class="news-table__heading">--}}
        {{--                <h3>В работе</h3>--}}
        {{--                <a href="#" class="news-table__heading-new">--}}
        {{--                    <svg class="icon icon-plus">--}}
        {{--                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-plus"></use>--}}
        {{--                    </svg>--}}
        {{--                    <span>Новая</span>--}}
        {{--                </a>--}}
        {{--            </div>--}}
        {{--            <div class="news-table-content">--}}
        {{--                <table>--}}
        {{--                    <thead>--}}
        {{--                    <tr>--}}
        {{--                        <td>Статус</td>--}}
        {{--                        <td>Заголовок</td>--}}
        {{--                        <td>Редактор</td>--}}
        {{--                        <td>Создан</td>--}}
        {{--                        <td></td>--}}
        {{--                    </tr>--}}
        {{--                    </thead>--}}
        {{--                    <tbody>--}}
        {{--                    <tr>--}}
        {{--                        <td>В работе</td>--}}
        {{--                        <td>--}}
        {{--                            <a href="#">Очень длинный заголовок, который может занимать две строки и больше</a>--}}
        {{--                            <span>О городе</span>--}}
        {{--                        </td>--}}
        {{--                        <td>--}}
        {{--                            Василий Парфенов--}}
        {{--                        </td>--}}
        {{--                        <td>22.03.2019 12:31</td>--}}
        {{--                        <td>--}}
        {{--                        <span class="news-table-more">--}}
        {{--                            <a href="#pixel" class="news-table-more__trigger">--}}
        {{--                                <svg class="icon icon-more">--}}
        {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
        {{--                                </svg>--}}
        {{--                            </a>--}}
        {{--                            <span class="news-table-more__popup">--}}
        {{--                                <a href="#">Редактировать</a>--}}
        {{--                                <a href="#">Удалить</a>--}}
        {{--                            </span>--}}
        {{--                        </span>--}}
        {{--                        </td>--}}
        {{--                    </tr>--}}
        {{--                    <tr>--}}
        {{--                        <td>В работе</td>--}}
        {{--                        <td>--}}
        {{--                            <a href="#">Идейные соображения высшего порядка</a>--}}
        {{--                            <span>О городе</span>--}}
        {{--                        </td>--}}
        {{--                        <td>--}}
        {{--                            Василий Парфенов--}}
        {{--                        </td>--}}
        {{--                        <td>22.03.2019 12:31</td>--}}
        {{--                        <td>--}}
        {{--                        <span class="news-table-more">--}}
        {{--                            <a href="#pixel" class="news-table-more__trigger">--}}
        {{--                                <svg class="icon icon-more">--}}
        {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
        {{--                                </svg>--}}
        {{--                            </a>--}}
        {{--                            <span class="news-table-more__popup">--}}
        {{--                                <a href="#">Редактировать</a>--}}
        {{--                                <a href="#">Удалить</a>--}}
        {{--                            </span>--}}
        {{--                        </span>--}}
        {{--                        </td>--}}
        {{--                    </tr>--}}
        {{--                    <tr>--}}
        {{--                        <td>В работе</td>--}}
        {{--                        <td>--}}
        {{--                            <a href="#">Очень длинный заголовок, который может занимать две строки и больше</a>--}}
        {{--                            <span>О городе</span>--}}
        {{--                        </td>--}}
        {{--                        <td>--}}
        {{--                            Василий Парфенов--}}
        {{--                        </td>--}}
        {{--                        <td>22.03.2019 12:31</td>--}}
        {{--                        <td>--}}
        {{--                        <span class="news-table-more">--}}
        {{--                            <a href="#pixel" class="news-table-more__trigger">--}}
        {{--                                <svg class="icon icon-more">--}}
        {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
        {{--                                </svg>--}}
        {{--                            </a>--}}
        {{--                            <span class="news-table-more__popup">--}}
        {{--                                <a href="#">Редактировать</a>--}}
        {{--                                <a href="#">Удалить</a>--}}
        {{--                            </span>--}}
        {{--                        </span>--}}
        {{--                        </td>--}}
        {{--                    </tr>--}}
        {{--                    <tr>--}}
        {{--                        <td>В работе</td>--}}
        {{--                        <td>--}}
        {{--                            <a href="#">Очень длинный заголовок, который может занимать две строки и больше</a>--}}
        {{--                            <span>О городе</span>--}}
        {{--                        </td>--}}
        {{--                        <td>--}}
        {{--                            Василий Парфенов--}}
        {{--                        </td>--}}
        {{--                        <td>22.03.2019 12:31</td>--}}
        {{--                        <td>--}}
        {{--                        <span class="news-table-more">--}}
        {{--                            <a href="#pixel" class="news-table-more__trigger">--}}
        {{--                                <svg class="icon icon-more">--}}
        {{--                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
        {{--                                </svg>--}}
        {{--                            </a>--}}
        {{--                            <span class="news-table-more__popup">--}}
        {{--                                <a href="#">Редактировать</a>--}}
        {{--                                <a href="#">Удалить</a>--}}
        {{--                            </span>--}}
        {{--                        </span>--}}
        {{--                        </td>--}}
        {{--                    </tr>--}}
        {{--                    </tbody>--}}
        {{--                </table>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        <div class="news-table">
            <div class="news-table__heading">
                <h3>Опубликовано</h3>
                <a href="#" class="news-table__heading-new">
                    <svg class="icon icon-plus">
                        <use xlink:href="./img/sprites/main-sprite.svg#icon-plus"></use>
                    </svg>
                    <span>Новая</span>
                </a>
{{--                <div class="select news-table__heading-sort">--}}
                    {{--                    <select>--}}
                    {{--                        <option>Пример 1</option>--}}
                    {{--                        <option>Пример 2</option>--}}
                    {{--                        <option>Пример 3</option>--}}
                    {{--                    </select>--}}
{{--                </div>--}}
            </div>
            <div class="news-table-content">
                <table>
                    <thead>
                    <tr>
                                                <td>Статус</td>
                        <td>Заголовок</td>
                        <td>Редактор</td>
                        <td>Создан</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $new)
                        <tr>
                            <td>Опубликовано</td>
                            <td>
                                <a href="/news/{{ $new->slug }}">{{ $new->title }}</a>
                                <span>{{ \App\Category::find($new->category_id)->name }}</span>
                            </td>
                            <td>
                                {{ $new->user_id }}
                            </td>
                            <td>{{ $new->created_at }}</td>
                            <td>
                        <span class="news-table-more">
                            <a href="#pixel" class="news-table-more__trigger">
                                <svg class="icon icon-more">
                                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>
                                </svg>
                            </a>
                            <span class="news-table-more__popup">
                                <a href="{{ route('news.edit', $new) }}">Редактировать</a>
{{--                                <a href="#">Снять с публикации</a>--}}
                            </span>
                        </span>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
