@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
{{--                        {{ dd($question) }}--}}
                        {{ Breadcrumbs::render('question_edit', $question, $question->test_id) }}
{{--                        <a class="btn btn-primary edit-btn" href="{{ route('tests.edit', [$question->test_id]) }}">Вернуться к опросу</a>--}}
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['questions.update', $question->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="title">Вопрос</label>
                                <input type="text" class="form-control" name="title" value="{{ $question->title }}">
                                @if ($errors->has('title'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <input type="hidden" class="form-control" name="answer_id" value="1">
                            <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                            <dd>
                                <div role="button" data-toggle="collapse" data-target="#collapseDescription" class="card-header" aria-expanded="false" aria-controls="collapseDescription">
                                    Описание (опционально)
                                    <span class="pull-right"><i class="fa fa-caret-down"></i></span>
                                </div>
                                <div class="collapse card-body" id="collapseDescription">
                                    <textarea class="form-control" name="description"
                                              rows="10">{{ $question->description }}</textarea>
                                </div>
                                @if ($errors->has('description'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-warning" type="submit">Изменить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    <div class="card-footer">
                        <h3>Ответы</h3>
                        <a href="{{ route('answers.create', $question->id) }}" class="btn btn-success">Добавить</a>

                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Ответ</td>
                                    <td>Описание</td>
                                    <td>Верный ответ</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @forelse($question->answers as $answer)
                                <tr>
                                    <td>{{ $answer->id }}</td>
                                    <td>{{ $answer->title }}</td>
                                    <td>{!! $answer->description !!}</td>
                                    <td><input type="checkbox" {{ $answer->correct ? 'checked':'' }} disabled></td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('answers.show', $answer->id) }}"><i
                                                class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" href="{{ route('answers.edit', $answer->id) }}"><i
                                                class="fa fa-edit"></i></a>
                                        {!! Form::open(['route' => ['answers.destroy', $answer->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                        <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i
                                                class="fa fa-remove"></i></button>
                                        {!! Form::close() !!}

{{--                                        <a href="{{ route('answers.destroy', $answer->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @empty
                                <h3 class="text-center">Нет ответов</h3>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
