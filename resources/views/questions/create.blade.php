@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('question_add', $test) }}
{{--                        <a class="btn btn-primary edit-btn" href="{{ route('tests.edit', [$question->test_id]) }}">Вернуться к опросу</a>--}}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'questions.store', 'method' => 'post']) !!}

                            <dd>
                                <label for="title">Вопрос</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                @if ($errors->has('title'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <input type="hidden" class="form-control" name="test_id" value="{{ $test->id }}">
                            <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                            <dd>
                                <label for="description">Описание</label>
                                <textarea class="form-control" name="description" rows="10">{{ old('description') }}</textarea>
                                @if ($errors->has('description'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-success" type="submit">Сохранить</button>

                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
