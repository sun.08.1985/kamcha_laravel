@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Вопросы</h1>
{{--                        <a class="btn btn-primary edit-btn" href="{{ route('tests.edit', [$question->test_id]) }}">Вернуться к опросу</a>--}}
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Название</td>
                                    <td>Количество</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->title }}</td>
                                    <td>
                                        @isset($question->answers)
                                            {{ $question->answers->count() }}
                                        @endisset
                                    </td>
                                    <td>
{{--                                        <a class="btn btn-primary" href="{{ route('questions.show', $question->id) }}"><i class="fa fa-eye"></i></a>--}}
                                        <a class="btn btn-success" href="{{ route('questions.edit', $question->id) }}"><i class="fa fa-edit"></i></a>
                                        {!! Form::open(['route' => ['questions.destroy', $question->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                        <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-remove"></i></button>
                                        {!! Form::close() !!}

{{--                                        <a href="{{ route('questions.destroy', $question->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @include('pagination.default', ['paginator' => $questions])
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
