@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Вопрос - {{ $question->title }}<h1>
                        <a class="btn btn-primary edit-btn" href="{{ route('tests.edit', [$question->test_id]) }}">Вернуться к опросу</a>
                        @auth
                            <a class="btn btn-success btn-sm edit-btn" href="{{ route('questions.edit', $question) }}">Редактировать</a>
                        @endauth
                    </div>
                    <div class="card-body">
                        {!! $question->description !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
