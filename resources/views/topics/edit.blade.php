@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('topic_edit', $topic) }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['topics.update', $topic->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" value="{{ $topic->name }}">
                                @if ($errors->has('name'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>

                            <button class="btn btn-warning" type="submit">Изменить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
