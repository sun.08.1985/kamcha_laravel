@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('topic_view') }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['topics.update', $topic->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" disabled value="{{ $topic->name }}">
                            </dd>
                            <dd>
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" disabled value="{{ $topic->email }}">
                            </dd>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
