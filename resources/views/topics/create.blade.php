@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('topic_add') }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'topics.store', 'method' => 'post']) !!}

                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-success" type="submit">Сохранить</button>

                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
