@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('topics') }}
                        @if (Auth::user()->hasRoles(['super_admin', 'admin']) )
                            <a class="position-absolute btn btn-success btn-sm btn-add" href="{{ route('topics.create') }}">Добавить</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>{{ __('Name') }}</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($topics as $topic)
                                <tr>
                                    <td>{{ $topic->id }}</td>
                                    <td>{{ $topic->name }}</td>
                                    <td>
{{--                                        <a class="btn btn-sm btn-primary" href="{{ route('topics.show', $topic->id) }}"><i class="fa fa-eye"></i></a>--}}
                                        <a class="btn btn-sm btn-success" href="{{ route('topics.edit', $topic->id) }}"><i class="fa fa-edit"></i></a>
                                        {!! Form::open(['route' => ['topics.destroy', $topic->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                        <button class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}

{{--                                        <a href="{{ route('topics.destroy', $topic->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                        @include('pagination.default', ['paginator' => $topics])
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
