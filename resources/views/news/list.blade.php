@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <h3>Опросы</h3>
        <div class="row">

            <div class="col-md-12">
                <table class="table">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Название</td>
                            <td>Количество</td>
                            <td>{{ __('Action') }}</td>
                        </tr>
                    </thread>
                    <tbody>

                    @foreach($news as $new)
                        <tr>
                            <td>{{ $new->id }}</td>
                            <td>{{ $new->title }}</td>
                            <td>
                                @isset($new->questions)
                                    {{ $new->questions->count() }}
                                @endisset
                            </td>
                            <td>
                                <a class="btn btn-info" href="{{ route('new.start', $new->id) }}"><i class="fa fa-play"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
