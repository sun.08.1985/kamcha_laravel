@extends('layouts.design')

@section('menu_left')
    @include('layouts.menu_left')
@endsection

@section('sidebar_right')
    @include('layouts.menu_right')
@endsection


@section('h1')
    {{ $new->title }}
@stop

@section('title')
    {{ $new->title }} - {{ config('app.name') }}
@stop

@section('keywords')
    {{ $new->keywords }}
@stop

@section('description')
    {{ $new->description }}
@stop

@section('content')
    <article class="article">
        <div class="article-info">
            <div class="article-info__category">{{ $new->category->name }}</div>
            <time class="article-info__date">{{ \Carbon\Carbon::parse($new->created_at)->diffForHumans() }}</time>

        </div>
        <div class="article-content">
            <h1>{{ $new->title }}</h1>
            @auth()
                @if(Auth::user()->hasRoles(['admin', 'super_admin']))
                <small>
                    <a href="{{ route('news.edit', $new->id) }}" class="btn btn--green small">
                        {{--                        <svg class="icon icon-social-twitter">--}}
                        {{--                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>--}}
                        {{--                        </svg>--}}
                        Редактировать
                    </a>
                </small>
                @endif
            @endauth
            {!! $new->content !!}
        </div>



{{--        <x-comments :model="$new"/>--}}

        <div class="article-comments">
            @comments(['model' => $new]) @endcomments

{{--            <div class="article-comments-add">--}}
{{--                <div class="avatar">--}}
{{--                    <span>И</span>--}}
{{--                </div>--}}
{{--                <textarea placeholder="Оставить комментарий" class="autoheight__inner"></textarea>--}}
{{--                <button class="btn btn--blue">Отправить</button>--}}
{{--            </div>--}}
{{--            <div class="article-comments-list">--}}
{{--                <div class="comment">--}}
{{--                    <div class="comment-info">--}}
{{--                        <div class="avatar comment-info__avatar">--}}
{{--                            <span>Р</span>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="comment-info__name">Максим Малыгин</a>--}}
{{--                        <time class="comment-info__time">5 дней назад</time>--}}
{{--                    </div>--}}
{{--                    <div class="comment__content">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры--}}
{{--                        позволяет оценить значение модели развития.</div>--}}
{{--                </div>--}}
{{--                <div class="comment">--}}
{{--                    <div class="comment-info">--}}
{{--                        <div class="avatar comment-info__avatar">--}}
{{--                            <span>Р</span>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="comment-info__name">Зинаида Емельянова</a>--}}
{{--                        <time class="comment-info__time">5 дней назад</time>--}}
{{--                    </div>--}}
{{--                    <div class="comment__content">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры--}}
{{--                        позволяет оценить значение модели развития.</div>--}}
{{--                </div>--}}
{{--                <div class="comment">--}}
{{--                    <div class="comment-info">--}}
{{--                        <div class="avatar comment-info__avatar">--}}
{{--                            <span>Р</span>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="comment-info__name">Зинаида Емельянова</a>--}}
{{--                        <time class="comment-info__time">5 дней назад</time>--}}
{{--                    </div>--}}
{{--                    <div class="comment__content">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры--}}
{{--                        позволяет оценить значение модели развития.</div>--}}
{{--                </div>--}}
{{--                <div class="comment">--}}
{{--                    <div class="comment-info">--}}
{{--                        <div class="avatar comment-info__avatar">--}}
{{--                            <span>Р</span>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="comment-info__name">Зинаида Емельянова</a>--}}
{{--                        <time class="comment-info__time">5 дней назад</time>--}}
{{--                    </div>--}}
{{--                    <div class="comment__content">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры--}}
{{--                        позволяет оценить значение модели развития.</div>--}}
{{--                </div>--}}
{{--                <div class="comment">--}}
{{--                    <div class="comment-info">--}}
{{--                        <div class="avatar comment-info__avatar">--}}
{{--                            <span>Р</span>--}}
{{--                        </div>--}}
{{--                        <a href="#" class="comment-info__name">Зинаида Емельянова</a>--}}
{{--                        <time class="comment-info__time">5 дней назад</time>--}}
{{--                    </div>--}}
{{--                    <div class="comment__content">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры--}}
{{--                        позволяет оценить значение модели развития.</div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="article-comments-add">--}}
{{--                <div class="avatar">--}}
{{--                    <span>И</span>--}}
{{--                </div>--}}
{{--                <textarea placeholder="Оставить комментарий" class="autoheight__inner"></textarea>--}}
{{--                <button class="btn btn--blue">Отправить</button>--}}
{{--            </div>--}}
        </div>
    </article>
@endsection

@section('more_news')
    <section class="simillar">
        <div class="simillar-wrap limiter">
            <div class="simillar__title">
                <h4>Еще новости на тему «{{ \App\Category::find($new->category_id)->name }}»</h4>
            </div>
            <div class="simillar-list">
                @foreach(\App\News::where('category_id', $new->category_id)->limit(3)->get() as $new_s)
                    <div class="simillar__item">
                        <a href="#" class="simillar__item-photo">
                            <img src="{{ $new_s->image_url }}" alt="">
                        </a>
                        <a href="#" class="simillar__item-title">{{ $new_s->title }}</a>
                        <time class="simillar__item-date">{{ \Carbon\Carbon::parse($new_s->created_at)->diffForHumans() }}</time>
                        <div class="simillar__item-views">{{ $new_s->see_count }} просмотров</div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endsection
