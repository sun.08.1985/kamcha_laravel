@if(!empty($news))
    @foreach($news as $new)
    <div class="news__item">
        <div class="news__item-top">
            <a href="#" class="news__item-category">{{ $new->category->name }}</a>
            <time class="news__item-date">{{ \Carbon\Carbon::parse($new->created_at)->diffForHumans() }}</time>
            <a href="#" class="news__item-share">
                <svg class="icon icon-share">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-share"></use>
                </svg>
            </a>
        </div>
        <div class="news__item-content">
            <h3><a href="{{ route('news.slug', $new->slug) }}">{{ $new->title }}</a></h3>
            <p>{{ Str::limit(strip_tags($new->content), 255) }}</p>
            @if($new->image_url)
                <img src="{{ $new->image_url }}" alt="{{ $new->title }}">
            @endif
        </div>
        <div class="news__item-bot">
            <div class="news__item-views">
                <svg class="icon icon-eye">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                </svg>
                <span>{{ $new->see_count }}</span>
            </div>
            <div class="news__item-comments">
                <svg class="icon icon-comments">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-comments"></use>
                </svg>
                <span>{{ $new->commentsWithChildrenAndCommenter()->count() }}</span>
            </div>
        </div>
    </div>


@endforeach
@endif
