@extends('layouts.admin')

@section('sidebar')
    @parent

@stop

@section('content')
    <section class="admin-panel-content">
{{--        <form class="admin-panel-content__search">--}}
{{--            <input type="text" placeholder="Поиск...">--}}
{{--        </form>--}}
        <div class="admin-panel-section admin-panel-article-edit">
            {!! Form::open(['route' => ['news.update', $new->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                <div class="admin-panel-section__heading">
                    <div class="select admin-panel-article-edit__select">
                        <select name="category_id" value="{{$new->category->id}}">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <a href="#" class="admin-panel__prorogue"> </a>
                    <button type="submit" class="admin-panel__publish btn btn--blue">Опубликовать</button>
                </div>
                <div class="admin-panel-section__content admin-panel-new-ntf__area">
                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="in1">Заголовок</label>
                        <div class="input-module__inner-area"><input name="title" value="{{$new->title}}" type="text" class="input" minlength="5" maxlength="100"></div>
                        <span></span>
                    </div>
                </div>
                <div class="input-module input-module--textarea admin-panel-new-ntf__area-mess">
                    <div class="input-module__inner">
                        <label for="in3">Текст оповещения</label>
                        <div class="input-module__inner-area">
                            <textarea name="description" value="{{$new->description}}" class="autoheight__inner" maxlength="200" style="height: 36px;"></textarea>
                            <div class="input-module__inner-area-counter">
                                <div>19</div>/200
                            </div>
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="image_url">Превью - ссылка на картинку</label>
                        <div class="input-module__inner-area">
                            <input type="file" class="form-control" name="image_url" accept="image/x-png">
                        </div>
                        @if($new->image_url)
                            <img src="{{ $new->image_url }}" alt="{{ $new->title }}">
                        @endif
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="keywords">Ключевые слова</label>
                        <div class="input-module__inner-area">
                            <input type="text" class="form-control" name="keywords" value="{{$new->keywords}}" maxlength="200">
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="order">Позиция</label>
                        <div class="input-module__inner-area">
                            <input type="number" class="form-control" name="order" value="{{$new->order}}">
                            <label for="menu_news">В меню новостей <input class="form-control" type="checkbox" name="menu_news" {{$new->menu_news? 'checked':'' }}></label>
                        </div>
                    </div>
                </div>

                <div class="admin-panel-section__content admin-panel-article-edit__area">
                    <textarea id="mce_0" aria-hidden="true" name="content">{{ $new->content }}</textarea>
                </div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop
