@extends('layouts.admin')

@section('sidebar')
    @parent

@stop

@section('content')
    <section class="admin-panel-content">
        {{--        <form class="admin-panel-content__search">--}}
        {{--            <input type="text" placeholder="Поиск...">--}}
        {{--        </form>--}}
        <div class="admin-panel-section admin-panel-article-edit">
            {!! Form::open(['route' => ['news.store', old('id')], 'method' => 'POST', 'id' => 'formPublish', 'enctype' => 'multipart/form-data']) !!}
            <div class="admin-panel-section__heading">
                <div class="select admin-panel-article-edit__select">
                    <select name="category_id" value="{{ old('category_id') }}">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <a href="#" class="admin-panel__prorogue"> </a>
                <button type="submit" class="admin-panel__publish btn btn--blue">Опубликовать</button>
            </div>
            <div class="admin-panel-section__content admin-panel-new-ntf__area">
                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="in1">Заголовок</label>
                        <div class="input-module__inner-area"><input name="title" value="{{ old('title') }}" type="text" class="input" minlength="5" maxlength="100"></div>
                        <span></span>
                    </div>
                </div>
                <div class="input-module input-module--textarea admin-panel-new-ntf__area-mess">
                    <div class="input-module__inner">
                        <label for="in3">Текст оповещения</label>
                        <div class="input-module__inner-area">
                            <textarea name="description" value="{{ old('description') }}" class="autoheight__inner" maxlength="200" style="height: 36px;"></textarea>
                            <div class="input-module__inner-area-counter">
                                <div>0</div>/200
                            </div>
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="image_url">Превью - ссылка на картинку</label>
                        <div class="input-module__inner-area">
                            <input type="file" class="form-control" name="image_url" value="{{ old('image_url') }}" accept="image/x-png">
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="keywords">Ключевые слова</label>
                        <div class="input-module__inner-area">
                            <input type="text" class="form-control" name="keywords" value="{{ old('keywords') }}" maxlength="200">
                       </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="order">Позиция</label>
                        <div class="input-module__inner-area">
                            <input type="number" class="form-control" name="order" value="{{ old('order') }}">
                            <label for="menu_news">В меню новостей <input class="form-control" type="checkbox" name="menu_news" {{ old('menu_news') ? 'checked':'' }}></label>
                        </div>
                    </div>
                </div>





                <div class="admin-panel-new-ntf__area-receivers">
                    <h4>Кого оповещать</h4>
                    <div class="admin-panel-new-ntf__area-receivers-variants">
                        <label>
                            <input type="radio" name="receivers" checked="">
                            <span>Всех</span>
                        </label>
                        <label>
                            <input type="radio" name="receivers">
                            <span>Адрес</span>
                        </label>
                    </div>
                </div>
                <div class="input-module admin-panel-new-ntf__area-address">
                    <input type="hidden" name="district" data-kladr-id="4100900000000">
                    <input type="hidden" name="zip">
                    <input type="hidden" name="region" data-kladr-id="4100000000000">
                    <div class="input-module__inner">
                        <label for="in1">Город</label>
                        <div class="input-module__inner-area"><input name="city" id="city" type="text" class="input"></div>
                        <span></span>
                    </div>
                    <div class="input-module__inner">
                        <label for="in2">Улица</label>
                        <div class="input-module__inner-area"><input name="street" id="street" type="text" class="input"></div>
                        <span></span>
                    </div>
                    <div class="input-module__inner">
                        <label for="in3">Дом</label>
                        <div class="input-module__inner-area"><input name="building" id="building" type="text" class="input"></div>
                        <span></span>
                    </div>

                </div>
{{--                <a href="#" class="admin-panel-new-ntf__area-add">--}}
{{--                    <svg class="icon icon-plus">--}}
{{--                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-plus"></use>--}}
{{--                    </svg>--}}
{{--                    --}}{{--                        <span>Добавить адрес</span>--}}
{{--                </a>--}}
            </div>
            <div class="admin-panel-section__content admin-panel-article-edit__area">
                <textarea id="mce_0" aria-hidden="true" name="content">{{ old('content') }}</textarea>
            </div>
            {!! Form::close() !!}
        </div>
    </section>

@stop

@section('kladr')

    <script src="/js/jquery-3.4.1.min.js" type="text/javascript"></script>
    <link href="/css/jquery.fias.min.css" rel="stylesheet">
    <script src="/js/jquery.fias.min.js" type="text/javascript"></script>
    {{--    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
    <script>
        // Form example
        (function () {
            var $container = $('.admin-panel-section');//document.getElementById('formPublish'));

            var $tooltip = $('#tooltip');

            var $zip = $container.find('[name="zip"]'),
                $region = $container.find('[name="region"]'),
                $district = $container.find('[name="district"]'),
                $city = $container.find('[name="city"]'),
                $street = $container.find('[name="street"]'),
                $building = $container.find('[name="building"]');

            $()
                .add($region)
                .add($district)
                .add($city)
                .add($street)
                .add($building)
                .fias({
                    parentInput: $container.find('#formPublish'),
                    verify: true,
                    select: function (obj) {
                        if (obj.zip) $zip.val(obj.zip);//Обновляем поле zip
                        setLabel($(this), obj.type);
                        $tooltip.hide();
                    },
                    check: function (obj) {
                        var $input = $(this);

                        if (obj) {
                            setLabel($input, obj.type);
                            $tooltip.hide();
                        }
                        else {
                            showError($input, 'Ошибка');
                        }
                    },
                    checkBefore: function () {
                        var $input = $(this);

                        if (!$.trim($input.val())) {
                            $tooltip.hide();
                            return false;
                        }
                    }
                });

            $region.fias('type', $.fias.type.region);
            $district.fias('type', $.fias.type.district);
            $city.fias('type', $.fias.type.city);
            $street.fias('type', $.fias.type.street);
            $building.fias('type', $.fias.type.building);

            $district.fias('withParents', true);
            $city.fias('withParents', true);
            $street.fias('withParents', true);

            // Отключаем проверку введённых данных для строений
            $building.fias('verify', false);

            // Подключаем плагин для почтового индекса
            $zip.fiasZip($container);

            function setLabel($input, text) {
                text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                $input.parent().find('label').text(text);
            }

            function showError($input, message) {
                $tooltip.find('span').text(message);

                var inputOffset = $input.offset(),
                    inputWidth = $input.outerWidth(),
                    inputHeight = $input.outerHeight();

                var tooltipHeight = $tooltip.outerHeight();
                var tooltipWidth = $tooltip.outerWidth();

                $tooltip.css({
                    left: (inputOffset.left + inputWidth - tooltipWidth) + 'px',
                    top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
                });

                $tooltip.fadeIn();
            }

            function hideFreeVersion() {
                $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                setTimeout(function () {
                    $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                }, 200);
            }

            $city.on('keyup', function () {
                hideFreeVersion();
            });
            $street.on('keyup', function () {
                hideFreeVersion();
            });
            $building.on('keyup', function () {
                hideFreeVersion();
            });

        })();
    </script>
    <style>
        /* Устанавливаем свой шрифт для выпадающего списка*/
        #kladr_autocomplete a,
        #kladr_autocomplete strong{
            font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
            font-size: 15px;
        }

        #kladr_autocomplete small {
            font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
            font-size: 13px;
        }

        /* Добавляем скругления и тень у выпадающего списка*/
        #kladr_autocomplete ul {
            border-radius: 0 0 5px 5px;
            border: 1px solid #ded7f9;
            overflow: hidden;
            background: #fff;
            -webkit-box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
            box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
            z-index: 1;
        }

        /* Прописываем стили для тултипа с сообщением об ошибке*/
        .tooltip {
            position: absolute;
            top: 16px;
            left: 360px;
            color: #b94a48;
            padding: 8px 10px;
            border-radius: 5px;
            border: 1px solid #eed3d7;
            background-color: #f2dede;
            opacity: 0.8;
            font-size: 14px;
            z-index: 100000;
        }
    </style>
@stop
