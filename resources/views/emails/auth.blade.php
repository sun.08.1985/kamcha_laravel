@component('mail::message')
    # Ваша учетная запись создана

    Логин: {{ $email }}
    {{ __('Password') }}: {{ $password }}

    @component('mail::button', ['url' => config('app.url')])
        Войти
    @endcomponent

    Спасибо, {{ config('app.name') }}
@endcomponent
