@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
{{--                        {{ Breadcrumbs::render('user_edit', $user) }}--}}
                        Назначение опроса
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['users.assigned.pupil', $test_id], 'method' => 'PUT']) !!}

                            <dd>
                                <label for="user_id">Ученик</label>
                                <select class="form-control" name="user_id">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('current_test_id'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-warning" type="submit">Рекомендовать</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
