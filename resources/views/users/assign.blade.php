@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('user_edit', $user) }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['users.assigned', $user->id], 'method' => 'PUT']) !!}

                            <dd>
                                <label for="current_test_id">Опросы</label>
                                <input type="text" class="form-control" name="current_test_id" value="{{ $user->current_test_id }}">
                                @if ($errors->has('current_test_id'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-warning" type="submit">Назначить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
