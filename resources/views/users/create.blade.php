@extends('layouts.admin')

@section('content')
    {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'id' => 'formPublish']) !!}
    <div class="admin-panel-section__heading">
        <div class="select admin-panel-article-edit__select">


        </div>
        <a href="#" class="admin-panel__prorogue"> </a>
        <button type="submit" class="admin-panel__publish btn btn--blue">Создать</button>
    </div>

    <div class="admin-panel-section__content admin-panel-new-ntf__area">

        {{--                    <div class="input-module admin-panel-new-ntf__area-title">--}}
        {{--                        <div class="input-module__inner">--}}
        {{--                            <label for="in1">Заголовок</label>--}}
        {{--                            <div class="input-module__inner-area"><div class="input-module__inner-area"><input name="title" value="Особенная путина-2020" type="text" class="input"></div></div>--}}
        {{--                            <span></span>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        <div class="input-module admin-panel-new-ntf__area-title">
            <div class="input-module__inner">
                <div class="input-module__inner-area"><input type="hidden" name="district" data-kladr-id="4100900000000"></div>
                <div class="input-module__inner-area"><input type="hidden" name="zip"></div>
                <div class="input-module__inner-area"><input type="hidden" name="region" data-kladr-id="4100000000000"></div>
                <label for="name">{{ __('Name') }}</label>
                <div class="input-module__inner-area"><input type="text" class="input" name="name" value="{{ old('name') }}"></div>
                @if ($errors->has('name'))
                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                @endif
            </div>
        </div>
        <div class="input-module admin-panel-new-ntf__area-title">
            <div class="input-module__inner">
                <label for="email">E-mail</label>
                <div class="input-module__inner-area"><input type="email" class="input" name="email" value="{{ old('email') }}"></div>
                @if ($errors->has('email'))
                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                @endif
            </div>
        </div>
        <div class="input-module admin-panel-new-ntf__area-title">
            <div class="input-module__inner">
                <label for="phone">{{ __('Phone') }}</label>
                <div class="input-module__inner-area"><input type="phone" class="input" name="phone" value="{{ old('phone') }}"></div>
                @if ($errors->has('phone'))
                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                @endif
            </div>
        </div>
        <div class="input-module admin-panel-new-ntf__area-title">
            <div class="input-module__inner">
                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                <div class="input-module__inner-area"><input id="city" class="input @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required></div>
                {{--                                    <select id="city" class="input @error('city') is-invalid @enderror" name="city" value="{{ Auth::user()->adress->city }}" required>--}}
                {{--                                        <option @if (Auth::user()->adress->city === 'Усть-Камчатск') selected @endif>Усть-Камчатск</option>--}}
                {{--                                        <option @if (Auth::user()->adress->city === 'Ключи') selected @endif>Ключи</option>--}}
                {{--                                        <option @if (Auth::user()->adress->city === 'Козыревск') selected @endif>Козыревск</option>--}}
                {{--                                    </select>--}}

                @error('city')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="input-module admin-panel-new-ntf__area-title">
                <div class="input-module__inner">
                    <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>

                    <div class="input-module__inner-area"><input id="street" type="text" class="input @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" required
                                                                 autocomplete="street"></div>

                    @error('street')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="building" class="col-md-4 col-form-label text-md-right">{{ __('Building') }}</label>

                        <div class="input-module__inner-area"><input id="building" type="text" class="input @error('building') is-invalid @enderror" name="building"
                                                                     value="{{ old('building') }}" required autocomplete="building"></div>

                        @error('building')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="input-module admin-panel-new-ntf__area-title">
                        <div class="input-module__inner">
                            <label for="appartment" class="col-md-4 col-form-label text-md-right">{{ __('Appartment') }}</label>
                            <div class="input-module__inner-area"><input id="appartment" type="text" class="input @error('appartment') is-invalid @enderror" name="appartment"
                                                                         value="{{ old('appartment') }}" required autocomplete="appartment"></div>

                            @error('appartment')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="input-module admin-panel-new-ntf__area-title">
                            <div class="input-module__inner">
                                <label for="role_id">{{ __('Role') }}</label>
                                {{--                                {{ dd(\App\Role::all()->where('id', '<=', old('role_id))') }}--}}
                                <select class="input" name="role_id">
                                    @foreach(\App\Role::all()->where('id', '<=', Auth::user()->role_id) as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </div>
                            <div class="input-module admin-panel-new-ntf__area-title">
                                <div class="input-module__inner">

                                    <label for="password">{{ __('Password') }}</label>
                                    <div class="input-module__inner-area"><input type="password" class="input" name="password" value=""></div>
                                    @if ($errors->has('password'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </div>
                                <div class="input-module admin-panel-new-ntf__area-title">
                                    <div class="input-module__inner">
                                        <label for="password_confirm">Подтверждение пароля</label>
                                        <div class="input-module__inner-area"><input type="password" class="input" name="password_confirmation" value=""></div>
                                        @if ($errors->has('password_confirmation'))
                                            <b class="alert alert-danger">{{ $errors->first() }}</b>
                                        @endif
                                    </div>
                                    <button class="btn btn-warning" type="submit">Изменить</button>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        </section>

                        @stop

                        @section('kladr')
                            <link href="/css/jquery.fias.min.css" rel="stylesheet">
                            <script src="/js/jquery.fias.min.js" type="text/javascript"></script>
                            {{--    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
                            <script>
                                // Form example
                                (function () {
                                    var $container = $('.input-module__inner-area');//document.getElementById('formPublish'));

                                    var $tooltip = $('#tooltip');

                                    var $zip = $container.find('[name="zip"]'),
                                        $region = $container.find('[name="region"]'),
                                        $district = $container.find('[name="district"]'),
                                        $city = $container.find('[name="city"]'),
                                        $street = $container.find('[name="street"]'),
                                        $building = $container.find('[name="building"]');

                                    $()
                                        .add($region)
                                        .add($district)
                                        .add($city)
                                        .add($street)
                                        .add($building)
                                        .fias({
                                            parentInput: $('body').find('#formPublish'),
                                            verify: true,
                                            select: function (obj) {
                                                if (obj.zip) $zip.val(obj.zip);//Обновляем поле zip
                                                setLabel($(this), obj.type);
                                                $tooltip.hide();
                                            },
                                            check: function (obj) {
                                                var $input = $(this);

                                                if (obj) {
                                                    setLabel($input, obj.type);
                                                    $tooltip.hide();
                                                } else {
                                                    showError($input, 'Ошибка');
                                                }
                                            },
                                            checkBefore: function () {
                                                var $input = $(this);

                                                if (!$.trim($input.val())) {
                                                    $tooltip.hide();
                                                    return false;
                                                }
                                            }
                                        });

                                    $region.fias('type', $.fias.type.region);
                                    $district.fias('type', $.fias.type.district);
                                    $city.fias('type', $.fias.type.city);
                                    $street.fias('type', $.fias.type.street);
                                    $building.fias('type', $.fias.type.building);

                                    $district.fias('withParents', true);
                                    $city.fias('withParents', true);
                                    $street.fias('withParents', true);

                                    // Отключаем проверку введённых данных для строений
                                    $building.fias('verify', false);

                                    // Подключаем плагин для почтового индекса
                                    $zip.fiasZip($container);

                                    function setLabel($input, text) {
                                        text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                                        $input.parent().find('label').text(text);
                                    }

                                    function showError($input, message) {
                                        $tooltip.find('span').text(message);

                                        var inputOffset = $input.offset(),
                                            inputWidth = $input.outerWidth(),
                                            inputHeight = $input.outerHeight();

                                        var tooltipHeight = $tooltip.outerHeight();
                                        var tooltipWidth = $tooltip.outerWidth();

                                        $tooltip.css({
                                            left: (inputOffset.left + inputWidth - tooltipWidth) + 'px',
                                            top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
                                        });

                                        $tooltip.fadeIn();
                                    }

                                    function hideFreeVersion() {
                                        $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                                        setTimeout(function () {
                                            $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                                        }, 200);
                                    }

                                    $city.on('keyup', function () {
                                        hideFreeVersion();
                                    });
                                    $street.on('keyup', function () {
                                        hideFreeVersion();
                                    });
                                    $building.on('keyup', function () {
                                        hideFreeVersion();
                                    });

                                })();
                            </script>
                            <style>
                                /* Устанавливаем свой шрифт для выпадающего списка*/
                                #kladr_autocomplete a,
                                #kladr_autocomplete strong {
                                    font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
                                    font-size: 15px;
                                }

                                #kladr_autocomplete small {
                                    font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
                                    font-size: 13px;
                                }

                                /* Добавляем скругления и тень у выпадающего списка*/
                                #kladr_autocomplete ul {
                                    border-radius: 0 0 5px 5px;
                                    border: 1px solid #ded7f9;
                                    overflow: hidden;
                                    background: #fff;
                                    -webkit-box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
                                    box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
                                    z-index: 1;
                                }

                                /* Прописываем стили для тултипа с сообщением об ошибке*/
                                .tooltip {
                                    position: absolute;
                                    top: 16px;
                                    left: 360px;
                                    color: #b94a48;
                                    padding: 8px 10px;
                                    border-radius: 5px;
                                    border: 1px solid #eed3d7;
                                    background-color: #f2dede;
                                    opacity: 0.8;
                                    font-size: 14px;
                                    z-index: 100000;
                                }
                            </style>
@stop
