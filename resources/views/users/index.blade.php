@extends('layouts.admin')

@section('content')

    <section class="admin-panel-content">
        <form class="admin-panel-content__search">
            <input type="text" placeholder="Поиск...">
        </form>

        <div class="news-table">
            <div class="news-table__heading">
                <h3>Пользователи</h3>
                <a href="{{ route('users.create') }}" class="news-table__heading-new">
                    <svg class="icon icon-plus">
                        <use xlink:href="./img/sprites/main-sprite.svg#icon-plus"></use>
                    </svg>
                    <span>Новый</span>
                </a>
            </div>
            <div class="news-table-content">
                <table>
                    <thead>
                        <tr>
                            @if( Auth::user()->hasRoles(['super_admin']) )
                                <td>ID</td>
                            @endif
                            <td>{{ __('Name') }}</td>
                            <td>Email</td>
                            <td>{{ __('Phone') }}</td>
                            <td>{{ __('Role') }}</td>
    {{--                                    <td>{{ __('Score') }}</td>--}}

    {{--                                    @if ($user->hasRoles(['user']) )--}}
    {{--                                        <td>{{ __('Score') }}</td>--}}
    {{--                                        <td>{{ __('Class') }}</td>--}}
    {{--                                    @endif--}}

                            <td>{{ __('Action') }}</td>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $user)
                        <tr>
                            @if (Auth::user()->hasRoles(['super_admin']))
                                <td>{{ $user->id }}</td>
                            @endif
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td>
                                <span class="news-table-more">
                                    <a href="#pixel" class="news-table-more__trigger">
                                        <svg class="icon icon-more">
                                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-more"></use>
                                        </svg>
                                    </a>
                                    <span class="news-table-more__popup">
                                        <a href="{{ route('users.edit', $user->id) }}">Редактировать</a>
                                    </span>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
