<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title', 'Без названия') </title>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/site.js') }}" defer></script>--}}

    <!-- Fonts -->
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}
{{--    <script src="//use.fontawesome.com/3f9de7ff60.js"></script>--}}



    @include('layouts.bootstrap')
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    @yield('header')

</head>
<body>
<div id="app">
    @include('layouts.header')
    <main class="py-4">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h1>@yield('h1', 'Без названия')</h1>
                                @auth()
                                    @if (Auth::user()->hasRoles(['admin', 'super_admin']) )
                                        <a class="btn btn-success btn-sm edit-btn" href="{{ route('pages.edit', $page) }}">Редактировать</a>
                                    @endif
                                @endauth
                            </div>
                            <div class="card-body">
                                @yield('content')
                            </div>
                        </div>

                    </div>
            </div>
        </div>

    </main>


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-4 pull-left1">
                    <ul class="nav nav-pills.nav">
                        @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page)
                            <li><a class="nav-link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-sm-4 dropup">
                    <a class="dropdown-toggle d-none" data-toggle="dropdown" aria-expanded="false" href="#" style="text-decoration: none !important;">Язык<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/lang/ru">RU</a></li>
                        <li><a href="/lang/en">EN</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul class="social-network social-circle text-right ">
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>


            </div>
        </div>
    </footer>

</div>
@yield('footer')
@yield('script')
@include('layouts.scripts')
@yield('kladr')
</body>
</html>
