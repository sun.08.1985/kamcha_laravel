<script src="https://cdn.gravitec.net/storage/acf2a6c6c99ff086b9da82bc01f2c30b/client.js" async></script>
<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/inputmask.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/inputmask.extensions.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/inputmask.phone.extensions.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/phone-codes/phone.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/phone-codes/phone-be.js') }}"></script>
<script src="{{ asset('node_modules/maskedinput/inputmask/phone-codes/phone-ru.js') }}"></script>
<script>
    $('[name*="phone"]').click(function(){
        $(this).setCursorPosition(3);
    }).inputmask('+7 (999) 999-99-99');
</script>
@auth()
    <script>

        var lcity = '{{ Auth::user()->adress->city }}';
        var ul = '{{ Auth::user()->adress->street }}';
        var dom = '{{ Auth::user()->adress->building }}';
        var Gravitec = Gravitec || [];
        Gravitec.push([
            "segmentation.removeAllTags",
            function() {console.log("Метки удалены")},
            function(err){console.log(err.message)
            }])
        Gravitec.push(["registerUserForPush", function () {
        Gravitec.push([
            "segmentation.addTag",
            lcity
        ]);
        Gravitec.push([
            "segmentation.addTag",
            lcity + ', ' + ul
        ]);
        Gravitec.push([
            "segmentation.addTag",
            lcity + ', ' + ul + ', д.' + dom
        ]);
        }]);

    </script>
@endauth
