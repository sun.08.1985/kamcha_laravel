<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
{{--    <link rel="icon" sizes="16x16" href="/design/img/favicons/favicon-16x16.png" type="image/png">--}}
{{--    <link rel="icon" sizes="32x32" href="/design/img/favicons/favicon-32x32.png" type="image/png">--}}
{{--    <link rel="apple-touch-icon-precomposed" href="/design/img/favicons/apple-touch-icon-precomposed.png">--}}
{{--    <link rel="apple-touch-icon" href="/design/img/favicons/apple-touch-icon.png">--}}
{{--    <link rel="apple-touch-icon" sizes="57x57" href="/design/img/favicons/apple-touch-icon-57x57.png">--}}
{{--    <link rel="apple-touch-icon" sizes="60x60" href="/design/img/favicons/apple-touch-icon-60x60.png">--}}
{{--    <link rel="apple-touch-icon" sizes="72x72" href="/design/img/favicons/apple-touch-icon-72x72.png">--}}
{{--    <link rel="apple-touch-icon" sizes="76x76" href="/design/img/favicons/apple-touch-icon-76x76.png">--}}
{{--    <link rel="apple-touch-icon" sizes="114x114" href="/design/img/favicons/apple-touch-icon-114x114.png">--}}
{{--    <link rel="apple-touch-icon" sizes="120x120" href="/design/img/favicons/apple-touch-icon-120x120.png">--}}
{{--    <link rel="apple-touch-icon" sizes="144x144" href="/design/img/favicons/apple-touch-icon-144x144.png">--}}
{{--    <link rel="apple-touch-icon" sizes="152x152" href="/design/img/favicons/apple-touch-icon-152x152.png">--}}
{{--    <link rel="apple-touch-icon" sizes="167x167" href="/design/img/favicons/apple-touch-icon-167x167.png">--}}
{{--    <link rel="apple-touch-icon" sizes="180x180" href="/design/img/favicons/apple-touch-icon-180x180.png">--}}
{{--    <link rel="apple-touch-icon" sizes="1024x1024" href="/design/img/favicons/apple-touch-icon-1024x1024.png">--}}
<!-- connecting fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@700&display=swap" rel="stylesheet">
    <!-- connecting styles -->
    <link rel="stylesheet" href="/design/styles/main.min.css">
    <style>
        .header-logo {
            max-width: 100% !important;
        }
        .mobile-poll-list {
            display: none;
        }
        @media (max-width: 950px) {
            .mobile-poll-list {
                display: block;
            }
        }
        .mobile-poll {
            color: inherit !important;
        }
        input[type="submit"] {
            border: none !important;
        }

        .btn--white:active, .btn--white:focus {
            background-color: var(--light-periwinkle);
        }

        .btn--white:hover {
            background-color: var(--pale-grey-two);
        }
        .btn:active, .btn:focus {
            transition: 275ms;
        }
        .btn:hover {
            transition: 275ms;
        }
        .aside-quiz__slide-go input {
            width: 100%;
            display: block;
            text-align: center;
        }
    </style>
</head>

<body class="has-bg">
<!-- begin of page -->

<div class="wrapper">

    <!-- begin of header -->
    <header class="header">
        <div class="header-wrap limiter">
            <div class="header-logo"><a href="/"><img src="/favicon.ico">{{ config('app.name', 'Laravel') }}</a></div>
            <div class="header-notifications">
                <button href="#" class="header-notifications__trigger ntf__trigger">
                    <svg class="icon icon-ntf">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-ntf"></use>
                    </svg>
                    <span></span>
                </button>
            </div>
            <button class="header-search__trigger">
                <svg class="icon icon-search">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-search"></use>
                </svg>
            </button>
            <div class="header-search">
                <form class="search__inner" action="/">
                    <button type="submit">
                        <svg class="icon icon-search">
                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-search"></use>
                        </svg>
                    </button>
                    <input type="text" placeholder="Поиск" name="q">
                </form>
            </div>
            <div class="header-auth">
                @auth()
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <div class="header-profile">
                        <button class="{{ (Auth::user()->avatar == 'default.jpg')? '': 'avatar' }} header-profile__trigger" id="profilePopupTrigger" style="background-size: cover; background-image: url(/uploads/avatars/{{ Auth::user()->avatar }})">
                            <span>{{ (Auth::user()->avatar == 'default.jpg')? 'Аккаунт': '' }}</span>
                        </button>
                        <div class="header-profile-popup">
                            <div class="header-profile-popup__info">
                                <span>{{ Auth::user()->name }}</span>
                                <span>{{ Auth::user()->email }}</span>
                            </div>
                            <a href="{{ route('admin') }}" class="header-profile-popup__link">Профиль</a>
                            <a class="header-profile-popup__link" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                        </div>
                    </div>
                @endauth
                @guest()
{{--                <button class="header-auth__trigger" id="authTrigger">Войти</button>--}}
                    <button class="header-auth__trigger call-login" id="authTrigger">Войти</button>
{{--                <a href="{{ route('login') }}" class="header-auth__trigger" id="authTrigger">Войти</a>--}}
{{--                <a href="{{ route('register') }}" class="header-auth__trigger" id="authTrigger">Зарегистрироваться</a>--}}
                @endguest
            </div>
            <button class="header-burger">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
    </header>
    <!-- end of header -->
    <div class="mobile-menu-overlay">
        <div class="mobile-menu">
            <div class="mobile-menu-profile">
                @auth()
                        <a href="{{ route('admin') }}" class="btn btn--blue mobile-menu-profile__auth">Профиль</a>
                        <a class="btn btn--blue mobile-menu-profile__auth" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                @endauth
                @guest()
                    <a href="#" class="btn btn--blue mobile-menu-profile__auth call-login">Войти</a>
                @endguest
            </div>
            <nav class="mobile-menu-nav">
                <a class="mobile-menu-nav__link" href="/">Свежее</a>
                @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page_menu)
                    <a class="mobile-menu-nav__link" href="/{{ $page_menu->slug }}">{{ $page_menu->title }}</a>
                @endforeach
            </nav>
            <nav class="modile-menu-subnav">
                @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page_menu)
                    <a class="mobile-menu-subnav__link" href="/{{ $page_menu->slug }}">{{ $page_menu->title }}</a>
                @endforeach
            </nav>
            <div class="mobile-menu-socials">
                <a href="#" class="mobile-menu-socials__link">
                    <svg class="icon icon-social-ok">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-ok"></use>
                    </svg>
                </a>
                <a href="#" class="mobile-menu-socials__link">
                    <svg class="icon icon-social-vk">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-vk"></use>
                    </svg>
                </a>
                <a href="#" class="mobile-menu-socials__link">
                    <svg class="icon icon-social-fb">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-fb"></use>
                    </svg>
                </a>
                <a href="#" class="mobile-menu-socials__link">
                    <svg class="icon icon-social-twitter">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-twitter"></use>
                    </svg>
                </a>
            </div>
            <div class="mobile-menu-copyrights">
                © 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены.
            </div>
        </div>
    </div>
    <!-- begin of main -->
    <main class="main page-main">
        <div class="main-wrap limiter">
            <div class="main-notifications">
                <div class="notifications-title">
                    <h4>Уведомления</h4>
                    <button class="notifications-close ntf__trigger">
                        <svg class="icon icon-cross">
                            <use xlink:href="./img/sprites/main-sprite.svg#icon-cross"></use>
                        </svg>
                    </button>
                </div>
{{--                <div class="notifications-manage">--}}
{{--                    <div class="notifications-manage__count">12 новых</div>--}}
{{--                    <a href="#" class="notifications-manage__check">Отметить все как прочитанные</a>--}}
{{--                </div>--}}
{{--                <div class="notification seen">--}}
{{--                    <div class="notification-content">На следующей неделе ожидается похолодание. Одевайтесь теплее.</div>--}}
{{--                    <button class="notification__close"></button>--}}
{{--                </div>--}}
{{--                <div class="notification">--}}
{{--                    <div class="notification-content">Данный экземпляр системы является DEV версией.</div>--}}
{{--                    <button class="notification__close"></button>--}}
{{--                </div>--}}
            </div>
            <div class="main-nav">
                <div class="nav">
                    @yield('menu_left')
                </div>
            </div>
            <div class="main-content">
                <div class="mobile-poll-list">

                    @if(Inani\Larapoll\Poll::count() >= 1)
                        @foreach(Inani\Larapoll\Poll::limit(1)->get() as $poll)

                            <div class="mobile-poll">
                                {{ PollWriterRemod::draw(Inani\Larapoll\Poll::find($poll->id)) }}
                            </div>

                        @endforeach
                    @else
                        Активных опросов нет
                    @endif
                </div>
                @yield('content')
            </div>
            @yield('sidebar_right')
            @yield('more_news')
        </div>
    </main>
    <!-- end of main -->

    <!-- begin of footer -->
    <footer class="footer">
        <div class="footer-wrap limiter">
            <nav class="footer-nav">
                @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page_menu)
                    <a class="footer-nav__link" href="/{{ $page_menu->slug }}">{{ $page_menu->title }}</a>
                @endforeach
{{--                <a href="/about" class="footer-nav__link">О проекте</a>--}}
{{--                <a href="#" class="footer-nav__link">Обращение граждан</a>--}}
{{--                <a href="#" class="footer-nav__link">Часто задаваемые вопросы</a>--}}
{{--                <a href="#" class="footer-nav__link">Политика конфиденциальности</a>--}}
            </nav>
            <div class="footer-socials">
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-ok">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-ok"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-vk">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-vk"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-fb">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-fb"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-twitter">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-twitter"></use>
                    </svg>
                </a>
            </div>
            <div class="footer-copyrights">
                © 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены.
            </div>
        </div>
    </footer>
    <!-- end of footer -->

</div>

<!-- end of page -->

<!-- modals -->
<div class="modals-overlay">
    <div class="modal modal-reg" style="display: none;">
        <div class="modal-title">
            <h3>Регистрация</h3>
        </div>
        <form class="modal-form" action="{{ route('register') }}" method="POST" id="registerForm">
            @csrf
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Имя</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input" name="name" required>
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Фамилия</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input" name="firstname">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Электронная почта</label>
                    <div class="input-module__inner-area">
                        <input type="email" class="input" name="email">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module input-module--hidden-symbols">
                <div class="input-module__inner">
                    <label for="in5">Пароль</label>
                    <div class="input-module__inner-area">
                        <input type="password" class="input" name="password" required autocomplete="new-password">
                        <button type="button">
                            <svg class="icon icon-eye">
                                <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                            </svg>
                        </button>
                    </div>
                    <label for="in5">Подтверждение пароля</label>
                    <div class="input-module__inner-area">
                        <input type="password" class="input" name="password_confirmation" required autocomplete="new-password">
                        <button type="button">
                            <svg class="icon icon-eye">
                                <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                            </svg>
                        </button>
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module__manage">
                <span class="is-invalid" id="errorregister"></span>
            </div>
            <a href="{{ route('register') }}" class="btn btn--blue">Зарегистрироваться</a>
{{--            <button type="submit" class="btn btn--blue">Зарегистрироваться</button>--}}
        </form>
        <div class="modal-reg__login">
            <span>Уже зарегистрированы?</span>
            <button class="btn btn--gray call-login">Войти</button>
        </div>
        <div class="modal-reg__privacy">
            Регистрируясь, вы принимаете условия <a href="#">Политики&nbsp;конфиденциальности</a>
        </div>
        <button class="modal-close"></button>
    </div>
    <div class="modal modal-login" style="display: none;">
        <div class="modal-title">
            <h3>Войти</h3>
        </div>
        <form class="modal-form" action="{{ route('login') }}" method="POST" id="loginForm">
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Электронная почта</label>
                    <div class="input-module__inner-area">
                        <input type="email" class="input" name="email" required>
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module input-module--hidden-symbols">
                <div class="input-module__inner">
                    <label for="in5">Пароль</label>
                    <div class="input-module__inner-area">
                        <input type="password" class="input" name="password" required>
                        <button type="button">
                            <svg class="icon icon-eye">
                                <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                            </svg>
                        </button>
                    </div>
                    <span></span>
                </div>
                <div class="input-module__manage">
                    <span class="is-invalid" id="errorLogin"></span>
                </div>
                <div class="input-module__manage">
                    <a href="{{ route('password.request') }}">Не помню пароль</a>
                </div>
            </div>
            <button type="submit" class="btn btn--blue">Войти</button>
        </form>
        <div class="modal-login__reg">
            <a href="{{ route('register') }}" class="btn btn--gray">Зарегистрироваться</a>
{{--            <button class="btn btn--gray call-reg">Зарегистрироваться</button>--}}
        </div>

{{--        <div class="modal-login__socials">--}}
{{--            <span>Или войдите с помощью</span>--}}
{{--            <a href="#" class="modal-login__socials-link">--}}
{{--                <svg class="icon icon-social-ok">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-ok"></use>--}}
{{--                </svg>--}}
{{--            </a>--}}
{{--            <a href="#" class="modal-login__socials-link">--}}
{{--                <svg class="icon icon-social-vk">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-vk"></use>--}}
{{--                </svg>--}}
{{--            </a>--}}
{{--            <a href="#" class="modal-login__socials-link">--}}
{{--                <svg class="icon icon-social-fb">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-fb"></use>--}}
{{--                </svg>--}}
{{--            </a>--}}
{{--            <a href="#" class="modal-login__socials-link">--}}
{{--                <svg class="icon icon-social-twitter">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-twitter"></use>--}}
{{--                </svg>--}}
{{--            </a>--}}
{{--        </div>--}}
        <button class="modal-close"></button>
    </div>
    <div class="modal modal-share" style="display: none;">
        <div class="modal-title">
            <h3>Поделиться</h3>
        </div>
        <p style="margin: 0 10px;"><script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="https://yastatic.net/share2/share.js"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,viber,whatsapp,skype,telegram" data-size="s"></div></p>
        <button class="modal-close"></button>
    </div>
</div>

<!-- connecting scripts -->
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/app.js" defer></script>
<script src="/design/js/main.min.js" defer></script>
<script src="/design/js/vendor.min.js"></script>

@yield('kladr')
@yield('script')
@include('layouts.scripts')
</body>

</html>
