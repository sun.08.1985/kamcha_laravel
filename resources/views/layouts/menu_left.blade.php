<a class="nav__link" href="/">Свежее</a>
@foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page_menu)
    <a class="nav__link" href="/{{ $page_menu->slug }}">{{ $page_menu->title }}</a>
@endforeach
