<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/favicon.ico">{{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <i class="fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth()
                    @if (Auth::user()->hasRoles(['super_user', 'editor', 'author', 'admin', 'super_admin']) )
                        @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
                            <li class="nav-item"><a class="nav-link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                        @endforeach
{{--                    @else--}}
{{--                        <li class="nav-item"><a class="nav-link" href="{{ route('exams.list')  }}">Опросы</a></li>--}}
                    @endif
                @endauth
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        @if (Auth::user()->hasRoles(['user']) )
                            <span>Баланс: <strong>{{ Auth::user()->score }}</strong> баллов</span>
                        @elseif (Auth::user()->hasRoles(['super_user']) )
                            <span>Баланс: <strong>{{ Auth::user()->score ?? '0' }}</strong> оценок</span>
                        @endif

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('admin') }}">
                                {{ __('Admin') }}
                            </a>
                            <hr>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
