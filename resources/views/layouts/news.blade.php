@extends('design.layout')

@section('content')
    <div class="col-md-8 col-lg-6">
    <section class="content news">
        <div class="container">
            <div class="main">
                <div class="row flex-wrap">
                    <div class="col-lg-3">
                        <div class="sidebar-menu">
                            <nav class="main-nav navbar-right navbar-expand-lg" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="nav-item"><a class="main-nav_link" href="#">Популярное</a></li>
                                    <li class="nav-item"><a class="main-nav_link" href="#">Свежее</a></li>
                                    <li class="nav-item"><a class="main-nav_link" href="#">Опросы</a></li>
                                    <li class="nav-item"><a class="main-nav_link" href="#">Рядом</a></li>
                                    <li class="nav-item mobile-collaps"><a class="main-nav_link" href="#">Голосования</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-6">
                        <div class="content__articles">
                            <div class="news__item_content">
                                <div class="news__item_box-padding">
                                    <div class="articles__item_text_label">
                                        <a href="#" class="articles_label_category">О городе</a>
                                        <span class="articles_label_dayago">16 апреля</span>
                                    </div>

                                    <h1 class="news__item_title">Длинный заголовок, который может занимать две строки и больше</h1>

                                    <p>Лид, если есть. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности в значительной степени обуславливает создание модели развития. Не следует, однако забывать, что рамки и место обучения кадров позволяет выполнять важные задания по разработке форм развития.</p>
                                </div>
                                <div class="news__item_img">
                                    <img src="img/img1.jpg" alt="">
                                    <p class="news__item_img_label">Подпись картинки</p>
                                </div>
                                <div class="news__item_box-padding">
                                    <p>Не следует, однако забывать, что сложившаяся структура организации способствует подготовки и реализации модели развития. Равным образом постоянный количественный рост и сфера нашей активности в значительной степени обуславливает создание дальнейших направлений развития. Значимость этих проблем настолько очевидна, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании системы обучения кадров, соответствует насущным потребностям.<br>С другой стороны новая модель организационной деятельности влечет за собой процесс внедрения и модернизации соответствующий условий активизации. Разнообразный и богатый опыт новая модель организационной деятельности позволяет оценить значение существенных финансовых и административных условий. Таким образом консультация с широким активом требуют от нас анализа направлений прогрессивного развития.</p>

                                    <h2>Длинный заголовок второго уровня, который может занимать две строки и больше</h2>

                                    <p>Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки направлений прогрессивного развития. Разнообразный и богатый опыт постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки направлений прогрессивного развития. Повседневная практика показывает, что постоянное информационно-пропагандистское обеспечение нашей деятельности в значительной степени обуславливает создание направлений прогрессивного развития.</p>

                                    <p class="quote">Цитата. Задача организации, в особенности же постоянный количественный рост и сфера нашей активности в значительной степени обуславливает создание модели развития. Не следует, однако забывать, что рамки и место обучения кадров позволяет выполнять важные задания по разработке форм развития.</p>

                                    <h3 class="list_title">Пример списка с буллитами</h3>

                                    <ul class="list_bullets">
                                        <li>Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки направлений прогрессивного развития.</li>
                                        <li>Разнообразный и богатый опыт постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки направлений прогрессивного развития.</li>
                                        <li>Повседневная практика показывает, что постоянное информационно-пропагандистское обеспечение нашей деятельности в значительной степени обуславливает создание направлений прогрессивного развития.</li>
                                    </ul>

                                    <h3 class="list_title">Пример цифрового списка</h3>

                                    <ol class="list_numeral">
                                        <li>Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности представляет собой интересный эксперимент проверки направлений прогрессивного развития.</li>
                                        <li>Разнообразный и богатый опыт постоянный количественный рост и сфера нашей активности представляет собой интересный эксперимент проверки направлений прогрессивного развития.</li>
                                        <li>Повседневная практика показывает, что постоянное информационно-пропагандистское обеспечение нашей деятельности в значительной степени обуславливает создание направлений прогрессивного развития.</li>
                                    </ol>
                                </div>
                            </div>
                            <div class="news__item_coment">
                                <h4>5 комментариев</h4>
                                <div class="news__item_coment_user">
                                    <form action="coment_user.php" method="post">
                                        <div class="news__item_coment_user_avatar_box">
                                            <div class="news__item_coment_user_avatar">
                                                <span class="news__item_coment_user_avatar_icon">И</span>
                                            </div>
                                        </div>
                                        <input id="news_coment" type="text" name="news_coment" value="" placeholder="Оставить комментарий" required="">
                                    </form>
                                </div>

                                <div class="news__item_coments_box">
                                    <div class="discussions_item">
                                        <div class="discussions_item_user">
                                            <div class="discussions_item_user_avatar">
                                                <img src="img/user2.jpg" alt="">
                                            </div>
                                            <span class="discussions_item_user_name">Максим Малыгин</span>
                                            <span class="discussions_item_user_data">27.04.2020</span>
                                        </div>
                                        <p class="discussions_item_text">Не следует, однако забывать, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития.</p>
                                    </div>
                                    <div href="#" class="discussions_item">
                                        <div class="discussions_item_user">
                                            <div class="discussions_item_user_avatar">
                                                <img src="img/user2.jpg" alt="">
                                            </div>
                                            <span class="discussions_item_user_name">Клавдия Суворова</span>
                                            <span class="discussions_item_user_data">13 дней назад</span>
                                        </div>
                                        <p class="discussions_item_text">Значимость этих проблем настолько очевидна, что консультация с широким активом требуют от нас анализа существенных финансовых и административных условий.</p>
                                    </div>
                                    <div href="#" class="discussions_item">
                                        <div class="discussions_item_user">
                                            <div class="discussions_item_user_avatar">
                                                <img src="img/user2.jpg" alt="">
                                            </div>
                                            <span class="discussions_item_user_name">Зинаида Емельянова</span>
                                            <span class="discussions_item_user_data">5 дней назад</span>
                                        </div>
                                        <p class="discussions_item_text">С другой стороны дальнейшее развитие различных форм деятельности требуют определения и уточнения позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также укрепление и развитие структуры позволяет оценить значение модели развития.</p>
                                    </div>
                                    <div href="#" class="discussions_item">
                                        <div class="discussions_item_user">
                                            <div class="discussions_item_user_avatar">
                                                <img src="img/user2.jpg" alt="">
                                            </div>
                                            <span class="discussions_item_user_name">Максим Малыгин</span>
                                            <span class="discussions_item_user_data">16 минут назад</span>
                                        </div>
                                        <p class="discussions_item_text">Не следует, однако забывать, что постоянный количественный рост.</p>
                                    </div>
                                    <div href="#" class="discussions_item">
                                        <div class="discussions_item_user">
                                            <div class="discussions_item_user_avatar">
                                                <img src="img/user2.jpg" alt="">
                                            </div>
                                            <span class="discussions_item_user_name">Клавдия Суворова</span>
                                            <span class="discussions_item_user_data">2 минуты назад</span>
                                        </div>
                                        <p class="discussions_item_text">Значимость этих проблем настолько очевидна, что консультация с широким активом требуют от нас анализа существенных финансовых и административных условий.</p>
                                    </div>
                                </div>
                                <div class="news__item_coment_user">
                                    <form action="coment_user.php" method="post">
                                        <div class="news__item_coment_user_avatar_box">
                                            <div class="news__item_coment_user_avatar">
                                                <span class="news__item_coment_user_avatar_icon">И</span>
                                            </div>
                                        </div>
                                        <input id="news_coment" type="text" name="news_coment" value="" placeholder="Оставить комментарий" required="">
                                        <div class="btn_box">
                                            <button class="btn_bg">Отправить</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3 d-none d-md-block">
                        <div class="sidebar-right">
                            <div class="sidebar__poll">
                                <div class="sidebar__poll_label">
                                    <span>Опрос</span>
                                </div>

                                <div class="sidebar__poll_slick">
                                    <div class="sidebar__poll_item">
                                        <h3 class="sidebar__poll_title">Как вы относитесь к администрации города?</h3>
                                        <p class="sidebar__poll_text">Расскажите нам о вашем отношении к представителям власти</p>
                                        <a href="#" class="btn_bg">Пройти опрос</a>
                                    </div>
                                    <div class="sidebar__poll_item">
                                        <h3 class="sidebar__poll_title">Как вы относитесь к администрации города?</h3>
                                        <p class="sidebar__poll_text">Расскажите нам о вашем отношении к представителям власти</p>
                                        <a href="#" class="btn_bg">Пройти опрос</a>
                                    </div>
                                    <div class="sidebar__poll_item">
                                        <h3 class="sidebar__poll_title">Как вы относитесь к администрации города?</h3>
                                        <p class="sidebar__poll_text">Расскажите нам о вашем отношении к представителям власти</p>
                                        <a href="#" class="btn_bg">Пройти опрос</a>
                                    </div>
                                    <div class="sidebar__poll_item">
                                        <h3 class="sidebar__poll_title">Как вы относитесь к администрации города?</h3>
                                        <p class="sidebar__poll_text">Расскажите нам о вашем отношении к представителям власти</p>
                                        <a href="#" class="btn_bg">Пройти опрос</a>
                                    </div>
                                    <div class="sidebar__poll_item">
                                        <h3 class="sidebar__poll_title">Как вы относитесь к администрации города?</h3>
                                        <p class="sidebar__poll_text">Расскажите нам о вашем отношении к представителям власти</p>
                                        <a href="#" class="btn_bg">Пройти опрос</a>
                                    </div>
                                </div>

                                <div class="sidebar__poll_nav">
                                    <a href="#" class="arrow sidebar__poll_nav_prev"><i class="icon-left-dir"></i></a>
                                    <p class="sidebar__poll_nav_text">Опрос <span class="sidebar__poll_nav_show">2</span> из <span class="sidebar__poll_nav_total">5</span></p>
                                    <a href="#" class="arrow sidebar__poll_nav_next"><i class="icon-right-dir"></i></a>
                                </div>
                            </div>
                            <div class="sidebar__discussions">
                                <h3 class="sidebar__discussions_title">Активно обсуждают</h3>

                                <a href="#" class="sidebar__discussions_item">
                                    <div class="discussions_item_user">
                                        <div class="discussions_item_user_avatar">
                                            <img src="img/user2.jpg" alt="">
                                        </div>
                                        <span class="discussions_item_user_name">Максим Малыгин</span>
                                    </div>
                                    <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                                    <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
                                </a>

                                <a href="#" class="sidebar__discussions_item">
                                    <div class="discussions_item_user">
                                        <div class="discussions_item_user_avatar">
                                            <img src="img/user.jpg" alt="">
                                        </div>
                                        <span class="discussions_item_user_name">Клавдия Суворова</span>
                                    </div>
                                    <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                                    <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
                                </a>

                                <a href="#" class="sidebar__discussions_item">
                                    <div class="discussions_item_user">
                                        <div class="discussions_item_user_avatar">
                                            <img src="img/user2.jpg" alt="">
                                        </div>
                                        <span class="discussions_item_user_name">Максим Малыгин</span>
                                    </div>
                                    <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                                    <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
                                </a>

                                <a href="#" class="sidebar__discussions_item">
                                    <div class="discussions_item_user">
                                        <div class="discussions_item_user_avatar">
                                            <img src="img/user2.jpg" alt="">
                                        </div>
                                        <span class="discussions_item_user_name">Максим Малыгин</span>
                                    </div>
                                    <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                                    <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
                                </a>

                                <a href="#" class="sidebar__discussions_item">
                                    <div class="discussions_item_user">
                                        <div class="discussions_item_user_avatar">
                                            <img src="img/user2.jpg" alt="">
                                        </div>
                                        <span class="discussions_item_user_name">Максим Малыгин</span>
                                    </div>
                                    <p class="discussions_item_text">Салюта не было((( И это 75 лет Победы. Позор-позор. И людей практически не был…</p>
                                    <h4 class="discussions_item_theme">Праздник 9 мая в городе</h4>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="more-news">
        <div class="container">
            <div class="row">
                <div class="col-10 offset-2">
                    <h5>Еще новости на тему «О городе»</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a href="#" class="more-news_item">
                        <div class="more-news_item_img">
                            <img src="img/img1.jpg" alt="">
                        </div>
                        <h3 class="more-news_item_title">Задача организации, в особенности же постоянный количественный рост </h3>
                        <div class="more-news_item_info">
                            <span class="data">16 апреля</span><span class="deco"> • </span><span class="views">456</span><span> просмотров</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="more-news_item">
                        <div class="more-news_item_img">
                            <img src="img/img1.jpg" alt="">
                        </div>
                        <h3 class="more-news_item_title">Рамки и место обучения кадров позволяет выполнять важные задания по разработке фор…</h3>
                        <div class="more-news_item_info">
                            <span class="data">16 апреля</span><span class="deco"> • </span><span class="views">456</span><span> просмотров</span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="#" class="more-news_item">
                        <div class="more-news_item_img">
                            <img src="img/img1.jpg" alt="">
                        </div>
                        <h3 class="more-news_item_title">Сложившаяся структура организации способствует подготовки</h3>
                        <div class="more-news_item_info">
                            <span class="data">16 апреля</span><span class="deco"> • </span><span class="views">456</span><span> просмотров</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
