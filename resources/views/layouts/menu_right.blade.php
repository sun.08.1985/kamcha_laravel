<aside class="main-aside">
    <div class="aside">
        <div class="aside-quiz">
            <div class="aside-quiz-track">
                @if(Inani\Larapoll\Poll::count() >= 1)

                <div style="display:none;">{{ $i = true }}</div>
                @foreach(Inani\Larapoll\Poll::limit(1)->get() as $poll)

                <div class="aside-quiz__slide {{ $i ?? 'is-current' }}">
                    {{ PollWriterRemod::draw(Inani\Larapoll\Poll::find($poll->id)) }}
                </div>

                {{ $i = false }}

                @endforeach
                @else
                Активных опросов нет
                @endif

            </div>
            <div class="aside-quiz-controls">
                <button class="aside-quiz__prev" id="quizPrev"></button>
                <div class="aside-quiz-status">
                    Опрос <span class="aside-quiz-status__current">1</span> из <span class="aside-quiz-status__amount">5</span>
                </div>
                <button class="aside-quiz__next" id="quizNext"></button>
            </div>
        </div>
        <div class="aside-latest">
            <div class="aside-latest__title">
                <h4>Активно обсуждают</h4>
            </div>
            @foreach($news_comment as $news_comment_one)
            <div class="aside-latest__item">
                <div class="avatar aside-latest__item-avatar" style="background-size: cover; background-image: url(/uploads/avatars/{{ $news_comment_one->commenter->avatar }})">
                    <span>{{ ($news_comment_one->commenter->avatar == 'default.jpg')? Str::limit($news_comment_one->commenter->name, 1, ''): '' }}</span>
                </div>
                <a href="{{ route('news.slug', $news_comment_one->commentable->slug) }}#comment-{{ $news_comment_one->id }}" class="aside-latest__item-name">{{ $news_comment_one->commenter->name }}</a>
                <p class="lighter aside-latest__item-content">{!! Str::limit($news_comment_one->comment, 200) !!}</p>

                <a href="{{ route('news.slug', $news_comment_one->commentable->slug) }}#comment-{{ $news_comment_one->id }}" class="aside-latest__item-category">{{ $news_comment_one->commentable->title }}</a>
            </div>
            {{--                    {!! $news_comment_one->comment !!}--}}
            @endforeach
        </div>
    </div>
</aside>
