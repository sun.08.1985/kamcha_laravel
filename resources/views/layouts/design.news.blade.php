
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="theme-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
{{--    <link rel="icon" sizes="16x16" href="/design/img/favicons/favicon-16x16.png" type="image/png">--}}
{{--    <link rel="icon" sizes="32x32" href="/design/img/favicons/favicon-32x32.png" type="image/png">--}}
{{--    <link rel="apple-touch-icon-precomposed" href="/design/img/favicons/apple-touch-icon-precomposed.png">--}}
{{--    <link rel="apple-touch-icon" href="/design/img/favicons/apple-touch-icon.png">--}}
{{--    <link rel="apple-touch-icon" sizes="57x57" href="/design/img/favicons/apple-touch-icon-57x57.png">--}}
{{--    <link rel="apple-touch-icon" sizes="60x60" href="/design/img/favicons/apple-touch-icon-60x60.png">--}}
{{--    <link rel="apple-touch-icon" sizes="72x72" href="/design/img/favicons/apple-touch-icon-72x72.png">--}}
{{--    <link rel="apple-touch-icon" sizes="76x76" href="/design/img/favicons/apple-touch-icon-76x76.png">--}}
{{--    <link rel="apple-touch-icon" sizes="114x114" href="/design/img/favicons/apple-touch-icon-114x114.png">--}}
{{--    <link rel="apple-touch-icon" sizes="120x120" href="/design/img/favicons/apple-touch-icon-120x120.png">--}}
{{--    <link rel="apple-touch-icon" sizes="144x144" href="/design/img/favicons/apple-touch-icon-144x144.png">--}}
{{--    <link rel="apple-touch-icon" sizes="152x152" href="/design/img/favicons/apple-touch-icon-152x152.png">--}}
{{--    <link rel="apple-touch-icon" sizes="167x167" href="/design/img/favicons/apple-touch-icon-167x167.png">--}}
{{--    <link rel="apple-touch-icon" sizes="180x180" href="/design/img/favicons/apple-touch-icon-180x180.png">--}}
{{--    <link rel="apple-touch-icon" sizes="1024x1024" href="/design/img/favicons/apple-touch-icon-1024x1024.png">--}}
<!-- connecting fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@700&display=swap" rel="stylesheet">
    <!-- connecting styles -->
    <link rel="stylesheet" href="/design/styles/main.min.css">
</head>

<body>
<!-- begin of page -->

<div class="wrapper">

    <!-- begin of header -->
    <header class="header">
        <div class="header-wrap limiter">
            <div class="header-logo">{{ config('app.name', 'Laravel') }}</div>
            <div class="header-search">
                <form class="search__inner">
                    <button type="submit">
                        <svg class="icon icon-search">
                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-search"></use>
                        </svg>
                    </button>
                    <input type="text" placeholder="Поиск">
                </form>
            </div>
            <div class="header-auth">
                <button class="header-auth__trigger" id="authTrigger">Войти</button>
            </div>
        </div>
    </header>
    <!-- end of header -->

    <!-- begin of main -->
    <main class="main page-contacts">
        <div class="main-wrap limiter">
            <div class="main-nav">
                <div class="nav">
                    {{--                    <a href="#" class="nav__link current">Популярное</a>--}}
                    @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
                        <a class="nav__link" href="/{{ $page->slug }}">{{ $page->title }}</a>
                    @endforeach
                    {{--                    <li class=""><a class="modal_burger-nav_link" href="/">Свежее</a></li>--}}
                    {{--                    <a href="#" class="nav__link">Свежее</a>--}}
                    {{--                    <a href="#" class="nav__link">Опросы</a>--}}
                    {{--                    <a href="#" class="nav__link">Рядом</a>--}}
                </div>
            </div>
            <div class="main-content">
                @yield('content')
            </div>
        </div>
    </main>
    <!-- end of main -->

    <!-- begin of footer -->
    <footer class="footer">
        <div class="footer-wrap limiter">
            <nav class="footer-nav">
                @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page)
                    <a class="nav__link" href="/{{ $page->slug }}">{{ $page->title }}</a>
                @endforeach
                <a href="/about" class="footer-nav__link">О проекте</a>
                <a href="#" class="footer-nav__link">Обратная связь</a>
                <a href="#" class="footer-nav__link">Часто задаваемые вопросы</a>
                <a href="#" class="footer-nav__link">Политика конфиденциальности</a>
            </nav>
            <div class="footer-socials">
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-ok">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-ok"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-vk">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-vk"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-fb">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-fb"></use>
                    </svg>
                </a>
                <a href="#" class="footer-socials__link">
                    <svg class="icon icon-social-twitter">
                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-twitter"></use>
                    </svg>
                </a>
            </div>
            <div class="footer-copyrights">
                © 2019-{{ \Carbon\Carbon::now()->format('Y') }}, {{ config('app.name', 'Laravel') }}. Все права защищены.
            </div>
        </div>
    </footer>
    <!-- end of footer -->

</div>

<!-- end of page -->

<!-- modals -->
<div class="overlay"></div>

<!-- connecting scripts -->
<script src="/design/js/main.min.js"></script>
<script src="/design/js/vendor.min.js"></script>
</body>

</html>
