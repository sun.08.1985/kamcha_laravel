<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('layouts.bootstrap')
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script src="https://cdn.gravitec.net/storage/acf2a6c6c99ff086b9da82bc01f2c30b/client.js" async></script>
    <!-- Fonts -->
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}
{{--    <script src="//use.fontawesome.com/3f9de7ff60.js"></script>--}}

    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js" charset="utf-8"></script>
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

        <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    @yield('header')

</head>
<body>
<div id="app">
    @include('layouts.header')
    <main class="py-4">
        <div class="container-fluid">
            <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-3">
                        <a href="{{ route('admin') }}" class="btn btn-outline-dark btn-block">Главная</a>
                        <hr>
                        @auth()

                            @if (Auth::user()->hasRoles(['author', 'editor', 'super_admin', 'admin']) )
                                <a class="btn btn-{{ (strpos(Route::currentRouteName(), 'pages') !== false) ? '' : 'outline-' }}dark btn-block mt-2" href="{{ route('pages.index') }}">Страницы</a>
{{--                                <button class="btn btn-outline-dark btn-block dropdown-toggle mt-2" type="button" data-toggle="collapse" data-target="#pages" aria-expanded="false" aria-controls="collapseExample">--}}
{{--                                    Страницы--}}
{{--                                </button>--}}
{{--                                <div class="collapse {{ (strpos(Route::currentRouteName(), 'pages') !== false) ? 'show' : '' }}" id="pages">--}}
{{--                                    <div class="card card-body">--}}
{{--                                        @foreach(\App\Page::orderBy('order', 'ASC')->limit(10)->get() as $page)--}}
{{--                                            <a class="nav-link" href="{{ route('pages.edit', $page->id) }}">{{ $page->title }}</a>--}}
{{--                                        @endforeach--}}
{{--                                        <a class="btn btn-dark" href="{{ route('pages.index') }}">Все страницы</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            @endif

                            @if (Auth::user()->hasRoles(['author', 'editor', 'super_admin', 'admin']) )
                                    <a class="btn btn-{{ (strpos(Route::currentRouteName(), 'news') !== false) ? '' : 'outline-' }}dark btn-block mt-2" href="{{ route('news.index') }}">Новости</a>
{{--                                <button class="btn btn-outline-dark btn-block dropdown-toggle mt-2" type="button" data-toggle="collapse" data-target="#news" aria-expanded="false" aria-controls="collapseExample">--}}
{{--                                    Новости--}}
{{--                                </button>--}}
{{--                                <div class="collapse {{ (strpos(Route::currentRouteName(), 'news') !== false) ? 'show' : '' }}" id="news">--}}
{{--                                    <div class="card card-body">--}}
{{--                                        @foreach(\App\News::orderBy('order', 'ASC')->limit(10)->get() as $page)--}}
{{--                                            <a class="nav-link" href="{{ route('news.edit', $page->id) }}">{{ $page->title }}</a>--}}
{{--                                        @endforeach--}}
{{--                                        <a class="btn btn-dark" href="{{ route('news.index') }}">Все новости</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            @endif

                            @if (Auth::user()->hasRoles(['author', 'editor', 'super_admin', 'admin']) )
                                <a class="btn btn-outline-dark btn-block mt-2" href="{{ route('poll.index') }}">Опросы</a>
                            @endif

{{--                            @if (Auth::user()->hasRoles(['author', 'editor', 'super_admin', 'admin']) )--}}
{{--                                <button class="btn btn-outline-dark btn-block dropdown-toggle mt-2" type="button" data-toggle="collapse" data-target="#tests" aria-expanded="false" aria-controls="collapseExample">--}}
{{--                                    Тесты--}}
{{--                                </button>--}}
{{--                                --}}{{--                                @php(dd(strpos(Route::currentRouteName(), 'tests')))--}}
{{--                                <div class="collapse {{--}}

{{--                    ((strpos(Route::currentRouteName(), 'tests') !== false) ||--}}
{{--                    (strpos(Route::currentRouteName(), 'exams') !== false)  ||--}}
{{--                    (strpos(Route::currentRouteName(), 'topics') !== false))--}}

{{--                    ? 'show' : '' }}" id="tests">--}}
{{--                                    <div class="card card-body">--}}
{{--                                        @if (Auth::user()->hasRoles(['admin', 'super_admin']))--}}
{{--                                            <a href="{{ route('topics.index') }}" class="nav-link">Темы</a>--}}
{{--                                        @endif--}}
{{--                                        <a class="nav-link" href="{{ route('tests.index') }}">Управление</a>--}}
{{--                                        <a class="nav-link" href="{{ route('exams.list') }}">Просмотр</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            @elseif (Auth::user()->hasRoles(['user', 'super_user']) )--}}
{{--                                <a class="btn btn-outline-dark btn-block" href="{{ route('exams.list') }}">Тесты</a>--}}
{{--                            @endif--}}

                        @endauth
                        @if (Auth::user()->hasRoles(['author', 'editor', 'super_admin', 'admin']) )
                            <button class="btn btn-outline-dark btn-block dropdown-toggle mt-2" type="button" data-toggle="collapse" data-target="#users" aria-expanded="false" aria-controls="collapseExample">
                                Пользователи и права
                            </button>
                            <div class="collapse {{ (strpos(Route::currentRouteName(), 'users') !== false || strpos(Route::currentRouteName(), 'roles') !== false )  ? 'show' : '' }}" id="users">
                                <div class="card card-body">
                                    <a href="{{ route('users.index') }}" class="btn btn-outline-dark btn-block">Пользователи</a>
                                    <a href="{{ route('adress.index') }}" class="btn btn-outline-dark btn-block">Адреса</a>
                                    @auth()
                                        @if (Auth::user()->hasRoles(['super_admin']))
                                            <a href="{{ route('roles.index') }}" class="btn btn-outline-dark btn-block">Роли</a>
                                        @endif
                                    @endauth
                                </div>
                            </div>
                        @endif
                        {{--                            @endif--}}
                        {{--                            {{ dd() }}--}}
                        {{--                            <a href="{{ route('pages.index') }}" class="btn btn-primary btn-block">Страницы</a>--}}

                        {{--                            <a href="{{ route('pages.create') }}" class="btn btn-success btn-block">Создать страницу</a>--}}
                        <hr>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-9">
                        @yield('content')
                    </div>

            </div>
        </div>

    </main>


    <footer class="footer">
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-4 pull-left1">
                    <ul class="nav nav-pills.nav">
                        @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_footer', 'on') as $page)
                            <li><a class="nav-link" href="/{{ $page->slug }}">{{ $page->title }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-sm-4 dropup">
                    <a class="dropdown-toggle d-none" data-toggle="dropdown" aria-expanded="false" href="#" style="text-decoration: none !important;">Язык<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/lang/ru">RU</a></li>
                        <li><a href="/lang/en">EN</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul class="social-network social-circle text-right ">
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>


            </div>
        </div>
    </footer>

</div>
@yield('footer')
@yield('script')
@include('layouts.scripts')
@yield('kladr')
</body>
</html>
