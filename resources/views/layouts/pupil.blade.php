<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <script src="//use.fontawesome.com/3f9de7ff60.js"></script>
    @include('layouts.bootstrap')
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
{{--    <script src="{{ asset('js/test_script.js') }}"></script>--}}
    @yield('header')
</head>
<body>
    @include('layouts.header')
    <main class="py-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    @yield('content')
                </div>
                <div class="col-xl-6">
                    @yield('sidebar')
                </div>
            </div>
        </div>
    </main>
    @include('layouts.scripts')
</body>
</html>
