<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="theme-color" content="#fff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
{{--    <link rel="icon" sizes="16x16" href="/design/img/favicons/favicon-16x16.png" type="image/png">--}}
{{--    <link rel="icon" sizes="32x32" href="/design/img/favicons/favicon-32x32.png" type="image/png">--}}
{{--    <link rel="apple-touch-icon-precomposed" href="/design/img/favicons/apple-touch-icon-precomposed.png">--}}
{{--    <link rel="apple-touch-icon" href="/design/img/favicons/apple-touch-icon.png">--}}
{{--    <link rel="apple-touch-icon" sizes="57x57" href="/design/img/favicons/apple-touch-icon-57x57.png">--}}
{{--    <link rel="apple-touch-icon" sizes="60x60" href="/design/img/favicons/apple-touch-icon-60x60.png">--}}
{{--    <link rel="apple-touch-icon" sizes="72x72" href="/design/img/favicons/apple-touch-icon-72x72.png">--}}
{{--    <link rel="apple-touch-icon" sizes="76x76" href="/design/img/favicons/apple-touch-icon-76x76.png">--}}
{{--    <link rel="apple-touch-icon" sizes="114x114" href="/design/img/favicons/apple-touch-icon-114x114.png">--}}
{{--    <link rel="apple-touch-icon" sizes="120x120" href="/design/img/favicons/apple-touch-icon-120x120.png">--}}
{{--    <link rel="apple-touch-icon" sizes="144x144" href="/design/img/favicons/apple-touch-icon-144x144.png">--}}
{{--    <link rel="apple-touch-icon" sizes="152x152" href="/design/img/favicons/apple-touch-icon-152x152.png">--}}
{{--    <link rel="apple-touch-icon" sizes="167x167" href="/design/img/favicons/apple-touch-icon-167x167.png">--}}
{{--    <link rel="apple-touch-icon" sizes="180x180" href="/design/img/favicons/apple-touch-icon-180x180.png">--}}
{{--    <link rel="apple-touch-icon" sizes="1024x1024" href="/design/img/favicons/apple-touch-icon-1024x1024.png">--}}
    <!-- connecting fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@700&display=swap" rel="stylesheet">
    <!-- connecting styles -->
    <link rel="stylesheet" href="/design/styles/main.min.css">
</head>

<body>
<!-- begin of page -->

<div class="admin-panel-wrapper">
    <aside class="admin-panel-aside">
        <div class="admin-panel-aside__logo">
            <a href="/">{{ config('app.name', 'Laravel') }}</a>
        </div>
        <div class="admin-panel-aside__new">
            <a href="#" class="btn btn--blue">
                <svg class="icon icon-plus">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-plus"></use>
                </svg>
                <span>Создать…</span>
            </a>
            <div class="admin-panel-aside__new-variants">
                <a href="{{ route('pages.create') }}">Страница</a>
                <a href="{{ route('news.create') }}">Новость</a>
                <a href="{{ route('poll.create') }}">Опрос</a>
{{--                <a href="#">Оповещение</a>--}}
            </div>
        </div>
        <nav class="admin-panel-aside__nav">
            <a href="{{ route('pages.index') }}" class="{{ (strpos(Route::currentRouteName(), 'pages') === false) ? '' : 'is-active' }}">
                <svg class="icon icon-news">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-news"></use>
                </svg>
                <span>Страницы</span>
            </a>
            <a href="{{ route('news.index') }}">
                <svg class="icon icon-news">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-news"></use>
                </svg>
                <span>Новости</span>
            </a>
            <a href="{{ route('poll.index') }}">
                <svg class="icon icon-quiz">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-quiz"></use>
                </svg>
                <span>Опросы</span>
            </a>

            <a href="{{ route('users.index') }}">
                <svg class="icon icon-quiz">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-quiz"></use>
                </svg>
                <span>Пользователи</span>
            </a>
{{--            <a href="{{ route('adress.index') }}">--}}
{{--                <svg class="icon icon-quiz">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-quiz"></use>--}}
{{--                </svg>--}}
{{--                <span>Адреса</span>--}}
{{--            </a>--}}
{{--            @auth()--}}
{{--                @if (Auth::user()->hasRoles(['super_admin']))--}}
{{--                    <a href="{{ route('roles.index') }}">--}}
{{--                        <svg class="icon icon-quiz">--}}
{{--                            <use xlink:href="/design/img/sprites/main-sprite.svg#icon-quiz"></use>--}}
{{--                        </svg>--}}
{{--                        <span>Роли</span>--}}
{{--                    </a>--}}
{{--                @endif--}}
{{--            @endauth--}}
{{--            <a href="#">--}}
{{--                <svg class="icon icon-ntf">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-ntf"></use>--}}
{{--                </svg>--}}
{{--                <span>Оповещения</span>--}}
{{--            </a>--}}
{{--            <a href="#">--}}
{{--                <svg class="icon icon-archive">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-archive"></use>--}}
{{--                </svg>--}}
{{--                <span>Архив</span>--}}
{{--            </a>--}}
        </nav>
        <div class="admin-panel-aside__profile">
            <div class="avatar admin-panel-aside__profile-avatar">
                <span>{{ Str::limit(Auth::user()->name, 1, '') }}</span>
            </div>
            <div class="admin-panel-aside__profile-info">
                <span>{{ Auth::user()->name }}</span>
                <span>{{ Auth::user()->role->name }}</span>
            </div>
        </div>
    </aside>

    @yield('content')

</div>

<!-- end of page -->

<!-- modals -->
<div class="modals-overlay">
    <div class="modal modal-reg">
        <div class="modal-title">
            <h3>Регистрация</h3>
        </div>
        <form class="modal-form">
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Имя</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Фамилия</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Электронная почта</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module input-module--hidden-symbols">
                <div class="input-module__inner">
                    <label for="in5">Пароль</label>
                    <div class="input-module__inner-area">
                        <input type="password" class="input">
                        <button>
                            <svg class="icon icon-eye">
                                <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                            </svg>
                        </button>
                    </div>
                    <span></span>
                </div>
            </div>
            <button type="submit" class="btn btn--blue">Зарегистрироваться</button>
        </form>
        <div class="modal-reg__login">
            <span>Уже зарегистрированы?</span>
            <button class="btn btn--gray call-login">Войти</button>
        </div>
        <div class="modal-reg__privacy">
            Регистрируясь, вы принимаете условия <a href="#">Политики конфиденциальности</a>
        </div>
        <button class="modal-close"></button>
    </div>
    <div class="modal modal-login">
        <div class="modal-title">
            <h3>Войти</h3>
        </div>
        <form class="modal-form">
            <div class="input-module">
                <div class="input-module__inner">
                    <label for="in5">Электронная почта</label>
                    <div class="input-module__inner-area">
                        <input type="text" class="input">
                    </div>
                    <span></span>
                </div>
            </div>
            <div class="input-module input-module--hidden-symbols">
                <div class="input-module__inner">
                    <label for="in5">Пароль</label>
                    <div class="input-module__inner-area">
                        <input type="password" class="input">
                        <button>
                            <svg class="icon icon-eye">
                                <use xlink:href="/design/img/sprites/main-sprite.svg#icon-eye"></use>
                            </svg>
                        </button>
                    </div>
                    <span></span>
                </div>
                <div class="input-module__manage">
                    <a href="#">Не помню пароль</a>
                </div>
            </div>
            <button type="submit" class="btn btn--blue">Войти</button>
        </form>
        <div class="modal-login__reg">
            <button class="btn btn--gray call-reg">Зарегистрироваться</button>
        </div>
        <div class="modal-login__socials">
            <span>Или войдите с помощью</span>
            <a href="#" class="modal-login__socials-link">
                <svg class="icon icon-social-ok">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-ok"></use>
                </svg>
            </a>
            <a href="#" class="modal-login__socials-link">
                <svg class="icon icon-social-vk">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-vk"></use>
                </svg>
            </a>
            <a href="#" class="modal-login__socials-link">
                <svg class="icon icon-social-fb">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-fb"></use>
                </svg>
            </a>
            <a href="#" class="modal-login__socials-link">
                <svg class="icon icon-social-twitter">
                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-social-twitter"></use>
                </svg>
            </a>
        </div>
        <button class="modal-close"></button>
    </div>
</div>

<!-- connecting scripts -->
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/design/js/main.min.js"></script>
<script src="/design/js/vendor.min.js"></script>
@yield('kladr')
@yield('script')
@include('layouts.scripts')
</body>

</html>
