@extends('layouts.design')
@section('menu_left')
    @include('layouts.menu_left')
@endsection

@section('sidebar_right')
    @include('layouts.menu_right')
@endsection

@section('title')
    {{ config('app.name') }}
@stop

@section('keywords')

@stop

@section('description')

@stop

@section('content')
    <div class="news" id="post-data">
        @include('news.data')
        <div class="ajax-load text-center" style="display:none">
            <p><img src="/design/img/loader.gif">Загрузка новостей</p>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var page = 1;
        var loaderFlag = true;

        $(window).scroll(function () {
            if (loaderFlag)
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 200) {
                    page++;
                    loadMoreData(page);
                }
        });

        function loadMoreData(page) {
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    beforeSend: function () {
                        $('.ajax-load').show();
                    }
                })
                .done(function (data) {
                    if (data.html == "" || data.html == " ") {
                        $('.ajax-load').html("Больше новостей нет.");
                        loaderFlag = false;
                        return;
                    }
                    $('.ajax-load').hide();
                    $("#post-data").append(data.html);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('сервер не отвечает...');
                });
        }
    </script>

    <style type="text/css">
        .ajax-load {
            background: #fff;
            padding: 10px 0px;
            width: 100%;
        }
    </style>
@endsection
