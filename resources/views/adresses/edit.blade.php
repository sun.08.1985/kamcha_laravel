@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('adress_edit', $adress) }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['users.update', $adress->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" value="{{ $adress->name }}">
                                @if ($errors->has('name'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" value="{{ $adress->email }}">
                                @if ($errors->has('email'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="phone">{{ __('Phone') }}</label>
                                <input type="phone" class="form-control" name="phone" value="{{ $adress->phone }}">
                                @if ($errors->has('phone'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <div class="pupil @if($adress->role_id != 1) d-none @endif">
                                <dd>
                                    <label for="school">Школа</label>
                                    <input @if($adress->role_id != 1) disabled="disabled" @endif type="text" class="form-control" name="school" value="{{ $adress->school }}">
                                    @if ($errors->has('school'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class">Класс</label>
                                    <input @if($adress->role_id != 1) disabled="disabled" @endif type="number" min="1" max="12" class="form-control" name="class" value="{{ $adress->class }}">
                                    @if ($errors->has('class'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class_litter">Литер</label>
                                    <input @if($adress->role_id != 1) disabled="disabled" @endif type="text" class="form-control" maxlength="1" name="class_litter" value="{{ $adress->class_litter }}">
                                    @if ($errors->has('class_litter'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                            </div>
                            <dd>
                                <label for="role_id">{{ __('Role') }}</label>
{{--                                {{ dd(\App\Role::all()->where('id', '<=', $adress->role_id)) }}--}}
                                <select class="form-control" name="role_id">
                                    @foreach(\App\Role::all()->where('id', '<=', $adress->role_id) as $role)
                                        <option {{ ($adress->role->id == $role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="password">{{ __('Password') }}</label>
                                <input type="password" class="form-control" name="password" value="">
                                @if ($errors->has('password'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="password_confirm">Подтверждение пароля</label>
                                <input type="password" class="form-control" name="password_confirmation" value="">
                                @if ($errors->has('password_confirmation'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <button class="btn btn-warning" type="submit">Изменить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
