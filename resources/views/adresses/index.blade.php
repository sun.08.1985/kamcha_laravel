@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('adresses') }}
                        @if (Auth::user()->hasRoles(['super_user', 'super_admin', 'admin']) )
                            <a class="position-absolute btn btn-success btn-sm btn-add" href="{{ route('adress.create') }}">Добавить</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    @if( Auth::user()->isAdmin() )
                                        <td>ID</td>
                                    @endif
                                    <td>{{ __('City') }}</td>
                                    <td>{{ __('Street') }}</td>
                                    <td>{{ __('Building') }}</td>
                                    <td>{{ __('Appartment') }}</td>

{{--                                    <td>{{ __('Action') }}</td>--}}
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($adresses as $adress)
                                <tr>
                                    @if (Auth::user()->isAdmin())
                                        <td>{{ $adress->id }}</td>
                                    @endif
                                    <td>{{ $adress->city }}</td>
                                    <td>{{ $adress->street }}</td>
                                    <td>{{ $adress->building }}</td>
                                    <td>{{ $adress->appartment }}</td>

{{--                                    <td>--}}
{{--                                        <a class="btn btn-sm btn-success" href="{{ route('adresses.edit', $adress->id) }}"><i class="fa fa-edit"></i></a>--}}
{{--                                        @if (Auth::user()->hasRoles(['super_admin', 'admin']) )--}}
{{--                                            {!! Form::open(['route' => ['adresses.destroy', $adress->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}--}}
{{--                                            <button class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-trash"></i></button>--}}
{{--                                            {!! Form::close() !!}--}}
{{--                                        @endif--}}
{{--                                        <a href="{{ route('adresses.destroy', $adress->id) }}"></a>--}}
{{--                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
