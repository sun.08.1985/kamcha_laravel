@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Создание адреса</h1>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'adresss.store', 'method' => 'post']) !!}

                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="phone">{{ __('Phone') }}</label>
                                <input type="tel" class="form-control" name="phone" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <div class="pupil">
                                <dd>
                                    <label for="school">Школа</label>
                                    <input type="text" class="form-control" name="school" value="{{ old('school') }}">
                                    @if ($errors->has('school'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class">Класс</label>
                                    <input type="number" min="1" max="12" class="form-control" name="class" value="{{ old('class') }}">
                                    @if ($errors->has('class'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class_litter">Литер</label>
                                    <input type="text" class="form-control" maxlength="1" name="class_litter" value="{{ old('class_litter') }}">
                                    @if ($errors->has('class_litter'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                            </div>
                            <dd>
                                <label for="role_id">{{ __('Role') }}</label>
                                <select class="form-control" name="role_id" value="{{ old('role_id') }}">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="password">{{ __('Password') }}</label>
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                                @if ($errors->has('password'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>
                            <dd>
                                <label for="password_confirm">Подтверждение пароля</label>
                                <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                @if ($errors->has('password_confirmation'))
                                    <b class="alert alert-danger">{{ $errors->first() }}</b>
                                @endif
                            </dd>

                            <button class="btn btn-success" type="submit">Сохранить</button>

                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
