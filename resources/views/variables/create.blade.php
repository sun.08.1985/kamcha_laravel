@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <h3>Создание ответа на вопрос - {{ $test->title }}</h3>
        <h6>{{ $test->description }}</h6>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">


                    {!! Form::open(['route' => 'variables.store', 'method' => 'post']) !!}

                        <dd>
                            <label for="title">Ответ</label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                            @endif
                        </dd>
                        <input type="hidden" class="form-control" name="test_id" value="{{ $test->id }}">
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <dd>
                            <label for="description">Пояснение</label>
                            <textarea class="form-control" name="description" rows="10">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <b class="alert alert-danger">{{ $errors->first() }}</b>
                            @endif
                        </dd>
                        <dd>
                            <label for="correct">Верный ответ</label>
                            <input name="correct" type="checkbox" {{ old('correct') }}>
                        </dd>
                        <button class="btn btn-success" type="submit">Сохранить</button>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
@stop
