@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <h3>Ответы</h3>
        <a href="{{ route('variables.create') }}" class="btn btn-success">Добавить</a>
        <div class="row">

            <div class="col-md-12">
                <table class="table">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Название</td>
                            <td>Количество ответов</td>
                            <td>{{ __('Action') }}</td>
                        </tr>
                    </thread>
                    <tbody>

                    @foreach($variables as $variable)
                        <tr>
                            <td>{{ $variable->id }}</td>
                            <td>{{ $variable->title }}</td>
                            <td>
{{--                                <a class="btn btn-primary" href="{{ route('variables.show', $variable->id) }}"><i class="fa fa-eye"></i></a>--}}
                                <a class="btn btn-success" href="{{ route('variables.edit', $variable->id) }}"><i class="fa fa-edit"></i></a>
                                {!! Form::open(['route' => ['variables.destroy', $variable->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                    <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-remove"></i></button>
                                {!! Form::close() !!}

{{--                                <a href="{{ route('variables.destroy', $variable->id) }}"></a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
