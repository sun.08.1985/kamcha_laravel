@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <h3>Ответ - {{ $variable->title }}</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">


                    <input type="text" class="form-control" name="title" value="{{ $variable->title }}">
                    <input type="hidden" class="form-control" name="variable_id" value="1">
                    <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                    <textarea class="form-control" name="description">{{ $variable->description }}</textarea>


                </div>
            </div>
        </div>
    </div>
@stop
