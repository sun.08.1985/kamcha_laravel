@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <h3>Опросы</h3>
        <div class="row">

            <div class="col-md-12">
                <table class="table">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Название</td>
                            <td>Количество</td>
                            <td>{{ __('Action') }}</td>
                        </tr>
                    </thread>
                    <tbody>

                    @foreach($pages as $page)
                        <tr>
                            <td>{{ $page->id }}</td>
                            <td>{{ $page->title }}</td>
                            <td>
                                @isset($page->questions)
                                    {{ $page->questions->count() }}
                                @endisset
                            </td>
                            <td>
                                <a class="btn btn-info" href="{{ route('page.start', $page->id) }}"><i class="fa fa-play"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
