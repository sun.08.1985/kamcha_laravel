@extends('layouts.design')

@section('menu_left')
    @include('layouts.menu_left')
@endsection

@section('h1')
    {{ $page->title }}
@stop

@section('title')
    {{ $page->title }} - {{ config('app.name') }}
@stop

@section('keywords')
    {{ $page->keywords }}
@stop

@section('description')
    {{ $page->description }}
@stop

@section('header')
    {{ $page->header }}
@stop

@section('content')
    <h1>{{ $page->title }}</h1>
    @if(Session::has('error'))
        <span style="color: red; font-weight: bolder">{{ Session::get('error') }}</span>
        {{ Session::forget('error') }}
    @endif
    @if(Session::has('complited'))
        <span style="color: green; font-weight: bolder" id="text_complited">{{ Session::get('complited') }}</span>
        {{ Session::forget('complited') }}
    @endif
    @auth()
        @if(Auth::user()->hasRoles(['admin', 'super_admin']))
            <small>
            <a href="{{ route('pages.edit', $page->id) }}" class="btn btn--green">
{{--                <svg class="icon icon-social-twitter">--}}
{{--                    <use xlink:href="/design/img/sprites/main-sprite.svg#icon-pen"></use>--}}
{{--                </svg>--}}
                Редактировать
            </a>
            </small>
        @endif
    @endauth
    {!! $page->content !!}

@stop

@section('footer')
    {{ $page->footer }}
    <script>
        $(document).ready(function () {
            setTimeout(function () {
                $('#text_complited').fadeOut(3000);
            }, 1000);
        });

    </script>
@stop
