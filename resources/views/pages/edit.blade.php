@extends('layouts.admin')

@section('sidebar')
    @parent

@stop

@section('content')
    <section class="admin-panel-content">
        {{--        <form class="admin-panel-content__search">--}}
        {{--            <input type="text" placeholder="Поиск...">--}}
        {{--        </form>--}}
        <div class="admin-panel-section admin-panel-article-edit">
            {!! Form::open(['route' => ['pages.update', $page->id], 'method' => 'PUT']) !!}
            <div class="admin-panel-section__heading">
                <div class="select admin-panel-article-edit__select">
{{--                    <select name="category_id" value="{{$page->category}}">--}}
{{--                        @foreach($categories as $category)--}}
{{--                            <option value="{{ $category->id }}">{{ $category->name }}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
                </div>
                <a href="#" class="admin-panel__prorogue"> </a>
                <button type="submit" class="admin-panel__publish btn btn--blue">Опубликовать</button>
            </div>
            <div class="admin-panel-section__content admin-panel-new-ntf__area">
                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="in1">Заголовок</label>
                        <div class="input-module__inner-area"><input name="title" value="{{$page->title}}" type="text" class="input"></div>
                        <span></span>
                    </div>
                </div>
                <div class="input-module input-module--textarea admin-panel-new-ntf__area-mess">
                    <div class="input-module__inner">
                        <label for="in3">Текст оповещения</label>
                        <div class="input-module__inner-area">
                            <textarea name="description" value="{{$page->description}}" class="autoheight__inner" maxlength="200" style="height: 36px;"></textarea>
                            <div class="input-module__inner-area-counter">
                                <div>19</div>/200
                            </div>
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="keywords">Ключевые слова</label>
                        <div class="input-module__inner-area">
                            <input type="text" class="form-control" name="keywords" value="{{$page->keywords}}">
                        </div>
                        <span></span>
                    </div>
                </div>

                <div class="input-module admin-panel-new-ntf__area-title">
                    <div class="input-module__inner">
                        <label for="order">Позиция</label>
                        <div class="input-module__inner-area">
                            <input type="number" class="form-control" name="order" value="{{$page->order}}">
                            <label for="menu_header">В меню шапки <input class="form-control" type="checkbox" name="menu_header" {{$page->menu_header? 'checked':'' }}></label>
                            <label for="menu_footer">В меню подвала <input class="form-control" type="checkbox" name="menu_footer" {{$page->menu_footer? 'checked':'' }}></label>
                        </div>
                    </div>
                </div>

                <div class="admin-panel-section__content admin-panel-article-edit__area">
                    <textarea id="mce_0" aria-hidden="true" name="content">{{ $page->content }}</textarea>
                </div>
                {!! Form::close() !!}
            </div>
    </section>
@stop
