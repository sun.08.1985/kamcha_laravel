@extends('layouts.design')

@section('menu_left')
    @foreach(Page::orderBy('order', 'ASC')->get()->where('menu_header', 'on') as $page)
        <a class="nav__link" href="/{{ $page->slug }}">{{ $page->title }}</a>
    @endforeach
@endsection

@section('content')
    <section class="profile tabs">
        <div class="profile-triggers">
            <button class="tabs__trigger profile-triggers__item">Профиль</button>
            <button class="tabs__trigger profile-triggers__item">Уведомления</button>
        </div>
        <div class="profile-tabs tabs-list">
            {!! Form::open(['route' => ['profile.update'], 'method' => 'PUT', 'id' => 'formPublish', 'enctype' => 'multipart/form-data']) !!}
            <input type="hidden" name="role_id" value="{{ Auth::user()->role_id }}">
            <div class="profile-main">
                <div class="profile-main-avatar">
                    <div class="avatar profile-main-avatar__container" style="background-size: cover; background-image: url(/uploads/avatars/{{ Auth::user()->avatar }})">
{{--                        <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">--}}
                        <span>{{ (Auth::user()->avatar == 'default.jpg')? Str::limit(Auth::user()->name, 1, ''): '' }}</span>
                    </div>
                    <input type="file" id="downloadAvatar" name="avatar" accept="image/x-png">
                    <label for="downloadAvatar" class="btn btn--gray">Изменить</label>
                </div>
                <div class="profile-main-block">
                    <div class="profile-main-block__title">
                        <h3>Основная информация</h3>
                    </div>
                    <div class="profile-main-block-container">
                        <div class="input-module">
                            <div class="input-module__inner is-wrong-">
                                <label for="in2">Фамилия</label>
                                <div class="input-module__inner-area"><input type="text" name="firstname" class="input" value="{{ Auth::user()->firstname }}"></div>
                                {{--                                <span>Текст ошибки</span>--}}
                            </div>
                        </div>
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in1">Имя</label>
                                <div class="input-module__inner-area"><input type="text" name="name" class="input" value="{{ Auth::user()->name }}"></div>
                                <span></span>
                            </div>
                        </div>

                        <div class="input-module">
                            <div class="input-module__inner is-wrong-">
                                <label for="in2">Отчество</label>
                                <div class="input-module__inner-area"><input type="text" name="lastname" class="input" value="{{ Auth::user()->lastname }}"></div>
                                {{--                                <span>Текст ошибки</span>--}}
                            </div>
                        </div>
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in3">Электронный адрес</label>
                                <div class="input-module__inner-area"><input type="text" name="email" class="input" value="{{ Auth::user()->email }}"></div>
                                <span></span>
                            </div>
                            <div class="input-module">
                                <label for="in3">Пароль</label>
                                <div class="input-module__inner-area"><input type="password" name="password" class="input" value="" autocomplete=""></div>
                                <span></span>
{{--                                <a href="#" class="btn btn--gray">Изменить пароль</a>--}}
                            </div>
                        </div>
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in4">Телефон</label>
                                <div class="input-module__inner-area"><input type="text" name="phone" class="input" value="{{ Auth::user()->phone }}"></div>
                                <span></span>
                            </div>
                        </div>
                        <div class="input-module input-module--calendar">
                            <div class="input-module__inner">
                                <label for="in5">Дата рождения</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="birthday" class="input" value="{{ Carbon\Carbon::parse(Auth::user()->birthday)->format('d.m.Y') }}">
                                    <svg class="icon icon-calendar">
                                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-calendar"></use>
                                    </svg>
                                </div>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-main-block profile-main-block--2">
                    <div class="profile-main-block__title">
                        <h3>Паспортные данные</h3>
                    </div>
                    <div class="profile-main-block__subtitle">
                        <p>Здесь будет текст, зачем нужны паспортные данные. И о безопасности сохранении данных.</p>
                    </div>

                    <div class="profile-main-block-container">
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in5">Серия</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="serial_passport" class="input" placeholder="0000" value="{{ Auth::user()->serial_passport }}">
                                </div>
                                <span></span>
                            </div>
                        </div>
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in5">Номер</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="number_passport" class="input" placeholder="000000" value="{{ Auth::user()->number_passport }}">
                                </div>
                                <span></span>
                            </div>
                        </div>
                        <div class="input-module input-module--calendar">
                            <div class="input-module__inner">
                                <label for="in5">Дата выдачи</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="when_passport" class="input" value="{{ Carbon\Carbon::parse(Auth::user()->when_passport)->format('d.m.Y') }}">
                                    <svg class="icon icon-calendar">
                                        <use xlink:href="/design/img/sprites/main-sprite.svg#icon-calendar"></use>
                                    </svg>
                                </div>
                                <span></span>
                            </div>
                        </div>
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in5">Кем выдан</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="where_passport" class="input" value="{{ Auth::user()->where_passport }}">
                                </div>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile-main-block">
                    <div class="profile-main-block__title">
                        <h3>Дополнительная информация</h3>
                    </div>
                    <div class="profile-main-block-container">
                        <div class="input-module">
                            <div class="input-module__inner">
                                <label for="in5">Место проживания</label>
                                <div class="input-module__inner-area">
                                    <input type="text" name="city" class="input" value="{{ Auth::user()->adress->city }}">
                                </div>
                                <div class="input-module__inner-area">
                                    <input type="text" name="street" class="input" value="{{ Auth::user()->adress->street }}">
                                </div>
                                <div class="input-module__inner-area">
                                    <input type="text" name="building" class="input" value="{{ Auth::user()->adress->building }}">
                                </div>
                                <div class="input-module__inner-area">
                                    <input type="text" name="appartment" class="input" value="{{ Auth::user()->adress->appartment }}">
                                </div>

                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="profile-main-block">--}}
{{--                    <div class="profile-main-block__title">--}}
{{--                        <h3>Удалить аккаунт</h3>--}}
{{--                    </div>--}}
{{--                    <div class="profile-main-block-container">--}}
{{--                        <div class="profile-main-delete">--}}
{{--                            <p>Все ваши данные будут удалены. Вы сможете восстановить аккаунт в течении 7 дней.</p>--}}
{{--                            <a href="#" class="btn btn--red">Удалить</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="profile-main__save">
                    <input type="submit" class="btn btn--blue" value="Сохранить">
                </div>
            </div>
            {!! Form::close() !!}
            <div class="profile-ntf">
                {!! Form::open(['route' => ['profile.update'], 'method' => 'PUT', 'id' => 'formNotification']) !!}

                <div class="profile-ntf__row">
                    <span>Уведомления на сайте</span> <div class="switcher">
                        <input type="checkbox" name="sms_notification" {{ (Auth::user()->sms_notification)? 'checked': '' }}>
                    </div>
                </div>
                <div class="profile-ntf__row">
                    <span>Уведомления в браузере Chrome</span> <div class="switcher">
                        <input type="checkbox" name="push_notification" {{ (Auth::user()->push_notification)? 'checked': '' }}>
                    </div>
                </div>
{{--                <div class="profile-ntf__row">--}}
{{--                    <span>Уведомления в браузере Chrome</span> <div class="switcher">--}}
{{--                        <input type="checkbox">--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="profile-ntf__row">
                    <span>Уведомления на почту</span> <div class="switcher">
                        <input type="checkbox" name="email_notification" {{ (Auth::user()->email_notification)? 'checked': '' }}>
                    </div>
                </div>
                <div class="profile-main__save">
                    <input type="submit" class="btn btn--blue" value="Сохранить">
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

