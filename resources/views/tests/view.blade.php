@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('test_view', $test) }}
                        @if( Auth::user()->hasRoles(['author', 'editor', 'admin', 'super_admin']) )
                            <a class="btn btn-success btn-sm edit-btn" href="{{ route('tests.edit', $test) }}">Редактировать</a>
                        @endif
                    </div>
                    <div class="card-body">
                        {{ $test->description }}
                    </div>
                    <div class="card-footer">
                        <h3>Вопросы</h3>
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Вопрос</td>
                                    <td>Количество ответов</td>
                                </tr>
                            </thread>
                            <tbody>

                            @forelse($test->questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->title }}</td>
                                    <td>
                                        @isset($question->answers)
                                            {{ $question->answers->count() }}
                                        @endisset
                                    </td>
                                </tr>
                            @empty
                                <h3 class="text-center">Нет вопросов</h3>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
