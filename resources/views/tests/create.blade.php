@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('test_add') }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => 'tests.store', 'method' => 'post']) !!}

                                <dd>
                                    <label for="time_estimation">Время прохождения (мин)</label>
                                    <input type="number" class="form-control" name="time_estimation" value="{{ old('time_estimation') ? old('time_estimation') : 45 }}">
                                    @if ($errors->has('time_estimation'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class">Класс</label>
                                    <input type="number" class="form-control" name="class" min="1" max="12" value="{{ old('class') ? old('class') : 1 }}">
                                    @if ($errors->has('class'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="topic_id">Предмет</label>
                                    <select class="form-control" name="topic_id" value="{{ old('topic_id') }}">
                                        <option {{ old('topic_id') ? '': 'selected' }} value="">Не выбран</option>
                                        @foreach($topics as $topic)
                                            <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('topic_id'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="title">Название</label>
                                    <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                    @if ($errors->has('title'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                                <dd>
                                    <label for="description">Описание</label>
                                    <textarea class="form-control" name="description" rows="10">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <button class="btn btn-success" type="submit">Сохранить</button>

                            {!! Form::close() !!}


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
