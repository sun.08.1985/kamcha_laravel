@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('tests') }}
                        @if( Auth::user()->hasRoles(['author', 'editor', 'admin', 'super_admin']) )
                            <a href="{{ route('tests.create') }}" class="btn btn-success btn-sm position-absolute btn-add">Добавить</a>
                        @endif
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Название</td>
                                    <td>Количество</td>
                                    <td>Время, мин</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($tests as $test)
                                <tr>
                                    <td>{{ $test->id }}</td>
                                    <td>{{ $test->title }}</td>
                                    <td>
                                        @isset($test->questions)
                                            {{ $test->questions->count() }}
                                        @endisset
                                    </td>
                                    <td>{{ $test->time_estimation }}</td>
                                    <td>
                                        <a class="btn btn-info" href="{{ route('exams.start', $test->id) }}"><i class="fa fa-play"></i></a>
{{--                                        <a class="btn btn-primary" href="{{ route('tests.show', $test->id) }}"><i class="fa fa-eye"></i></a>--}}
                                        @if( Auth::user()->hasRoles(['author', 'editor', 'admin', 'super_admin']) )
                                            <a class="btn btn-success" href="{{ route('tests.edit', $test->id) }}"><i class="fa fa-edit"></i></a>
                                            {!! Form::open(['route' => ['tests.destroy', $test->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                            <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-remove"></i></button>
                                            {!! Form::close() !!}
                                        @endif
{{--                                        <a href="{{ route('tests.destroy', $test->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
