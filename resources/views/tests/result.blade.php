@extends('layouts.tests')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Опрос - {{ $test->test->title }}</h1>
                    </div>
                    <div class="card-body">
{{--                        @if( Auth::user()->hasRoles(['admin', 'super_admin']) )--}}
{{--                            <div class="alert alert-warning">--}}
{{--                                <h3>Видно только админам</h3>--}}
                                Верно: {{ $test->correct_answer }}<br>
                                Ошибок: {{ $test->incorrect_answer }}<br>
{{--                                К зачислению {{ $count_true - $count_false }}--}}
{{--                            </div>--}}
{{--                        @endif--}}
                        <h2>Спасибо за прохождение данного теста!</h2>
                        <p class="lead">
{{--                            Оповещение родителей будет произведено путем смс-информирования.--}}
{{--                            <br>--}}
                            <a class="btn btn-primary" href="{{ route('admin') }}">Вернуться в личный кабинет</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('script')

@stop
