@extends('layouts.tests')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Опрос - {{ $test->title }}</h1>
                        <p id = "time"></p>
                        <p id = "rezult">
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="accordion" id="accordionTest">

                            {!! Form::open(['route' => ['exams.control', $test->id], 'method' => 'post', 'id' => 'form_control']) !!}
{{--                                <input name="_check" type="hidden" value="{{ time() }}">--}}
                                <input type="hidden" name="test_id" value="{{ $test->id }}">
                                <input type="hidden" name="travel_time" id="travel_time" value="0">
                                @php $id = 0 @endphp

                                @foreach($test->questions_random_limit as $question)

                                    @if($question->answers->count() > 0)

                                        @php $id++ @endphp

                                        <div class="card @if ($id != 1) d-none @endif">
                                            <div class="card-header" id="testHeader{{ $id }}">
                                                <h5 class="mb-0">
                                                    <button class="btn " type="button" data-toggle="collapse" data-target="#test{{ $id }}" aria-expanded="true" aria-controls="test{{ $id }}">
                                                        <h4>Вопрос {{ $id }}.</h4>
                                                        <p>{{ $question->title }}</p>
                                                    </button>
                                                </h5>
                                            </div>

                                            <div id="test{{ $id }}" class="collapse @if ($id == 1) show @endif" aria-labelledby="test{{ $id }}" data-parent="#accordionTest" data-total-answer="{{ $test->questions->count() }}">
                                                <div class="card-body">
                                                    {!! $question->description !!}
                                                    <hr>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h6>Варианты ответов</h6>
                                                                <ul>
                                                                    @forelse($question->answersRandom as $answer)
                                                                        <li class="">
                                                                            <label>
                                                                                <input type="radio" name="q{{ $question->id }}a{{ $answer->id }}" data-answer-id="{{ $answer->id }}" data-question-id="{{ $question->id }}"> {{ $answer->title }}
                                                                                <small>{!! $answer->description !!}</small>
                                                                            </label>
                                                                        </li>
                                                                    @empty
                                                                        Нет вариантов
                                                                    @endforelse
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if ($id == 1)
                                                    <div class="card-footer">
                                                        <a class="btn btn-success btn-sm" id="next_question" data-question-id = "{{ $id+1 }}">Следующий</a>
                                                    </div>
                                                @endif

                                            </div>
                                        </div>
                                    @endif

                                @endforeach

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('script')
    <script>
                $(document).on('keydown', function (e) {
            e.preventDefault();
            if (e.keyCode == 13) $('#next_question').click();

        });
        var TimeControl = {{ $test->time_estimation }} * 60;

        var stopTimer;
        function testTimer(startTime)  {
            var totalMin = parseInt(TimeControl / 60);
            document.getElementById("rezult").innerHTML = '';

            var time = startTime;
            var min = parseInt(time / 60);
            if ( min < 1 ) min = 0;
            time = parseInt(time - min * 60);

            if ( min < 10 ) min = '0'+min;
            var seconds = time;
            if ( seconds < 10 ) seconds = '0'+seconds;
            document.getElementById("time").innerHTML='<span>Осталось времени- '+min+' мин '+seconds+' секунд</span>';
            document.getElementById("travel_time").value = totalMin - min;
            startTime--;
            if ( startTime  >= 0 ) {
                stopTimer  =  setTimeout(function(){testTimer(startTime); }, 1000);
            } else {
                document.getElementById("time").innerHTML='<span>Осталось времени- 00 мин 00 секунд</span>';
                var rezult = document.getElementById("rezult");
                rezult.innerHTML ="Время вышло";
                alert('ТЕСТ ЗАВЕРШЕН');
                $('#form_control').submit();
                clearTimeout(stopTimer);
            }
        }

        $(document).ready(function () {

            const totalAnswer = $('[data-total-answer]').data('total-answer');
            var nextQuestion;
            $('body').on('click', '#next_question', function () {

                nextQuestion = parseInt( $(this).attr('data-question-id') );
                if (nextQuestion <= totalAnswer) {
                    $('[data-target="#test' + parseInt(nextQuestion-1) + '"]').attr('data-target', '');
                    $('[data-target="#test' + nextQuestion + '"]').click();
                    $(this).parent().appendTo( $('#test' + nextQuestion) );
                    $('#test' + nextQuestion).parent().removeClass('d-none');
                    $('#test' + parseInt(nextQuestion-1)).parent().addClass('d-none');

                    nextQuestion++;
                    $(this).attr('data-question-id', nextQuestion);
                } else {
                    // alert('ТЕСТ ЗАВЕРШЕН');
                    $('#form_control').submit();
                }
            });

        });


        $(document).ready(function () {
            testTimer (TimeControl);
        });

    </script>
@stop
