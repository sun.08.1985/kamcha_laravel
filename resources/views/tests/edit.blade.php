@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('test_edit', $test) }}
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            {!! Form::open(['route' => ['tests.update', $test->id], 'method' => 'PUT']) !!}

                                <dd>
                                    <label for="time_estimation">Время прохождения (мин)</label>
                                    <input type="number" class="form-control" name="time_estimation" value="{{ $test->time_estimation ?? 45 }}">
                                    @if ($errors->has('time_estimation'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="class">Класс</label>
                                    <input type="number" class="form-control" name="class" min="1" max="12" value="{{ $test->class ?? 1 }}">
                                    @if ($errors->has('class'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="topic_id">Предмет</label>
                                    <select class="form-control" name="topic_id" value="{{ old('topic_id') }}">
                                        <option value="">Не выбран</option>
                                        @foreach($topics as $topic)
                                            <option {{ ($test->topic_id == $topic->id) ? 'selected' : '' }} value="{{ $topic->id }}">{{ $topic->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('title'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <dd>
                                    <label for="title">Название</label>
                                    <input type="text" class="form-control" name="title" value="{{ $test->title }}">
                                    @if ($errors->has('title'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                                <dd>
                                    <label for="description">Описание</label>
                                    <textarea class="form-control" name="description" rows="10">{{ $test->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <b class="alert alert-danger">{{ $errors->first() }}</b>
                                    @endif
                                </dd>
                                <button class="btn btn-warning" type="submit">Изменить</button>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    <div class="card-footer">
                        <h3>Вопросы</h3>
                        <a href="{{ route('questions.create', $test->id) }}" class="btn btn-success">Добавить</a>
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>Вопрос</td>
                                    <td>Количество ответов</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @forelse($test->questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->title }}</td>
                                    <td>
                                        @isset($question->answers)
                                            {{ $question->answers->count() }}
                                        @endisset
                                    </td>
                                    <td>
{{--                                        <a class="btn btn-primary" href="{{ route('questions.show', $question->id) }}"><i--}}
{{--                                                class="fa fa-eye"></i></a>--}}
                                        <a class="btn btn-success" href="{{ route('questions.edit', $question->id) }}"><i
                                                class="fa fa-edit"></i></a>
                                        {!! Form::open(['route' => ['questions.destroy', $question->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}
                                        <button class="btn btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i
                                                class="fa fa-remove"></i></button>
                                        {!! Form::close() !!}

{{--                                        <a href="{{ route('questions.destroy', $question->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @empty
                                <h3 class="text-center">Нет вопросов</h3>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
@stop
