@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('tests') }}
                    </div>
                    <div class="card-body">

                        @foreach($topics as $topic)
{{--                            @if ($topic->tests->count() > 0)--}}
                                <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link btn-block text-left" data-toggle="collapse" data-target="#collapse{{ $topic->id }}" aria-expanded="true" aria-controls="collapseOne">
                                                {{ $topic->name }} <span class="pull-right">Доступно: {{ $topic->tests->count() }}</span>
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapse{{ $topic->id }}" class="collapse" aria-labelledby="heading{{ $topic->id }}" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table">
                                                <thread>
                                                    <tr>
                                                        <td>ID</td>
                                                        <td>Название</td>
                                                        <td>Количество вопросов</td>
                                                        <td>Время, мин</td>
                                                        <td>{{ __('Class') }}</td>
                                                        <td>{{ __('Action') }}</td>
                                                    </tr>
                                                </thread>
                                                <tbody>

                                                @foreach($topic->tests as $test)

                                                    <tr>
                                                        <td>{{ $test->id }}</td>
                                                        <td>{{ $test->title }}</td>
                                                        <td>
                                                            @isset($test->questions)
                                                                {{ $test->questions->count() }}
                                                            @endisset
                                                        </td>
                                                        <td>{{ $test->time_estimation }}</td>
                                                        <td>{{ $test->class }}</td>
                                                        <td>
{{--                                                            <a id="test{{ $test->id }}" data-url="{{ route('exams.start', $test->id) }}" class="btn btn-success copy">Отправить ссылку</a>--}}
{{--                                                            <a class="btn btn-success" href="{{ route('users.assign.pupil', $test->id) }}">Рекомендовать</a>--}}
                                                            @if(Auth::user()->hasRoles(['super_user', 'admin', 'super_admin']))
                                                                <a class="btn btn-info" href="{{ route('exams.start', $test->id) }}"><i class="fa fa-play"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>

                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
{{--                            @endif--}}
                        @endforeach
{{--                        @include('pagination.default', ['paginator' => $topics])--}}
                    </div>
                </div>
            </div>
        </div>

    </div>
    <textarea id="helper"></textarea>
    <script>
        function copyAttr(selector, attribute) {

            if (!document.queryCommandSupported('copy'))
                return alert('Copy is not supported!');

            var target = document.querySelector(selector);

            var helper = document.querySelector('#helper');
            helper.value = target.getAttribute(attribute);
            helper.select();

            document.execCommand('copy');

            helper.value = '';
        }
        $(document).on('click',".copy",(e)=>{
            copyAttr('#'+e.target.id, 'data-url');
            // alert('#'+e.target.id);
        })
    </script>
    <style>
        #helper {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }
    </style>

@stop
