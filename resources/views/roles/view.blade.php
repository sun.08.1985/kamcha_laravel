@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>Пользователь - {{ $role->name }}</h1>
                    </div>
                    <div class="card-body">
                        <div class="form-group">

                            {!! Form::open(['route' => ['roles.update', $role->id], 'method' => 'PUT']) !!}
                            <dd>
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control" name="name" disabled value="{{ $role->name }}">
                            </dd>
                            <dd>
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" name="email" disabled value="{{ $role->email }}">
                            </dd>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
