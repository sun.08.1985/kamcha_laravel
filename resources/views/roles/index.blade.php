@extends('layouts.app')

@section('sidebar')
    @parent

@stop

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ Breadcrumbs::render('roles') }}
{{--                        <a class="position-absolute btn btn-success btn-sm btn-add" href="{{ route('roles.create') }}">Добавить</a>--}}
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thread>
                                <tr>
                                    <td>ID</td>
                                    <td>{{ __('Name') }}</td>
                                    <td>{{ __('Action') }}</td>
                                </tr>
                            </thread>
                            <tbody>

                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>
{{--                                        <a class="btn btn-sm btn-primary" href="{{ route('roles.show', $role->id) }}"><i class="fa fa-eye"></i></a>--}}
                                        <a class="btn btn-sm btn-success" href="{{ route('roles.edit', $role->id) }}"><i class="fa fa-edit"></i></a>
{{--                                        {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'DELETE', 'class' => 'd-inline']) !!}--}}
{{--                                        <button class="btn btn-sm btn-danger" onclick="return confirm('Вы уверены что хотите удалить?')"><i class="fa fa-trash"></i></button>--}}
{{--                                        {!! Form::close() !!}--}}

{{--                                        <a href="{{ route('roles.destroy', $role->id) }}"></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
