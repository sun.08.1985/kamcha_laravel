@extends('layouts.design')

@section('menu_left')
    @include('layouts.menu_left')
@endsection

@section('content')
    <script src="/js/jquery-3.4.1.min.js"></script>
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <div class="container-fluid" style="padding-bottom: 70px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body form-group">
                        <form method="POST" action="{{ route('register') }}" id="formPublish">
                            @csrf
                            <input type="hidden" name="district" data-kladr-id="4100900000000">
                            <input type="hidden" name="zip">
                            <input type="hidden" name="region" data-kladr-id="4100000000000">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" maxldength="20">

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    {{--            ADRESS--}}
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="city" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required>
    {{--                                <select id="city" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required>--}}
    {{--                                    <option>Усть-Камчатск</option>--}}
    {{--                                    <option>Ключи</option>--}}
    {{--                                    <option>Козыревск</option>--}}
    {{--                                </select>--}}

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>

                                <div class="col-md-6">
                                    <input id="street" type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" autocomplete="street">

                                    @error('street')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="building" class="col-md-4 col-form-label text-md-right">{{ __('Building') }}</label>

                                <div class="col-md-6">
                                    <input id="building" type="text" class="form-control @error('building') is-invalid @enderror" name="building" value="{{ old('building') }}" autocomplete="building">

                                    @error('building')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="appartment" class="col-md-4 col-form-label text-md-right">{{ __('Appartment') }}</label>

                                <div class="col-md-6">
                                    <input id="appartment" type="text" class="form-control @error('appartment') is-invalid @enderror" name="appartment" value="{{ old('appartment') }}" autocomplete="appartment">

                                    @error('appartment')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    {{--          ENDADRESS              --}}
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" minlength="6">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }} <strong>*</strong></label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" minlength="6">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--</div>--}}
@endsection

@section('kladr')
    <link href="/css/jquery.fias.min.css" rel="stylesheet">
    <script src="/js/jquery.fias.min.js" type="text/javascript"></script>
    {{--    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
    <script>
        // Form example
        (function () {
            var $container = $('.form-group');//document.getElementById('formPublish'));

            var $tooltip = $('#tooltip');

            var $zip = $container.find('[name="zip"]'),
                $region = $container.find('[name="region"]'),
                $district = $container.find('[name="district"]'),
                $city = $container.find('[name="city"]'),
                $street = $container.find('[name="street"]'),
                $building = $container.find('[name="building"]');

            $()
                .add($region)
                .add($district)
                .add($city)
                .add($street)
                .add($building)
                .fias({
                    parentInput: $container.find('#formPublish'),
                    verify: true,
                    select: function (obj) {
                        if (obj.zip) $zip.val(obj.zip);//Обновляем поле zip
                        setLabel($(this), obj.type);
                        $tooltip.hide();
                    },
                    check: function (obj) {
                        var $input = $(this);

                        if (obj) {
                            setLabel($input, obj.type);
                            $tooltip.hide();
                        }
                        else {
                            showError($input, 'Ошибка');
                        }
                    },
                    checkBefore: function () {
                        var $input = $(this);

                        if (!$.trim($input.val())) {
                            $tooltip.hide();
                            return false;
                        }
                    }
                });

            $region.fias('type', $.fias.type.region);
            $district.fias('type', $.fias.type.district);
            $city.fias('type', $.fias.type.city);
            $street.fias('type', $.fias.type.street);
            $building.fias('type', $.fias.type.building);

            $district.fias('withParents', true);
            $city.fias('withParents', true);
            $street.fias('withParents', true);

            // Отключаем проверку введённых данных для строений
            $building.fias('verify', false);

            // Подключаем плагин для почтового индекса
            $zip.fiasZip($container);

            function setLabel($input, text) {
                text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
                $input.parent().find('label').text(text);
            }

            function showError($input, message) {
                $tooltip.find('span').text(message);

                var inputOffset = $input.offset(),
                    inputWidth = $input.outerWidth(),
                    inputHeight = $input.outerHeight();

                var tooltipHeight = $tooltip.outerHeight();
                var tooltipWidth = $tooltip.outerWidth();

                $tooltip.css({
                    left: (inputOffset.left + inputWidth - tooltipWidth) + 'px',
                    top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
                });

                $tooltip.fadeIn();
            }

            function hideFreeVersion() {
                $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                setTimeout(function () {
                    $('li[title="Бесплатная версия kladr-api.ru"]').remove();
                }, 200);
            }

            $city.on('keyup', function () {
                hideFreeVersion();
            });
            $street.on('keyup', function () {
                hideFreeVersion();
            });
            $building.on('keyup', function () {
                hideFreeVersion();
            });

        })();
    </script>
    <style>
        /* Устанавливаем свой шрифт для выпадающего списка*/
        #kladr_autocomplete a,
        #kladr_autocomplete strong{
            font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
            font-size: 15px;
        }

        #kladr_autocomplete small {
            font-family: MuseoSansCyrl, Arial, Helvetica, sans-serif;
            font-size: 13px;
        }

        /* Добавляем скругления и тень у выпадающего списка*/
        #kladr_autocomplete ul {
            border-radius: 0 0 5px 5px;
            border: 1px solid #ded7f9;
            overflow: hidden;
            background: #fff;
            -webkit-box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
            box-shadow: 0 2px 10px 0 rgba(86, 62, 142, 0.1);
            z-index: 1;
        }

        /* Прописываем стили для тултипа с сообщением об ошибке*/
        .tooltip {
            position: absolute;
            top: 16px;
            left: 360px;
            color: #b94a48;
            padding: 8px 10px;
            border-radius: 5px;
            border: 1px solid #eed3d7;
            background-color: #f2dede;
            opacity: 0.8;
            font-size: 14px;
            z-index: 100000;
        }




        label {
            width: 100%;
            font-weight: 600;
            font-size: 12px;
            line-height: 1.33;
            color: #4b4c50;
            margin-bottom: 5px; }

        input {
            width: 100%;
            font-size: 14px;
            line-height: 1.43;
            color: #212226;
            padding: 10px 15px;
            margin-bottom: 20px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #d7dae4;
            background-color: #f8faff;
            -webkit-transition: border 0.3s ease;
            -o-transition: border 0.3s ease;
            transition: border 0.3s ease; }
        input:hover, input:active, input:focus {
            outline: none;
            border: 1px solid #4f66a9; }

        .modal_input_pas {
            margin-bottom: 10px; }

        .modal_input_reestablish_box {
            margin-bottom: 20px; }

        .modal_input_reestablish {
            font-size: 14px;
            color: #4f66a9;
            position: relative; }
        .modal_input_reestablish::after {
            position: absolute;
            content: '';
            width: 100%;
            height: 1px;
            background-color: #4f66a9;
            display: block;
            -webkit-transform: translateY(0px) scale(0);
            -ms-transform: translateY(0px) scale(0);
            transform: translateY(0px) scale(0);
            -webkit-transition: -webkit-transform 0.2s ease;
            transition: -webkit-transform 0.2s ease;
            -o-transition: transform 0.2s ease;
            transition: transform 0.2s ease;
            transition: transform 0.2s ease, -webkit-transform 0.2s ease; }
        .modal_input_reestablish:hover::after {
            -webkit-transform: translateY(0px) scale(1);
            -ms-transform: translateY(0px) scale(1);
            transform: translateY(0px) scale(1); }

        .modal_input_label {
            font-size: 14px;
            line-height: 1.43;
            color: #8a8f9e;
            margin-top: 25px;
            margin-bottom: 16px; }

        .modal_input_social {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center; }
        .modal_input_social .modal_input_social_btn {
            display: block;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            background-color: #e7eaf1;
            padding: 4px;
            margin-right: 8px; }
        .modal_input_social .modal_input_social_btn:hover img {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1); }
        .modal_input_social img {
            width: 32px;
            max-width: 32px;
            -webkit-transition: -webkit-transform 0.3s ease;
            transition: -webkit-transform 0.3s ease;
            -o-transition: transform 0.3s ease;
            transition: transform 0.3s ease;
            transition: transform 0.3s ease, -webkit-transform 0.3s ease; }

        .modal_registration_pas_box {
            position: relative; }

        .modal_registration_pas_eye {
            position: absolute;
            top: 10px;
            right: 15px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center; }
        .modal_registration_pas_eye img {
            width: 24px;
            max-width: 24px;
            -webkit-transition: -webkit-transform 0.3s ease;
            transition: -webkit-transform 0.3s ease;
            -o-transition: transform 0.3s ease;
            transition: transform 0.3s ease;
            transition: transform 0.3s ease, -webkit-transform 0.3s ease; }
        .modal_registration_pas_eye:hover img {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1); }

        .modal_input_confid {
            font-size: 12px;
            line-height: 1.33;
            color: #8a8f9e;
            margin-top: 5px;
            text-align: center; }
        .modal_input_confid a {
            color: #4f66a9; }
    </style>
@stop
