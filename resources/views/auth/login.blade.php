@extends('layouts.design')

@section('menu_left')
    @include('layouts.menu_left')
@endsection

@section('content')
    <div class="container-fluid" style="padding-bottom: 70px">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <style>
        label {
            width: 100%;
            font-weight: 600;
            font-size: 12px;
            line-height: 1.33;
            color: #4b4c50;
            margin-bottom: 5px; }

        input {
            appearance: auto;
            -webkit-appearance: radio;
            -moz-appearance: radio;
            width: 100%;
            font-size: 14px;
            line-height: 1.43;
            color: #212226;
            padding: 10px 15px;
            margin-bottom: 20px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #d7dae4;
            background-color: #f8faff;
            -webkit-transition: border 0.3s ease;
            -o-transition: border 0.3s ease;
            transition: border 0.3s ease; }
        input:hover, input:active, input:focus {
            outline: none;
            border: 1px solid #4f66a9; }

        .modal_input_pas {
            margin-bottom: 10px; }

        .modal_input_reestablish_box {
            margin-bottom: 20px; }

        .modal_input_reestablish {
            font-size: 14px;
            color: #4f66a9;
            position: relative; }
        .modal_input_reestablish::after {
            position: absolute;
            content: '';
            width: 100%;
            height: 1px;
            background-color: #4f66a9;
            display: block;
            -webkit-transform: translateY(0px) scale(0);
            -ms-transform: translateY(0px) scale(0);
            transform: translateY(0px) scale(0);
            -webkit-transition: -webkit-transform 0.2s ease;
            transition: -webkit-transform 0.2s ease;
            -o-transition: transform 0.2s ease;
            transition: transform 0.2s ease;
            transition: transform 0.2s ease, -webkit-transform 0.2s ease; }
        .modal_input_reestablish:hover::after {
            -webkit-transform: translateY(0px) scale(1);
            -ms-transform: translateY(0px) scale(1);
            transform: translateY(0px) scale(1); }

        .modal_input_label {
            font-size: 14px;
            line-height: 1.43;
            color: #8a8f9e;
            margin-top: 25px;
            margin-bottom: 16px; }

        .modal_input_social {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center; }
        .modal_input_social .modal_input_social_btn {
            display: block;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            background-color: #e7eaf1;
            padding: 4px;
            margin-right: 8px; }
        .modal_input_social .modal_input_social_btn:hover img {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1); }
        .modal_input_social img {
            width: 32px;
            max-width: 32px;
            -webkit-transition: -webkit-transform 0.3s ease;
            transition: -webkit-transform 0.3s ease;
            -o-transition: transform 0.3s ease;
            transition: transform 0.3s ease;
            transition: transform 0.3s ease, -webkit-transform 0.3s ease; }

        .modal_registration_pas_box {
            position: relative; }

        .modal_registration_pas_eye {
            position: absolute;
            top: 10px;
            right: 15px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center; }
        .modal_registration_pas_eye img {
            width: 24px;
            max-width: 24px;
            -webkit-transition: -webkit-transform 0.3s ease;
            transition: -webkit-transform 0.3s ease;
            -o-transition: transform 0.3s ease;
            transition: transform 0.3s ease;
            transition: transform 0.3s ease, -webkit-transform 0.3s ease; }
        .modal_registration_pas_eye:hover img {
            -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
            transform: scale(1.1); }

        .modal_input_confid {
            font-size: 12px;
            line-height: 1.33;
            color: #8a8f9e;
            margin-top: 5px;
            text-align: center; }
        .modal_input_confid a {
            color: #4f66a9; }
    </style>
@endsection
