// require('./bootstrap');
// require('./edit_content');
require('./site');
// require('./users');
// function getRandomArbitrary(min, max) {
//     return Math.random() * (max - min) + min;
// }
$.fn.setCursorPosition = function(pos) {
    if ($(this).get(0).setSelectionRange) {
        $(this).get(0).setSelectionRange(pos, pos);
    } else if ($(this).get(0).createTextRange) {
        var range = $(this).get(0).createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
};
