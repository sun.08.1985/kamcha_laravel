$(document).ready(function () {
    $('body').on('change', '[name="role_id"]', function () {
        if ($(this).val() == '1')
            $('.pupil').removeClass('d-none').find('input').removeAttr('disabled');
        else
            $('.pupil').addClass('d-none').find('input').attr('disabled', 'disabled');
    });
});
