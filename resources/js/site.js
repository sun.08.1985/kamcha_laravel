document.addEventListener('DOMContentLoaded', () => {
    clearEmptyPool();
});

const clearEmptyPool = function (e) {
    $('.mobile-poll').each(function (i) {
        if ($(this).text().replace(/\s{1,}/g, ' ') == ' ') {
            $(this).remove();
        }
    });
    $('.aside-quiz__slide').each(function (i) {
        if ($(this).text().replace(/\s{1,}/g, ' ') == ' ') {
            $(this).remove();
        }
    });
    if ($('.aside-quiz-track').find('*').length == 1) {
        $('.aside-quiz').remove()
    }
};

var loginForm = $("#loginForm");
loginForm.submit(function(e){
    e.preventDefault();
    var formData = loginForm.serialize();

    $.ajax({
        url:'/api/api_login',
        type:'POST',
        data:formData,
        success:function(data){
            if (data === '0') {
                $('#errorLogin').html('Имя пользователя и/или пароль не совпадают.').fadeIn(500).fadeOut(2500);
            } else {
                location.reload();
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
});
