<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthday')->nullable();

            $table->boolean('sms_notification')->default(false);
            $table->boolean('email_notification')->default(false);
            $table->boolean('push_notification')->default(true);

            $table->integer('serial_passport')->nullable();
            $table->integer('number_passport')->nullable();
            $table->text('where_passport')->nullable();
            $table->date('when_passport')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
