<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestCompletedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_complited', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('user_id')->nullable();
	        $table->integer('test_id')->nullable();
	        $table->integer('correct_answer')->nullable();
	        $table->integer('incorrect_answer')->nullable();
	        $table->string('answer_array')->nullable();
	        $table->integer('estimation')->nullable();
	        $table->integer('travel_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_complited');
    }
}
