<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->nullable();
            $table->string('slug')->unique();
            $table->string('title')->nullable();

            $table->string('image_url')->nullable();

            $table->string('keywords')->nullable();
            $table->string('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('menu_news', 3)->nullable();

            $table->integer('adress_id')->nullable();
            $table->integer('user_id')->nullable();

            $table->integer('see_count')->default(0);

            $table->bigInteger('category_id')->unsigned()->index()->default(1);
            $table->foreign('category_id')->references('id')->on('categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
