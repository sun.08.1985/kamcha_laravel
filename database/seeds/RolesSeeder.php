<?php

    use Illuminate\Database\Seeder;

    class RolesSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            DB::table('roles')->insert([
                [ // 1
                    'name' => 'Неавторизованный пользователь',
                    'type' => 'user',
                ],
                [ // 2
                    'name' => 'Авторизованный пользователь',
                    'type' => 'super_user',
                ],
                [ // 3
                    'name' => 'Редактор',
                    'type' => 'editor',
                ],
                [ // 4
                    'name' => 'Автор',
                    'type' => 'author',
                ],
                [ // 5
                    'name' => 'Администратор',
                    'type' => 'admin',
                ],
                [ // 6
                    'name' => 'Всемогущий админ',
                    'type' => 'super_admin',
                ],
            ]);
        }
    }
