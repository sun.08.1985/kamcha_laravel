<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AnswersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        DB::table('answers')->insert([
            [
                'question_id' => 1,
                'title' => $faker->lastName(),
                'correct' => 'on'
            ],
            [
                'question_id' => 1,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 1,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 1,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 2,
                'title' => $faker->lastName(),
                'correct' => 'on'
            ],
            [
                'question_id' => 2,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 2,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 2,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 3,
                'title' => $faker->lastName(),
                'correct' => 'on'
            ],
            [
                'question_id' => 3,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 3,
                'title' => $faker->lastName(),
                'correct' => null
            ],
            [
                'question_id' => 3,
                'title' => $faker->lastName(),
                'correct' => null
            ],
        ]);
    }
}
