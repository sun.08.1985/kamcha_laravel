<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
//            [
//                'title' => 'Главная',
//                'slug' => 'main',
//                'menu_header' => 'on',
//                'content' => '
//
//                ',
//                'order' => 0
//            ],
            [
                'title' => 'О проекте',
                'slug' => 'about',
                'menu_header' => 'on',
                'content' => '',
                'order' => 1
            ],
            [
                'title' => 'Контакты',
                'slug' => 'contacts',
                'menu_header' => 'on',
                'content' => '',
                'order' => 9
            ],

        ]);
    }
}
