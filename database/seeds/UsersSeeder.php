<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        DB::table('users')->insert([
            [
                'name' => $faker->firstName('male'),
                'email' => 'super_admin@kamcha.ru',
                'password' => bcrypt('super_admin'),
                'role_id' => 6,
                'adress_id' => 1,
                'score' => null,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
            [
                'name' => $faker->firstName('male'),
                'email' => 'admin@kamcha.ru',
                'password' => bcrypt('admin'),
                'role_id' => 5,
                'adress_id' => 1,
                'score' => null,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
            [
                'name' => $faker->firstName('female'),
                'email' => 'author@kamcha.ru',
                'password' => bcrypt('author'),
                'role_id' => 4,
                'adress_id' => 1,
                'score' => null,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
            [
                'name' => $faker->firstName('male'),
                'email' => 'editor@kamcha.ru',
                'password' => bcrypt('editor'),
                'role_id' => 3,
                'adress_id' => 1,
                'score' => null,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
            [
                'name' => $faker->firstName('female'),
                'email' => 'super_user@kamcha.ru',
                'password' => bcrypt('super_user'),
                'role_id' => 2,
                'adress_id' => 1,
                'score' => null,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
            [
                'name' => $faker->firstName('male'),
                'email' => 'user@kamcha.ru',
                'password' => bcrypt('user'),
                'role_id' => 1,
                'adress_id' => 1,
                'score' => 0,
                'phone' => $faker->phoneNumber,
                'api_token' => Str::random(80),
            ],
        ]);
    }
}
