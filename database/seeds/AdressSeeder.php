<?php

use Illuminate\Database\Seeder;

class AdressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adresses')->insert([
            [
                'city' => 'Ключи',
                'street' => 'Чайковского',
                'building' => '23',
                'appartment' => '1',
            ]
        ]);
    }
}
