<?php

use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        DB::table('news')->insert([
            [
                'image_url' => 'http://ust-kam.ru/images/cms/thumbs/8d0434774c9fae0cca0651eaeab9e218f0d2696d/1_186_auto_5_80.jpg',
                'title' => 'Особенная путина-2020',
                'slug' => Str::slug('Особенная путина-2020'),
                'content' => 'В этом году в Усть-Камчатске с 1 июня началась добыча лосося пока только на трёх контрольных участках реки Камчатки. Основной промысел отложен примерно на неделю (+фоторепортаж).
В Камчатском крае официально начинается лососёвая путина с 1 июня. Но в целях сохранения популяции ценной рыбы промысел перенесён на несколько дней до обеспечения оптимального заполнения нерестилищ – пока до 6 июня.
В предыдущие два года была отмечена недостаточная численность прошедших в реку Камчатку производителей ранней формы нерки. В связи с этим впервые определено, что на морских участках Камчатского залива промысел начнётся только после пропуска на нерест 120-150 тысяч производителей нерки в целом или 30-60 тысяч экземпляров ранней формы нерки в бассейне озера Ажабачье. Учёт рыбы ежедневно ведут специалисты КамчатНИРО. За первый день, по словам министра рыбного хозяйства Камчатского края Андрея Здетоветского, пропуск в реку Камчатку составил уже 40 тысяч экземпляров.
- Промышленный лов на реке Камчатке пока закрыт, оставлено только три контрольных рыбалки. 1 июня добыча лосося началась, и первый день сложился неплохо. На каждом контрольном участке рыбаки сделали по шесть забросов сетей и показали хорошие результаты: от 600 кг до 1 тонны за сплав. Можно предположить, что толчок рыбы прошёл дней пять назад. В сети попадались чавыча и нерка, последней около 80 % улова. Это говорит о том, что её уже зашло на нерест довольно приличное количество. Будем надеяться, что необходимое число производителей лосося пополнится в скором времени и оперативно откроют привычный промысел лосося, - рассказал президент Усть-Камчатской ассоциации по охране водных биоресурсов Михаил Коваленко.
- Мы очень рады, что Усть-Камчатские предприятия, и наше в том числе, приняли эстафету лососёвой путины. Слава Богу, что пандемия коронавируса никак не сказалась на её сроке старта: все у нас живы-здоровы, - поделилась мастер-технолог рыбоперерабатывающего завода ООО «Дельта Фиш ЛТД» Татьяна Гамза.
Действительно, путина-2020 имеет свои особенности из-за распространения коронавирусной инфекции, заражённых которой, кстати, в Усть-Камчатске нет. Определённые условия, в частности двухнедельную изоляцию, проходили приезжие из других регионов работники рыбных предприятий. В то же время в этой ситуации больше камчатцев изъявили желание поработать на сезонном промысле.
- Режим самоизоляции дал возможность нашим жителям Усть-Камчатского района в большем количестве устроиться на работу в связи с ограничением приезда жителей из других территорий Российской Федерации. Отмечу, что это и дополнительные поступления налогов в бюджет района. Уже третий год путина в Усть-Камчатске, и по прогнозам, и по вылову складывается, наверно, не очень удачно. Но мы люди с оптимизмом и надеемся, что этот год для добычи лосося будет более перспективным и радостным, - сказал глава Усть-Камчатского района  Василий Логинов.
Последние пару лет уловы не очень радуют усть-камчатских рыбаков: в 2019-м они составили чуть более 12 тысяч тонн лосося, в 2018-м около 14,5 тысяч тонн, тогда как в 2017-м путина стала рекордной: тогда усть-камчатские предприятия взяли 23 тысячи тонн ценных пород рыбы.
28 мая в Усть-Камчатске уже традиционно перед началом летнего промысла лососей состоялось заседание межведомственного объединённого штаба. Помимо хода и особенностей предстоящей лососёвой путины-2020 во время заседания особое внимание, как всегда, уделили борьбе с браконьерством. Отмечено, что в прошлую путину благодаря слаженному взаимодействию силовых структур и рыбопромышленников Усть-Камчатского района были сведены к минимуму риски с незаконной добычей лососей. Несмотря на это, в этом году меры усиливаются.
- В Усть-Камчатский район дополнительно мы направляем четыре группы оперативной инспекции из Елизовского района. Они будут базироваться на посту в п. Ключи, который будет выставлен, как и в 2019 году. Помимо этого мы такой пост организуем и в Усть-Камчатске, - отметил врио руководителя СВТУ ФАР Юрий Татаринов.
Он добавил, что наши рыбопромышленники к тому же помогли в подготовке трёх контрольных пунктов в верхнем течении реки Камчатки – в Мильковском районе.
Основной промысел начнётся, как только на нерест пройдёт определённое количество ранней нерки. В целом, на путину-2020 учёные дают средний прогноз: лимит для усть-камчатских компаний составляет 9000 тонн рыбы, из которых 5,5 тысяч нерки. Безусловно, желаем рыбакам и обработчикам их освоить!',
                'created_at' => '2020-06-02 00:00:00'
            ],
            [
                'image_url' => 'http://ust-kam.ru/images/cms/thumbs/6d92b8ab4b6aa472bb84dfc7120979588141a62e/111111111111_186_auto_5_80.jpg',
                'title' => 'КАК ЖИВЕТЕ, КАРАПУЗЫ? РОССИЙСКИЕ ДЕТИ ГЛАЗАМИ СТАТИСТИКИ',
                'slug' => Str::slug('КАК ЖИВЕТЕ, КАРАПУЗЫ? РОССИЙСКИЕ ДЕТИ ГЛАЗАМИ СТАТИСТИКИ'),
                'content' => 'С 1 июня начинаются выплаты на детей от 3 до 16 лет. В мае президент России Владимир Путин подписал указ о дополнительных мерах социальной поддержки российских семей с детьми. В Международный день защиты детей рассказываем, какова доля детей в общем населении страны, почему подростков скоро будет больше и о чем говорят цифры обеспеченности местами в детсадах.
По оценке Росстата, доля детей и подростков в возрасте до 18 лет, постоянно проживающих в России, составляет 22,4% от общей численности населения страны. «Это самая высокая доля детей в общей структуре населения страны за последние 10 лет — в 2010 году дети и подростки составляли 21,4% населения», — рассказала Медиаофису Всероссийской переписи населения заведующая лабораторией количественных методов исследования регионального развития РЭУ имени Г.В. Плеханова Елена Егорова. По ее словам, увеличение доли детей и подростков обусловлено ростом рождаемости в 2012–2017 годах.
Одновременно меняется и возрастная структура этой группы: доля малышей до 4 лет увеличилась с 26,5% в 2010 году до 28% в 2019 году. Доля детей в возрасте 5–10 лет выросла с 23,6 до 24,3%. При этом доля подростков 10–15 и 16–18 лет немного снизилась, но по мере взросления сегодняшних малышей она будет возрастать, полагает Егорова.
Актуальной остается проблема с обеспеченностью местами в дошкольных учреждениях — в настоящее время на 1000 детей в возрасте 1–6 лет в целом по России приходится 639 мест в детских садах. В городе на 1000 детей приходится 671 место, на селе — 549 мест. В 2010 году этот показатель составил 674 и 553 места соответственно. «Это говорит о том, что наша сложная демографическая ситуация — волны повышения или снижения рождаемости — меняет уровни обеспеченности детей соответствующими учреждениями и услугами, — отмечает Егорова. — Если сегодня может хватать мест в детском саду, то завтра может возникнуть их дефицит».
В нашу жизнь вернулась практика профилактических осмотров и диспансеризации — в настоящее время ими охвачено 96,7% детей и подростков. За последние 10 лет значительно — на 72% — сократилась заболеваемость детей вследствие осложнений беременности, родов и послеродового периода. На 15% снизилась заболеваемость психическими расстройствами, на 14% сократились болезни крови и кроветворных органов. Однако общая заболеваемость детей и подростков за 10 лет выросла на 10,9%.
За последнее десятилетие снизилась доля детей, родители которых лишены родительских прав. Сейчас в среднем на 10 тыс. человек приходится 13 детей в возрасте до 17 лет, чьи родители лишены родительских прав. В 2010 году было 25 таких детей. Подобная ситуация сложилась не только из-за возросшей ответственности взрослых, но и из-за того, что изменилась общественная установка: как бы то ни было, ребенку лучше в семье. Поэтому органы опеки и суд чаще стали применять ограничение родителей в правах. Это дает возможность возвращения к нормальному образу жизни — сейчас из 100 тыс. детей в возрасте до 17 лет 32 ребенка имеют родителей с ограниченными правами. Десятилетие назад таких детей по статистике было 29.
Точные данные о том, сколько детей в стране и в каких условиях они проживают, для формирования новых социальных программ можно получить только с помощью специальных выборочных исследований и Всероссийской переписи населения. Ранее планировалось, что основной этап Всероссийской переписи населения пройдет с 1 по 31 октября 2020 года. В апреле Росстат выступил с предложением перенести ее на 2021 год.
Всероссийская перепись населения пройдет с применением цифровых технологий. Главным нововведением предстоящей переписи станет возможность самостоятельного заполнения жителями России электронного переписного листа на портале Госуслуг (Gosuslugi.ru). При обходе жилых помещений переписчики будут использовать планшеты со специальным программным обеспечением. Также переписаться можно будет на переписных участках, в том числе в помещениях многофункциональных центров оказания государственных и муниципальных услуг «Мои документы».

Медиаофис ВПН-2020
media@strana2020.ru
www.strana2020.ru
+7 (495) 933-31-94
https://www.facebook.com/strana2020
https://vk.com/strana2020
https://ok.ru/strana2020
https://www.instagram.com/strana2020
youtube.com',
                'created_at' => '2020-06-02 00:00:00'
            ],
            [
                'image_url' => 'http://ust-kam.ru/images/cms/thumbs/6d92b8ab4b6aa472bb84dfc7120979588141a62e/pozdravlenie_s_dnyom_zawity_detej_186_auto_5_80.jpg',
                'title' => 'Поздравление главы Усть-Камчатского муниципального района и председателя Совета народных депутатов Усть-Камчатского муниципального района с Днём защиты детей',
                'slug' => Str::slug('Поздравление главы Усть-Камчатского муниципального района и председателя Совета народных депутатов Усть-Камчатского муниципального района с Днём защиты детей'),
                'content' => 'Дорогие мальчишки и девчонки, а также их родители, бабушки и дедушки! Поздравляем вас с Днём защиты детей!
Этот праздник напоминает нам, взрослым, о той огромной ответственности, которую мы несём за жизнь и здоровье детей. Мы должны окружить их любовью, заботой и вниманием, оградить от зла и жестокости, сохранить мирное небо над головой. Мы обязаны приложить все усилия для того, чтобы каждый ребёнок мог реализовать своё самое главное право – право на счастливое детство.
В этот праздничный день благодарим все семьи, в которых дети растут в атмосфере любви, заботы и понимания. Отдельные слова признательности – многодетным и приёмным родителям. Вы делаете большое дело и всегда будете получать необходимую поддержку.
Желаем вам крепких семей, душевного тепла и уюта в ваших домах! Пусть у наших мальчишек и девчонок будет радостное, счастливое детство, а их мечты всегда исполняются!

Председатель Совета народных депутатов
Усть-Камчатского муниципального района
И. В. Шубенко

Глава
Усть-Камчатского муниципального района
В. И. Логинов',
                'created_at' => '2020-06-01 00:00:00'
            ],
            [
                'image_url' => 'http://ust-kam.ru/images/cms/thumbs/6d92b8ab4b6aa472bb84dfc7120979588141a62e/rezhim_prodlyon_186_auto_5_80.jpg',
                'title' => 'Ограничительные меры сохранятся на Камчатке до 11 июня',
                'slug' => Str::slug('Ограничительные меры сохранятся на Камчатке до 11 июня'),
                'content' => 'Действующие в Камчатском крае ограничительные меры, направленные на профилактику распространения коронавирусной инфекции, будут продлены после 31 мая.
Послабления возможны только после 11 июня, в случае благоприятных показателей по заболеваемости.
Руководитель Управления Федеральной службы по надзору в сфере защиты прав потребителей и благополучию человека по Камчатскому краю Наталья Жданова сообщила о сохраняющейся в регионе неблагоприятной санитарно-эпидемиологической ситуации.
«Ситуация не способствует снятию каких-либо ограничений, о «плато» говорить пока рано. Из регионов мы одними из последних вошли в эту пандемию. И сейчас все процессы развиваются на Камчатке с некоторым опозданием, если сравнивать с Москвой и центральной Россией. Всё, что случается там, происходит у нас на две-три недели позже. Поэтому я призываю всех сейчас не делать лишних движений, сократить все контакты, по возможности оставаться дома в ближайшие две недели и всегда использовать средства защиты. Многие жители края заболели именно после майских праздников, когда выходили из дома и ездили на дачи», – пояснила Наталья Жданова.
В результате региональный штаб принял решение продлить все действующие ограничения минимум до 11 июня. Как было отмечено на заседании, за это время будет разработан план по постепенному смягчению ограничительных мер. Его задействуют после того, как ситуация стабилизируется.
В том числе ограничения продолжат действовать и по льготным проездным в городском общественном транспорте. Кроме того, в выходные будет усилен контроль за соблюдением масочного режима в автобусах, в том числе на пригородных маршрутах. В случае нарушений водитель либо пассажиры будут оштрафованы.
kamgov.ru',
                'created_at' => '2020-05-29 00:00:00'
            ],
            [
                'image_url' => 'http://ust-kam.ru/images/cms/thumbs/6d92b8ab4b6aa472bb84dfc7120979588141a62e/brt_702_186_auto_5_80.jpg',
                'title' => 'Премия «Дальний Восток» им. В.К. Арсеньева',
                'slug' => Str::slug('Премия «Дальний Восток» им. В.К. Арсеньева'),
                'content' => 'Камчатских авторов приглашают на виртуальную встречу с членами жюри премии «Дальний Восток» им. В.К. Арсеньева.
Круглый стол в онлайн-формате состоится 2 июня для представителей Камчатского края и Магаданской области.
«Первооткрывателем» стало Приморье – встреча в ZOOM прошла в регионе 26 мая. Прямой эфир транслировался на сайте премияарсеньева.рф.
Участники круглых столов – писатели федерального уровня, представители министерств культуры субъектов Дальнего Востока, представители регионального отделения Союза писателей РФ, региональные писатели, представители региональных издательств и вузов. Модератор – куратор премии «Дальний Восток» им. В.К. Арсеньева Вячеслав Коновалов.
«В условиях, когда население находится в состоянии неопределённости, неуверенности в завтрашнем дне, беспокоится за здоровье и судьбу своих близких, но в то же время имеет массу свободного времени, самое время переключить эту энергию в русло конструктивного мышления и интеллектуального, культурного диалога. И писательская элита страны в прямом смысле может стать «властителем дум», объединить и сосредоточить внимание аудитории на простых и позитивных вещах», – отметили организаторы.
Вторая литературная премия «Дальний Восток» им. В.К. Арсеньева набирает обороты. Для привлечения к ней внимания общественности запланирована серия активностей, в том числе и круглые столы в регионах Дальнего Востока. С учётом неблагоприятной эпидемиологической ситуации мероприятия пройдут в онлайн-формате.
Напомним, первым мероприятием в онлайн-формате стала пресс-конференция, в рамках которой был объявлен состав жюри литературной премии «Дальний Восток» им. В.К. Арсеньева сезона 2019-2020 и продлены сроки подачи заявок до 30 июля. Также на пресс-конференции прозвучали предложения дополнить три имеющиеся номинации поэтической и издать альманах, представляющий творчество лауреатов и номинантов, вошедших в шорт-лист.
kamgov.ru',
                'created_at' => '2020-05-29 00:00:00'
            ],
        ]);
    }
}
