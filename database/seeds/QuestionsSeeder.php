<?php

use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            [
                'test_id' => 1,
                'title' => 'Председатель совета'
            ],
            [
                'test_id' => 1,
                'title' => 'Помощник воспитателя'
            ],
            [
                'test_id' => 1,
                'title' => 'Заслуженный артист'
            ]
        ]);
    }
}
