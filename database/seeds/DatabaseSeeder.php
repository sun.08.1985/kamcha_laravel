<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersSeeder::class);
         $this->call(PagesSeeder::class);
         $this->call(RolesSeeder::class);
         $this->call(TopicsSeeder::class);
         $this->call(TestsSeeder::class);
         $this->call(QuestionsSeeder::class);
         $this->call(AnswersSeeder::class);
         $this->call(AdressSeeder::class);
         $this->call(CategoriesSeeder::class);
         $this->call(NewsSeeder::class);
    }
}
