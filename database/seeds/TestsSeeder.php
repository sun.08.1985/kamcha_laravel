<?php

use Illuminate\Database\Seeder;

class TestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tests')->insert([
            [
                'title' => 'Выборы',
                'topic_id' => 1,
                'time_estimation' => 15
            ]
        ]);
    }
}
