<?php

    namespace App\Http\Controllers;

    use App\Category;
    use Illuminate\Http\Request;
    use Illuminate\Support\Str;
    use App\News;
    use App\Http\Requests\NewValidation;
    use Illuminate\Support\Facades\DB;
    use tizis\laraComments\Http\CommentsHelper;
    use Image;

    class NewsController extends Controller
    {
        public function index()
        {
            $news = DB::table('news')->paginate(15);

            return view('news.index', ['news' => $news]);
        }

        public function create()
        {
            $categories = Category::all();
            return view('news.create', ['categories' => $categories]);
        }

        public function store(NewValidation $request)
        {
//            dd($request);
            if (env('APP_PUSH', true)) {

                try {
                    $tags = [];
                    $all = [];
                    $data_string = [];
                    $c = false;
                    $s = false;
                    $b = false;

                    if ($request->input('city') !== null) {

                        if ($request->input('street') !== null) {

                            if ($request->input('building')) {

                                $tmp = $request->input('city') . ', ' . $request->input('street') . ', д.' . $request->input('building');
                                array_push($tags, $tmp);
                                $b = true;

                            }

                            if (!$b) {

                                $tmp = $request->input('city') . ', ' . $request->input('street');
                                array_push($tags, $tmp);
                                $s = true;

                            }

                        }

                        if (!$s && !$b) {

                            $tmp = $request->input('city');
                            array_push($tags, $tmp);
                            $c = true;

                        }

                    }

                    //            dd($tags);


                    if ($c || $s || $b)
                        $data['audience']['tags'] = $tags;


                    $data['payload']['message'] = Str::limit($request->input('description'), 200) ?? 'Без описания';
                    $data['payload']['title'] = $request->input('title');
                    $data['payload']['redirect_url'] = env('APP_URL', '') . '/news/' . $request->input('slug');

                    $data_string = json_encode($data, JSON_UNESCAPED_UNICODE);

                    //            dd($data);

                    $ch = curl_init('https://uapi.gravitec.net/api/v3/push');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_USERPWD, env('GRAVITEC_APIKEY', '') . ':' . env('GRAVITEC_APISECRET', ''));
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    $result = curl_exec($ch);
                } catch (Exception $e) {
                    Session::put('error', 'Не было отправлено PUSH сообщение');
                }
            }

            $slug = $request->get('slug');
            $title = $request->get('title');
            if (empty($slug)) {
                $slug = Str::slug($title);
                $request->merge([
                    'slug' => $slug,
                ]);
            }

            $new = News::create($request->all());

            if($request->hasFile('image_url'))
            {
                $image = $request->file('image_url');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save( public_path('/uploads/news/' . $filename ) );

                $new->image_url = '/uploads/news/' . $filename;
                $new->save();
            }

            return redirect()->route('news.slug', $new->slug);
        }

        public function edit($id)
        {
            $new = News::find($id);
            $categories = Category::all();
            return view('news.edit', ['new' => $new, 'categories' => $categories]);
        }

        public function showBySlug($slug)
        {
            $new = News::where('slug', $slug)->first();
            if ($new)
                return $this->show($new->id);
                //return view('news.view', ['new' => $new]);
            else
                return abort(404);
        }

        public function show($id)
        {
            $new = News::find($id);

//            if (strpos(\request()->path(), 'api') == false)
//                return $new;
            $new->see_count++;
            $new->save();

            $news_comment = CommentsHelper::getNewestComments(20);

            return view('news.view', ['new' => $new, 'news_comment' => $news_comment]);
        }

        public function update(NewValidation $request, $id)
        {
//            dd($request);
            $menu_news = 'on';
            if (!$request->has('menu_news'))
                $menu_news = null;

            $new = News::find($id);

            $slug = $request->get('slug');
            $title = $request->get('title');
            if (empty($slug)) {
                $slug = Str::slug($title);
                $request->merge([
                    'slug' => $slug,
                ]);
            }

            $new->fill($request->all());
            $new->menu_news = $menu_news;

            if($request->hasFile('image_url'))
            {
                $image = $request->file('image_url');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save( public_path('/uploads/news/' . $filename ) );

                $new->image_url = '/uploads/news/' . $filename;
            }

            $new->save();

            return redirect()->route('news.slug', $new->slug);
        }

        public function destroy($id)
        {
            News::find($id)->delete($id);

            return redirect()->route('news.index');
        }

        public function category()
        {
            return belongsTo(\App\Category::class, 'category_id');
        }

    }
