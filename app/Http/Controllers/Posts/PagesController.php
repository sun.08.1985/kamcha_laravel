<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Http\Requests\PageValidation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PagesController extends Controller
{
    public function index()
    {
        $pages = DB::table('pages')->paginate(15);

        return view('pages.index', ['pages' => $pages]);
    }

    public function create()
    {
        return view('pages.create');
    }

    public function store(PageValidation $request)
    {
        $slug = $request->get('slug');
        $title = $request->get('title');
        if (empty($slug)) {
            $slug = Str::slug($title);
            $request->merge([
                'slug' => $slug,
            ]);
        }
        $page = Page::create($request->all());
        return redirect()->route('pages.slug', $page->slug);
    }

    public function edit($id)
    {
        $page = Page::find($id);

        return view('pages.edit', ['page' => $page]);
    }

    public function showBySlug($slug)
    {
        $page = Page::where('slug', $slug)->first();
//        dd($page);
        if ($page)
            return view('pages.view', ['page' => $page]);
        else
            return abort(404);
    }

    public function show($id)
    {
        $page = Page::find($id);
//        if (strpos(\request()->path(), 'api') == false)
//            return $page;
        return view('pages.view', ['page' => $page]);
    }

    public function update(PageValidation $request, $id)
    {
        $menu_header = 'on'; $menu_footer = 'on';
        if (!$request->has('menu_header'))
            $menu_header = null;
        if (!$request->has('menu_footer'))
            $menu_footer = null;

        $page = Page::find($id);

        $slug = $request->get('slug');
        $title = $request->get('title');
        if (empty($slug)) {
            $slug = Str::slug($title);
            $request->merge([
                'slug' => $slug,
            ]);
        }

        $page->fill($request->all());
        $page->menu_header = $menu_header;
        $page->menu_footer = $menu_footer;

        $page->save();

        return redirect()->route('pages.slug', $page->slug);
    }

    public function destroy($id)
    {
        Page::find($id)->delete($id);

        return redirect()->route('pages.index');
    }
}
