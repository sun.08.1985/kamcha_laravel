<?php

namespace App\Http\Controllers;

use App\Mail\Authorization;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use tizis\laraComments\Http\CommentsHelper;
use Session;

class WelcomeController extends Controller
{
    public function index (Request $request) {
        $news = News::orderBy('created_at', 'DESC')->paginate(3);

        if ($request->ajax()) {
//            if ($request->search) {
//                $news = News::where('title','LIKE',"%".$request->search."%");
////                dd($news);
//                if ($news)
//                {
//                    $view = view('news.data', compact('news'))->render();
//                    return response()->json(['html'=>$view]);
//                } else {
//                    $news = News::orderBy('created_at', 'DESC')->paginate(3);
//                }
//            }

            if($request->has('q')) {
                $error = ['error' => 'No results found, please try with different keywords.'];
                // Используем синтаксис Laravel Scout для поиска по таблице products.
                $news = News::where('title', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3);
//                $news->merge(News::where('description', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3));
//                $news->merge(News::where('content', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3));

                // Если есть результат есть, вернем его, если нет  - вернем сообщение об ошибке.
//            return $posts->count() ? $posts : $error;
//                return view('search', ['news' => $news, 'news_comment' => $news_comment]);
                $view = view('news.data', compact('news'))->render();
                return response()->json(['html'=>$view]);
            }

            $view = view('news.data', compact('news'))->render();
            return response()->json(['html'=>$view]);
        }

        $news_comment = CommentsHelper::getNewestComments(20);

        if($request->has('q')) {
            $error = ['error' => 'No results found, please try with different keywords.'];
            // Используем синтаксис Laravel Scout для поиска по таблице products.
            $news = News::where('title', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3);
//            $news->merge(News::where('description', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3));
//            $news->merge(News::where('content', 'LIKE', '%'.$request->get('q').'%')->orderBy('created_at', 'DESC')->paginate(3));

            // Если есть результат есть, вернем его, если нет  - вернем сообщение об ошибке.
//            return $posts->count() ? $posts : $error;
            return view('search', ['news' => $news, 'news_comment' => $news_comment]);
        }
        return view('welcome', ['news' => $news, 'news_comment' => $news_comment]);
    }

    public function feedback(Request $request)
    {
        $data = $request->toArray();
//dd($data);
        try {
            Mail::send('emails.feedback', $data, function ($mail) use ($data, $request) {
                $mail->to('sun.08.1985@gmail.com', 'Дмитрий')
                    ->cc('tutaom@gmail.com')
                    ->cc('kobba@bk.ru')
                    ->attach($request->file('image')->getRealPath(), [
                        'as' => $request->file('image')->getClientOriginalName(),
                        'mime' => $request->file('image')->getMimeType()
                    ])
                    ->subject('Письмо с сайта');
            });
            Session::put('complited', 'Спасибо за ваше обращение! Письмо успешно отправлено.');
//                ->cc('kobba@bk.ru')

        } catch (Exception $e) {
            Session::put('error', 'Не было отправлено письмо на почту');
//            dd($e);
        }

        return Redirect::back();//->with('complited', 'Ваше сообщение отправлено');
    }
}
