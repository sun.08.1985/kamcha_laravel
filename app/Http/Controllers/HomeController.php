<?php

namespace App\Http\Controllers;

use App\Adress;
use App\News;
use App\Page;
use App\TestCompleted;
use App\Topic;
use Illuminate\Http\Request;
use App\Test;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $topics = Topic::all();
        if ($user->hasRoles(['user', 'super_user'])) {
            $tests_ids = json_decode($user->current_test_id);

//            $tests = Test::all()->where('id');
            $tests = collect([]);

            if (!empty($tests_ids))
                foreach ($tests_ids as $tests_id) {
                    $tests = $tests->merge(Test::All()->where('id', $tests_id));
                }
            $tests = Test::all();
//dd($tests);

            $topics = Topic::all();
            return view('pupil.home', ['tests' => $tests, 'topics' => $topics]);
        } elseif ($user->hasRole('supeaar_user')) { // РОДИТЕЛЬ
//            dd(Adress::users(Auth::id()));
//            $users = User::All()->where('family_id', Auth::user()->family_id)->
//                        merge(User::All()->where('family_id', Auth::user()->id));
//            $childs = json_decode(Auth::user()->child_id);
//
//            if (is_array($childs))
//                foreach ($childs as $child) {
//                    $users = $users->merge(User::All()->where('id', $child));
//                }
//            else
//                $users = collect([]);

            $users = User::childs();
            $tests = collect([]);

            foreach ($users as $user) {
                $tests = $tests->merge(TestCompleted::All()->where('user_id', $user->id));
            }

            return view('home', ['tests' => $tests, 'topics' => $topics]);
        } elseif ($user->hasRoles(['editor', 'author', 'admin', 'super_admin'])) {
//            $tests = TestCompleted::all();
            $news = News::all();
            $pages = Page::all();
            return view('home', ['news' => $news, 'pages' => $pages]);
        }
    }

    public function chart()
    {
        if (Auth::user()->hasRoles(['super_admin', 'admin'])) {
            $result = TestCompleted::all();
        } else {
            $result = collect();
            foreach (User::childs() as $child) {
                $child_id = $child->id;
                $result = $result->merge(TestCompleted::all()->where('user_id', $child_id));
            }
            $result_finish = $result->keyBy('created_at')->sortBy('created_at');

            dd($result_finish);
        }
        return response()->json($result_finish);
    }

}
