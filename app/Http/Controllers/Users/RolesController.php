<?php

namespace App\Http\Controllers;

use App\Role;
use App\Http\Requests\RoleValidation;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', ['roles' => $roles]);
    }

    public function create()
    {
        return view('roles.create');
    }

    public function store(RoleValidation $request)
    {
        $role = Role::create($request->all());

        return redirect()->route('roles.show', $role);
    }

    public function edit($id)
    {
        $role = Role::find($id);

        return view('roles.edit', ['role' => $role]);
    }

    public function show($id)
    {
        $role = Role::find($id);

        return view('roles.view', ['role' => $role]);
    }

    public function update(RoleValidation $request, $id)
    {
        $role = Role::find($id);

        $role->fill($request->all());
        $role->save();

        return redirect()->route('roles.index');
    }

    public function destroy($id)
    {
        Role::find($id)->delete($id);

        return redirect()->route('roles.index');
    }
}
