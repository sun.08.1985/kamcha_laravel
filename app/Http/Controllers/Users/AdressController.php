<?php

namespace App\Http\Controllers;

use App\Adress;
use App\Http\Requests\AdressValidation;
use Illuminate\Http\Request;

class AdressController extends Controller
{
    public function index()
    {
        $adresses = Adress::all();
        return view('adresses.index', ['adresses' => $adresses]);
    }

    public function create()
    {
        return view('adresses.create');
    }

    public function store(AdressValidation $request)
    {
        $role = Adress::create($request->all());

        return redirect()->route('adresses.show', $role);
    }

    public function edit($id)
    {
        $role = Adress::find($id);

        return view('adresses.edit', ['role' => $role]);
    }

    public function show($id)
    {
        $role = Adress::find($id);

        return view('adresses.view', ['role' => $role]);
    }

    public function update(AdressValidation $request, $id)
    {
        $role = Adress::find($id);

        $role->fill($request->all());
        $role->save();

        return redirect()->route('adresses.index');
    }

    public function destroy($id)
    {
        Adress::find($id)->delete($id);

        return redirect()->route('adresses.index');
    }
}
