<?php

namespace App\Http\Controllers;

use App\Adress;
use App\Mail\CreateUser;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use App\Http\Requests\AssignTestValidation;
use App\Http\Requests\UserValidation;
use App\Http\Requests\UserUpdateValidation;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Image;
//use Illuminate\Support\Facades\Auth;


class UsersController extends Controller
{
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $users = User::all();
        } else {
//            $user = new Collection();
            $id = Auth::user()->id;

//            dd(Auth::user()->family);
            $users = User::All()->where('id', $id);
//            $users = $users->merge(User::All()->where('family_id', Auth::user()->id));//User::All()->where('family_id', Auth::user()->id));

//            if (Auth::user()->family_id != null) {
//                $users = $users->merge(User::All()->where('family_id', Auth::user()->family_id));
//                $users = $users->merge(User::All()->where('id', Auth::user()->family_id));
//            }

//            if (Auth::user()->child_id != null) {
//                $childs = json_decode(Auth::user()->child_id);
//                foreach ($childs as $child) {
//                    $users = $users->merge(User::All()->where('id', $child));
//                }
//            }
//            dd($users);
        }
//        dd(DB::table('users')->where('family_id', '=', $id)->get());
            return view('users.index', ['users' => $users]);
    }

    public function create()
    {
        $roles = Role::all()->where('id', '<=', Auth::user()->role_id);
        return view('users.create', ['roles' => $roles]);
    }

    public function store(UserValidation $request)
    {
        $pass = $request->input('password');
//        dd(Hash::make($pass));
//dd($pass);
        $data = $request->toArray();
        $adress = Adress::firstOrNew(['city' => $data['city'], 'street' => $data['street'], 'building' => $data['building'], 'appartment' => $data['appartment']]);
        $adress->save();

        dd($data);
        $user = User::create($request->all());
        $user->family_id = Auth::user()->id;
        $user->adress_id = $adress->id;
        $user->password = Hash::make($pass);

        $user->save();
        return redirect()->route('admin');
    }

    public function assign($id)
    {
        $user = User::find($id);

        return view('users.assign', ['user' => $user]);
    }

    public function assign_pupil($test_id)
    {
        $id = Auth::user()->id;
        $users = DB::table('users')->where('role_id', 1)->where('family_id', '!=', null)->where('family_id', $id)->orWhere('id', $id)->get();

        return view('users.assign_pupil', ['users' => $users, 'test_id' => $test_id]);
    }

    public function assigned(AssignTestValidation $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();

        return redirect()->route('exams.list', $user);
    }

    public function assigned_pupil(AssignTestValidation $request, $test_id)
    {
        $user = User::find($request->only('user_id'))->first();
        $tests_ids = json_decode($user->current_test_id);

        if (is_array($tests_ids)) {
            if (!in_array($test_id, $tests_ids)) {
                $tests_ids[] = $test_id;
            }
        }
        else {
            $tests_ids = [$test_id];
        }

        $user->current_test_id = json_encode($tests_ids);
        $user->save();

        return redirect()->route('exams.list');
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('users.edit', ['user' => $user]);
    }

    public function show($id)
    {
        $user = User::find($id);

        return view('users.view', ['user' => $user]);
    }

    public function update(UserUpdateValidation $request, $id)
    {
//        dd($request);
        $user = User::find($id);
//        dd($user);
        $pass = $request->input('password');
        $data = $request->all();

        $adress = Adress::firstOrNew(['city' => $request->input('city'), 'street' => $request->input('street'), 'building' => $request->input('building'), 'appartment' => $request->input('appartment')]);
        $adress->save();

        if (trim($request->input('password')) == '') {
            $data = $request->except('password');
        }
//
//        if($request->hasFile('avatar')){
//            $avatar = $request->file('avatar');
//            $filename = time() . '.' . $avatar->getClientOriginalExtension();
//
//            $directoryPath = '/uploads/avatars/';// . $user->id;
////            if(File::isDirectory(public_path($directoryPath))){
////                //Perform storing
////                dd('sdfsdf');
////            } else {
////                File::makeDirectory(public_path($directoryPath));
////                dd(public_path($directoryPath));
////                //Perform storing
////            }
////            Image::make($avatar)->resize(300)->save( public_path($directoryPath . '/' . $filename ) );
//
//            //Resize image here
//            $thumbnailpath = public_path($directoryPath . '/' . $filename );
//            $img = Image::make($thumbnailpath);
//
//            $img->resize(null, 300, function ($constraint) {
//                $constraint->aspectRatio();
//            });
//
//            $img->save($thumbnailpath);
//
////            $user = Auth::user();
//            $user->avatar = $thumbnailpath;
//        }

        if ($user->role_id >= $request->input('role_id')) {
            $user->fill($data);
            $user->adress_id = $adress->id;
            if (trim($request->input('password')) != '') {
                $user->password = Hash::make($pass);
            }
            $user->save();
        }

//        return redirect()->route('users.index');
    }

    public function parentScoreAdd($id) {
        $user = User::find($id);
        $user->score++;
        $user->save();

        return redirect()->back();
    }

    public function parentScoreRemove($id) {
        $user = User::find($id);
        $user->score--;
        $user->save();

        return redirect()->back();
    }

    public function resetscore($id) {
        $user = User::find($id);

        $user->score = 0;
        $user->save();

        return redirect()->back();
    }

    public function parentScoreToChild($id, $score) {
        $user = User::find($id);

        if (Auth::user()->score > 0) {
            $parent = Auth::user();
            $parent->score--;
            $parent->save();

            $user->score += $score;
            $user->save();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        User::find($id)->delete($id);

        return redirect()->route('users.index');
    }

    public function editCurrent()
    {
        $user = User::find(Auth::id());

        return view('users.profile_edit', ['user' => $user]);
    }

    public function updateCurrent(UserUpdateValidation $request)
    {
//        dd($request);
        $user = Auth::user();
        $pass = $request->input('password');

        $request->merge([
                'sms_notification' => $request->has('sms_notification') ? true : false,
                'push_notification' => $request->has('push_notification') ? true : false,
                'email_notification' => $request->has('email_notification') ? true : false,
            ]);

        $data = $request->all();



        $adress = Adress::firstOrNew(['city' => $request->input('city'), 'street' => $request->input('street'), 'building' => $request->input('building'), 'appartment' => $request->input('appartment')]);
        $adress->save();

        if (trim($request->input('password')) == '') {
            $data = $request->except('password', 'email', 'phone');
        } else {
            $data = $request->except('when_passport', 'birthday');
        }
//        dd($data);

//        if ($user->role_id >= $request->input('role_id') || $user->role_id <= 2) {
            $user->fill($data);
            $user->adress_id = $adress->id;
            $user->birthday = Carbon::parse($request->input('birthday'))->format('Y-m-d');
            $user->when_passport = Carbon::parse($request->input('when_passport'))->format('Y-m-d');
//            $user->passport_when = Carbon::createFromFormat('D.M.YY', $request->input('passport_when'));
            if (trim($request->input('password')) != '') {
                $user->password = Hash::make($pass);
            }

            if($request->hasFile('avatar')){
                $avatar = $request->file('avatar');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save( public_path('/uploads/avatars/' . $filename ) );

                $user->avatar = $filename;
            }

//            dd(Carbon::create($request->birthday)->diff(Carbon::now()));
//            if (Carbon::create($request->birthday)->diff(Carbon::now())->days < 4000)
//                return redirect()->back()->withErrors(['birthday' => 'Вы слишком молоды.']);
            $user->save();
//            dd($user);
//        }

        return redirect()->route('admin');
    }

}
