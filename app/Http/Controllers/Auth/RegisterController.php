<?php

namespace App\Http\Controllers\Auth;

use App\Adress;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginValidation;
use App\Mail\Authorization;
use App\Providers\RouteServiceProvider;
use App\User;
use http\Env\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:20', 'unique:users'],
            'city' => ['required', 'string', 'max:40'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try {
            Mail::to($data['email'])->send(new Authorization($data));
        } catch (Exception $e) {
            Session::put('error', 'Не было отправлено письмо на почту');
        }
//        dd(Session::get('error'));
        $adress = Adress::firstOrNew(['city' => $data['city'], 'street' => $data['street'], 'building' => $data['building'], 'appartment' => $data['appartment']]);
        $adress->save();
//        dd($adress);
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'adress_id' => $adress->id,
            'api_token' => Str::random(80),
        ]);
    }

    public function register_api(UserLoginValidation $request)
    {

        $data = $request->toArray();
//        $adress = Adress::firstOrNew(['city' => $data['city'], 'street' => $data['street'], 'building' => $data['building'], 'appartment' => $data['appartment']]);
//        $adress->save();
        dd($data);
        if (User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
//            'adress_id' => $adress->id,
            'api_token' => Str::random(80),
        ])) {
            return response('1');
        } else {
            return response('0');
        }


//        return response('failed');//$this->sendFailedLoginResponse($request);
    }
}
