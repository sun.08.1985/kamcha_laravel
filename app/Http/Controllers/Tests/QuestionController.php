<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestValidation;
use App\Http\Requests\VariableValidation;
use App\Test;
use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $questions = DB::table('questions')->paginate(15);
        return view('questions.index', ['questions' => $questions]);
    }

    public function create($test_id)
    {
        $test = Test::find($test_id);
        return view('questions.create', ['test' => $test]);
    }

    public function store(TestValidation $request)
    {
        $question = Question::create($request->all());

        return redirect()->route('answers.create', $question->id);
    }

    public function edit($id)
    {
        $question = Question::find($id);

        return view('questions.edit', ['question' => $question]);
    }

    public function show($id)
    {
        $question = Question::find($id);

        return view('questions.view', ['question' => $question]);
    }

    public function update(TestValidation $request, $id)
    {
        $question = Question::find($id);
        $question->fill($request->all());
        $question->save();

        return redirect()->route('questions.edit', $question);
    }

    public function destroy($id)
    {
        Question::find($id)->delete($id);

        return redirect()->back();
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }

}
