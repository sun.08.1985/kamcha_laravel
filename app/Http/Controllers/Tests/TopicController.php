<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopicValidation;
use Illuminate\Http\Request;
use App\Topic;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    public function index()
    {
        $topics = DB::table('topics')->paginate(15);
        return view('topics.index', ['topics' => $topics]);
    }

    public function create()
    {
        return view('topics.create');
    }

    public function store(TopicValidation $request)
    {
        $topic = Topic::create($request->all());

        return redirect()->route('topics.edit', $topic);
    }

    public function edit($id)
    {
        $topic = Topic::find($id);

        return view('topics.edit', ['topic' => $topic]);
    }

    public function show($id)
    {
        $topic = Topic::find($id);

        return view('topics.view', ['topic' => $topic]);
    }

    public function update(TopicValidation $request, $id)
    {
        $topic = Topic::find($id);

        $topic->fill($request->all());
        $topic->save();

        return redirect()->route('topics.index');
    }

    public function destroy($id)
    {
        Topic::find($id)->delete($id);

        return redirect()->route('topics.index');
    }


}
