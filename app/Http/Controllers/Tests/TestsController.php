<?php

namespace App\Http\Controllers;

use App\Test;
use App\Topic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\TestValidation;
use Illuminate\Support\Facades\DB;

class TestsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->hasRoles(['super_user', 'editor', 'author', 'super_admin', 'admin'])) {
            $tests = DB::table('tests')->orderBy('class')->paginate(15);
        } else {
            $tests = DB::table('tests')->where('class', $user->class)->paginate(15);
//                $tests_ids = json_decode($user->current_test_id);
//
////            $tests = Test::all()->where('id');
//                $tests = collect([]);
//
//                if (!empty($tests_ids))
//                    foreach ($tests_ids as $tests_id) {
//                        $tests = $tests->merge(Test::All()->where('id', $tests_id));
//                    }
        }

        return view('tests.index', ['tests' => $tests]);
    }

    public function create()
    {
        $topics = Topic::all();
        return view('tests.create', ['topics' => $topics]);
    }

    public function store(TestValidation $request)
    {
        $test = Test::create($request->all());
        //$test->questions()->create($request->only('question_id'));

        return redirect()->route('questions.create', $test->id);
    }

    public function edit($id)
    {
        $test = Test::find($id);
        $topics = Topic::all();
        return view('tests.edit', ['test' => $test, 'topics' => $topics]);
    }

    public function show($id)
    {
        $test = Test::find($id);

        return view('tests.view', ['test' => $test]);
    }

    public function update(TestValidation $request, $id)
    {
        $test = Test::find($id);
        $test->fill($request->all());
        $test->save();

        return redirect()->route('tests.edit', $test);
    }

    public function destroy($id)
    {
        Test::find($id)->delete($id);

        return redirect()->route('tests.index');
    }

}
