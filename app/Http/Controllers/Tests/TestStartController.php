<?php

namespace App\Http\Controllers;

use Session;
use App\Answer;
use App\Question;
use App\Topic;
use Illuminate\Http\Request;
use App\Test;
use App\User;
use App\TestCompleted;
use App\Http\Requests\TestStartValidation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TestStartController extends Controller
{

    public function list()
    {
        $tests = Test::all();
        $topics = Topic::all();

        return view('tests.list', ['tests' => $tests, 'topics' => $topics]);
    }

    public function execute($id)
    {
        $test = Test::find($id);
        return view('tests.execute', ['test' => $test]);
    }

    public function control(TestStartValidation $request)
    {

//        $valid = Validator::make($request->all(),[
//            '_check' => 'reinclusion',
//        ]);
//        if($valid->fails()){
//            return redirect()->route('admin');
//        }

//        dd($request);
        $user = User::find(Auth::id());
        $requests = $request->request->all();
        $test_id = null;
        $re = '/q(\d+)a(\d+)/m';
        $count_true = 0;
        $count_false = 0;
        $answers_correct_count = 0;
        $answers_result = [];
        $answers_correct = [];
        $travel_time = 0;
//        dd($answers);
        foreach ($requests as $value => $key) {
            if ( ($value !== '_token') && ($value !== 'test_id') && ($value !== 'travel_time') ) {

                preg_match_all($re, $value, $matches, PREG_SET_ORDER, 0);

//                dd($value);
                if (empty($matches)) return ( redirect()->back() );

                $question_id = $matches[0][1];
                $answer_id = $matches[0][2];
                $question = Question::find($question_id);
                $answer = Answer::find($answer_id);

                array_push($answers_result, (int)$answer_id);

//dd($question->answers);
//===============================================================================
//                foreach ($test->questions as $q) {
//                    foreach ($q->answers as $ans) {
//
//                        if ($ans->correct && $ans->id == $answer_id) echo $ans;
//                        if ($ans->correct) $count_true++; else $count_false--;
////                        echo $ans->correct.'<br>';
////                        array_push($add2, $quest->answers->where('correct', 'on'));
//                    }
//                }
//================================================================================

            } else {
                 if ($value === 'test_id') {
    //                 dd($key);
                     $add = [];
                     $add2 = [];
                     $test_id = $key;
                     if ($user->current_test_id != $test_id) redirect(route('exams.list'));
                     $test = Test::find($test_id);
                     $questions = $test->questions;

    //
    //                 foreach ($questions as $q) {
    //                     $quest = Question::find($q->id);
    //                     array_push($add, $quest->answers->where('correct', 'on'));
    //                 }

                 }
                 if ($value === 'travel_time') {
                     $travel_time = $key;
//                     dd($key);
                 }
            }
        }

        foreach ($test->questions as $q) {
            foreach ($q->answers->where('correct', '!=', null) as $ans) { //->where('correct', '!=', null)
                $answers_correct_count ++;
                array_push($answers_correct, $ans->id);
//                if ($ans->correct && $ans->id == $answer_id) echo $ans;
//                if ($ans->correct && key_exists($ans->id, $answers_result)) $count_true++; else $count_false++;
//                        echo $ans->correct.'<br>';
//                        array_push($add2, $quest->answers->where('correct', 'on'));
            }
        }
//dd($answers_result);
//dd($answers_correct);


//        sort($answers_correct);
//        sort($answers_result);
//        dd($answers_result);

//        dd($answers_correct);
//dd($answers_result);
//dd($answers_correct);
//
//dd(array_intersect($answers_correct, $answers_result));
        $count_true = count(array_intersect($answers_correct, $answers_result));
        $count_false = count($answers_result) - $count_true;
        if ($count_true < 3) $count_true = 2;
//        dd($answers_correct);
//        echo key_exists($ans->id, $answers_result);


// ЗАПИСЬ БАЛЛОВ
//        $user->score += $count_true-$count_false;
//        $user->current_test_id = json_encode(["0", "1", "2", "3"]);
//        $user->save();
        $tests_ids = json_decode($user->current_test_id);
//        dd($tests_ids);
        if (is_array($tests_ids)) {
//            dd(array_search($test_id, $tests_ids));
            if (in_array($test_id, $tests_ids)) {
                array_splice($tests_ids, array_search($test_id, $tests_ids), 1);
//                dd($tests_ids);
//                $tests_ids[] = $test_id;
            }
        }
        else {
//            dd($tests_ids);
            $tests_ids = [];
//            dd($tests_ids);
        }

        $user->current_test_id = json_encode($tests_ids);
        $user->save();
//        dd($tests_ids);
//
        $data = [];
//        dd($user->parentsFromFamily($user->family_id));

//        if (env('APP_SMS')) {
//            foreach ($user->parentsFromFamily($user->family_id) as $parent) {
//
//                $url = env('APP_SMS', 'https://smsc.ru/sys/send.php');
//                $ch = curl_init($url);
//                $data = array(
//                    'login' => env('APP_LOGIN', 'login'),
//                    'psw' => env('APP_PASS', 'password'),
//                    'phones' => preg_replace('/\D+/', '', $parent->phone),
//                    'mes' => $user->name . ' прошел опрос по ' . $test->topic->name . '. Тему опроса и оценку Вы можете найти в Вашем личном кабинете на сайте ' . route('home')
//                );
//                curl_setopt($ch, CURLOPT_POST, true);
//                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                $result = curl_exec($ch);
//                curl_close($ch);
//
////            echo 'Отправлено на тел.:'. preg_replace('/\D+/', '', $parent->phone) .'. ответ: '.$result.'<br>'.$data['mes'].'<br>';
//            }
//        }

        $res = TestCompleted::create([
            'test_id' => $test_id,
            'user_id' => $user->id,
            'correct_answer' => $count_true,
            'incorrect_answer' => $count_false,
            'estimation' => $test->time_estimation,
            'travel_time' => $travel_time,
            'answer_array' => json_encode($answers_result)
        ]);
//dd($res);
        return redirect()->route('exams.result', $res);
//        return view('tests.result', ['test' => $test, 'count_true' => $count_true, 'count_false' => $count_false]);
//                dd($add2);
//                echo $value.'-Вопрос-'.$question.'-Ответ-'.$answer.'<br>';
//            dd($arr);
//        $test = Test::find($id);
//        return view('tests.execute', ['test' => $test]);
    }

}
