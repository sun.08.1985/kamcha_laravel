<?php

    namespace App\Http\Controllers;

    use App\Variable;
    use App\Test;
    use Illuminate\Http\Request;
    use App\Http\Requests\VariableValidation;
    class VariablesController extends Controller
    {

        public function __construct()
        {
            $this->middleware('auth');
        }

        public function index()
        {
            $variables = Variable::all();

            return view('variables.index', ['variables' => $variables]);
        }

        public function create($test_id)
        {
            $test = Test::find($test_id);
            return view('variables.create', ['test' => $test]);
        }

        public function store(VariableValidation $request)
        {
//            dd($request->all(), $request->test()->id);
            $variable = Variable::create($request->all());
//            $variable->correct = ($request->only('correct') == 'on')?1:0;
//            $variable->test_id = $id;
            return redirect()->route('tests.index');
        }

        public function edit($id)
        {
            $variable = Variable::find($id);

            return view('variables.edit', ['variable' => $variable]);
        }

        public function show($id)
        {
            $variable = Variable::find($id);

            return view('variables.view', ['variable' => $variable]);
        }

        public function update(VariableValidation $request, $id)
        {
            $variable = Variable::find($id);
            $variable->fill($request->all());
            $variable->save();
//            dd($request->all());
//            $variable = Variable::create($request->all());
//            $variable->test()->create($request->only('test_id'));

            return redirect()->route('variables.show', $id);
        }

        public function destroy($id)
        {
            Variable::find($id)->delete($id);

            return redirect()->route('variables.index');
        }
    }
