<?php

namespace App\Http\Controllers;

use App\Http\Requests\VariableValidation;
use App\Question;
use App\Test;
use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $answers = DB::table('answers')->paginate(15);

        return view('answers.index', ['answers' => $answers]);
    }

    public function create($question_id)
    {
        $question = Question::find($question_id);

        return view('answers.create', ['question' => $question]);
    }

    public function store(VariableValidation $request)
    {
//dd($request);
        $title = $request->input('title');
        $description = $request->input('description');

//        $correct = preg_replace('/\D+/', '', $request->only('correct'));
        $correct = $request->input('correct');
        $question_id = $request->input('question_id');
        $user_id = $request->input('user_id');

        for($count = 0; $count < count($title); $count++)
        {
//            dd($correct);
            if ($correct == $count) $correct_flag = 'on'; else $correct_flag = null;
            $data = array(
                'title' => $title[$count],
                'description'  => $description[$count],
                'correct'  => $correct_flag,
                'question_id'  => $question_id,
                'user_id'  => $user_id,
            );

            if ($title[$count] != null)
                $insert_answer[] = $data;
        }
        Answer::insert($insert_answer);

//        dd($request);
//        foreach ()
//        Answer::create($request->all());
//
//        if ($request->has('next'))
//            return redirect()->route('answers.create', $request->question_id);
//        else
        return redirect()->route('questions.edit', $request->question_id);
    }

    public function edit($id)
    {
        $answer = Answer::find($id);

        return view('answers.edit', ['answer' => $answer]);
    }

    public function show($id)
    {
        $answer = Answer::find($id);

        return view('answers.view', ['answer' => $answer]);
    }

    public function update(VariableValidation $request, $id)
    {
        $correct = 'on';
        if (!$request->has('correct'))
            $correct = null;

        $answer = Answer::find($id);

        $answer->fill($request->all());
        $answer->correct = $correct;

        $answer->save();

        return redirect()->route('questions.edit', $request->question_id);
    }

    public function destroy($id)
    {
        Answer::find($id)->delete($id);

        return redirect()->back();
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

//    public function test()
//    {
//        return $this->belongsTo('App\Test');
//    }

}
