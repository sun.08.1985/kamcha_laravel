<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AvaibleTest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasRoles(['admin', 'super_admin']))
            return $next($request);

        $test_ids = json_decode(Auth::user()->current_test_id);
//dd($request->route('id'));
        if (is_array($test_ids))
            foreach($test_ids as $test_id) {
                // Check if user has the role This check will depend on how your roles are set up
                if($test_id == $request->route('id'))
                    return $next($request);
            }
        else
            $test_ids = [];

        return redirect('admin');
    }
}
