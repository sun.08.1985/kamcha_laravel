<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DebugAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if (!Auth::guest())
            if (!Auth::guest() && Auth::user()->hasRole('super_admin'))
            {
                Config::set('app.debug', true);
            } else
            {
//                Config::set('app.debug', false);
            }
        return $next($request);
    }
}
