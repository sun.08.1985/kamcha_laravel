<?php
    namespace App\Http\Policies;

    use App\Entity\Comment;
    use Illuminate\Support\Facades\Auth;
    use tizis\laraComments\Policies\CommentPolicy as CommentPolicyPackage;

    class CommentPolicy extends CommentPolicyPackage
    {
        // Переписываем проверку прав на удаление комментария
        public function delete($user, $comment): bool
        {
            // Теперь всего будет возвращаться true т.е. любой пользователь может удалить любой комментарий.
            if ( $comment->commenter_id == Auth::user()->id )
                return true;
            elseif ( Auth::user()->isAdmin() )
                return true;
            else
                return false;
        }
    }
