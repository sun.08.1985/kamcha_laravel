<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title', 'description', 'keywords', 'slug', 'header', 'content', 'footer', 'menu_header', 'menu_footer', 'order'];
}
