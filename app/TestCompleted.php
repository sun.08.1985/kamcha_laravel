<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TestCompleted extends Model
{
    protected $fillable = ['user_id', 'test_id', 'estimation', 'travel_time', 'correct_answer', 'incorrect_answer', 'answer_array'];
    protected $table = 'test_complited';

    public function jsonArray()
    {
//        dd($this);
//        $empty = [];
//        if (!empty(json_decode($this->answer_array)))
//        $test = Test::find($id);
        $answer_array = $this->answer_array;


//        if ($answer_array != null)
            $answer_array = collect(json_decode($answer_array));

//        else
//            $answer_array = [];
//        dd($answer_array);
        return $answer_array;
//        else
//            return $empty;
//        return $this;
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function test_user()
    {
//        $tests = TestCompleted::all()->where('user_id', Auth::id());
//        Auth::childs
        $tests = Auth::user()->tests_complited;
        return $tests;
    }

}
