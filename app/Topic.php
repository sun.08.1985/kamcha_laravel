<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Topic extends Model
{
    protected $fillable = [
        'name'
    ];

    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    public function tests_pupil($id = null)
    {
        if ($id === null)
            $user = Auth::user();
        else
            $user = User::find($id);

        $tests = collect([]);
//        dd($user);
        if ($user->hasRole('user')) {
            $tests_ids = json_decode($user->current_test_id);

//            $tests = Test::all()->where('id');

            foreach ($tests_ids as $tests_id) {
                $tests = $tests->merge(Test::All()->where('id', $tests_id));
            }
        } elseif($user->hasRoles('admin', 'super_admin')) {
            $tests = Test::all();
        }
//        dd($tests);
        return $tests;
    }

}
