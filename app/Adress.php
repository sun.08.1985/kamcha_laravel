<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adress extends Model
{
    protected $table = 'adresses';
    protected $fillable = ['city', 'street', 'building', 'appartment'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

}
