<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Inani\Larapoll\Traits\Voter;
use tizis\laraComments\Traits\Commenter;

class User extends Authenticatable
{
    use Notifiable;
    use Voter;
    use Commenter;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'score', 'current_test_id', 'phone', 'adress_id', 'api_token',
        'sms_notification', 'email_notification', 'push_notification',
        'where_passport', 'when_passport', 'serial_passport', 'number_passport',
        'birthday', 'firstname', 'lastname', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        if($this->role_id >= 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function hasRole($role) {
        if($this->role->type === $role)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function hasRoles(array $roles) {
        $result = false;
        foreach ($roles as $role) {
            if($this->role->type === $role)
            {
                $result = true;
            }
        }
        return $result;
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function adress()
    {
        return $this->belongsTo('App\Adress');
    }

    public function tests_complited() //Завершенные опросы
    {
        return $this->hasMany('App\TestCompleted');
    }

    public function parentsFromFamily($id) //Все родители семьи
    {
        $parent = $this->family($id)->where('role_id', 2);
        return $parent;
    }

    public function childsFromFamily($id) //Все дети семьи
    {
        $childs = $this->family($id)->where('role_id', 1);
        return $childs;
    }

    public function family($id = null) //Все члены семьи
    {
        if ($id === null) $id = Auth::user()->family_id;

        $users = DB::table('users')->where('family_id', $id)->orWhere('id', $id)->get();
        return $users;
    }

    static public function childs() //Все дети
    {
        $id = Auth::user()->family_id;

//        $users = DB::table('users')->where('family_id', $id)->orWhere('id', $id)->get();
        $users = User::All()->where('id', $id);
        $users = $users->merge(User::All()->where('family_id', Auth::user()->id));//User::All()->where('family_id', Auth::user()->id));

        if (Auth::user()->family_id != null) {
            $users = $users->merge(User::All()->where('family_id', Auth::user()->family_id));
            $users = $users->merge(User::All()->where('id', Auth::user()->family_id));
        }
        return $users;
    }

    public function generateToken()
    {
        $this->api_token = Str::random(80);
        $this->save();

        return $this->api_token;
    }
//$users = User::All()->where('id', $id);
//$users = $users->merge(User::All()->where('family_id', Auth::user()->id));//User::All()->where('family_id', Auth::user()->id));
//
//if (Auth::user()->family_id != null) {
//$users = $users->merge(User::All()->where('family_id', Auth::user()->family_id));
//$users = $users->merge(User::All()->where('id', Auth::user()->family_id));
//}
}
