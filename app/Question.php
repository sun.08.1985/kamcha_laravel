<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['title', 'description', 'test_id'];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function answersRandom() //disabled random
    {
        return $this->hasMany('App\Answer');//->inRandomOrder();
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }
}
