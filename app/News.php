<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use tizis\laraComments\Contracts\ICommentable;
use tizis\laraComments\Traits\Commentable;
use App\Category;

class News extends Model implements ICommentable {
    use Commentable;
    protected $fillable = ['title', 'description', 'keywords', 'slug', 'content', 'menu_news', 'order', 'category_id', 'image_url'];

    public function category()
    {
        return $this->belongsTo(\App\Category::class, 'category_id');
    }
}
