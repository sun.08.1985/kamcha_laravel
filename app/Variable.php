<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $fillable = ['title', 'description', 'test_id', 'correct'];

    public function test()
    {
        return $this->belongsTo('App\Test');
    }
}
