<?php

namespace App\Helpers;

use Inani\Larapoll\Guest;
use Inani\Larapoll\Helpers\PollWriter;
use Inani\Larapoll\Poll;

class PollWriterRemod extends PollWriter
{
    /**
     * Draw a Poll
     *
     * @param Poll $poll
     * @return string
     */
    public function draw($poll)
    {

        if(is_int($poll)){
            $poll = Poll::findOrFail($poll);
        }

        if(!$poll instanceof Poll){
            throw new \InvalidArgumentException("The argument must be an integer or an instance of Poll");
        }

        if ($poll->isComingSoon()) {
            return 'Скоро будет доступно';
        }

        if (!$poll->showResultsEnabled()) {
            return null;//'Спасибо за ваш голос';
        }


        $voter = $poll->canGuestVote() ? new Guest(request()) : auth(config('larapoll_config.admin_guard'))->user();

        if (is_null($voter) || $voter->hasVoted($poll->id) || $poll->isLocked()) {
            return $this->drawResult($poll);
        }

        if ($poll->isRadio()) {
            return $this->drawRadio($poll);
        }

        return $this->drawCheckbox($poll);
    }
}
