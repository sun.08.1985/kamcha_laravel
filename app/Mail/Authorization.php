<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class Authorization extends Mailable
{
    use Queueable, SerializesModels;

    protected $user_name, $user_email, $user_password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $user)
    {
        $this->user_name = $user['name'];
        $this->user_email = $user['email'];
        $this->user_password = $user['password'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS', 'hello@example.com'))
            ->markdown('emails.auth')->with([
                'name' => $this->user_name,
                'email' => $this->user_email,
                'password' => $this->user_password
            ]);
    }
}
