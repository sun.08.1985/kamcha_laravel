<?php

namespace App\Providers;

use App\Helpers\PollWriterRemod;
use App\Http\Requests\TestControlValidation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->loadViewsFrom(__DIR__.'/../../resources/views/polls', 'larapoll');
    }

    /**
     * Register the poll writer instance.
     *
     * @return void
     */
    protected function registerPollWriterRemod()
    {
        $this->app->singleton('pollwritterremod', function ($app) {
            return new PollWriterRemod();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerPollWriterRemod();
        Schema::defaultStringLength(191);
//        Validator::resolver(function($translator, $data, $rules, $messages)
//        {
//            return new TestControlValidation($translator, $data, $rules, $messages);
//        });
//
//        Validator::extend('date_difference', function ($attribute, $value, $parameters, $validator) {
//            $firstDate = Carbon::parse($parameters[0]);
//            $secondDate = Carbon::parse($parameters[1]);
//            $minDifference = (int)$parameters[2];
//            if($firstDate->diffInMonths($secondDate) < $minDifference)
//                return false;
//            return true;
//        });
    }

}
