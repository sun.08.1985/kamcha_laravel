<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = ['title', 'description', 'time_estimation', 'topic_id', 'class'];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function questions_random_limit() //random disabled
    {
//        $questions = $this->hasMany('App\Question');
//        $questions->orderBy('id', 'ASC')
        $variant_question = 5;
        $count_variant = (int)$this->hasMany('App\Question')->count() / $variant_question;
        return $this->hasMany('App\Question')->offset(rand(0, $count_variant) * $variant_question)->limit($variant_question); //inRandomOrder()->
    }

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    public function hasPupil($id_user, $id_test)
    {
        $user = User::find($id_user);

        if ($user->current_test_id === null) return false;
        $tests_ids = json_decode($user->current_test_id);

        foreach ($tests_ids as $tests_id) {
//            echo $id_test;
            if ($tests_id == $id_test)
                return true;
        }

    }

}
