<?php
return [
    'admin_auth' => env('LARAPOLL_ADMIN_AUTH_MIDDLEWARE', 'web'),
    'admin_guard' => env('LARAPOLL_ADMIN_AUTH_GUARD', 'auth'),
    'pagination' => env('LARAPOLL_PAGINATION', 15),
    'prefix' => env('LARAPOLL_PREFIX', 'admin'),
    'results' => 'polls.stubs.results',
    'radio' => 'polls.stubs.radio',
    'checkbox' => 'polls.stubs.checkbox',
    'user_model' => App\User::class,
];
