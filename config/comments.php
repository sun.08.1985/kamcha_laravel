<?php

return [
    // The model which creates the comments aka the User model
    'models' => [
        /**
         * Commenter model
         */
        'commenter' => \App\User::class,
        /**
         * Comment model
         */
        'comment' => \App\Comment::class
    ],
    'ui' => 'bootstrap4',
    'purifier' => [
        'HTML_Allowed' => '',
    ],
    'route' => [
        'root' => 'api',
        'group' => 'comments',
        'middleware' => 'censure'
    ],
    'policy_prefix' => 'comments',
    'testing' => [
        'seeding' => [
            'commentable' => '\App\News',
            'commenter' => '\App\User'
        ]
    ],
    /**
     * Only for API
     *
     * @example ['get']['preprocessor']['user'] => App\UseCases\CommentPreprocessor\User::class
     */
    'api' => [
        'get' => [
            'preprocessor' => [
                'user' => null,
                'comment' => null
            ]
        ]
    ]
];
