$(document).ready(function () {
    tinymce.init(
        {
            selector:'textarea',
            width: '100%',
            height: 300,
            images_upload_base_path: 'upload',
            images_upload_credentials: true,
            plugins: 'print preview fullpage paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            // imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true

        });
    var count = 0;
    var add_more = $("body").on('click', '.add-more', function () {
        count++;
        var random = count;
        $(".copy-fields").find('[id^="collapseDescription"]').attr('id', 'collapseDescription' + random);
        $(".copy-fields").find('[aria-controls^="collapseDescription"]').attr('data-target', '#collapseDescription' + random);
        $(".copy-fields").find('[aria-controls^="collapseDescription"]').attr('aria-controls', '#collapseDescription' + random);
        $(".copy-fields").find('[name="correct"]').attr('value', random);
        $(".copy-fields").find('[name="correct"]').removeClass('copy');
        // alert(random);
        var html = $(".copy-fields").html();
        $(this).parents(".after-add-more").after(html);
        tinymce.init(
            {
                selector:'textarea',
                width: '100%',
                height: 300
            });
    });

    var remove_more = $("body").on('click', '.remove', function () {
        if (confirm('Вы уверены что хотите удалить?'))
            $(this).parents(".control-group").remove();
    });

    $('form').one('submit', function (e) {
        e.preventDefault();
        var count = 0;
        $('input[type="radio"]:not(".copy")').each(function (e) {
            $(this).attr('value', count);
            count++;
        });
        $(this).submit();
    });
});
