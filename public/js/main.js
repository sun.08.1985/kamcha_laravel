$(document).ready(function() {

    /* ======= infolabel close ======= */
    $('.infolabel__box_close').on('click', function(e){
        event.preventDefault();
        var info = $(this).parent('.infolabel__box').parent('.infolabel__box_animate');
        info.animate({height: "hide"}, 500);
    });

    $('.modal_form .infolabel__box_close').on('click', function(e){
        event.preventDefault();
        var info = $(this).parent('.modal_notification_item');
        info.animate({height: "hide"}, 500);
    });


    /* ======= registration pas ======= */
    $('.modal_registration_pas_eye').on('click', function(e){
        event.preventDefault();
        if ($('.modal_registration_pas').attr('type') === 'password') {
            $('.modal_registration_pas').attr('type', 'text');
        } else {
            $('.modal_registration_pas').attr('type', 'password');
        }
    });




    /* ======= poll slider ======= */

    $('.sidebar__poll_slick').slick({
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        slidesToScroll: 1
    });

    $('.sidebar__poll_nav_prev').on('click', function(){
        $('.slick-prev').click();
    });
    $('.sidebar__poll_nav_next').on('click', function(){
        $('.slick-next').click();
    });





    


    $('.header__mobile_searsh').on('click', function(){
        $('body').click(function (e){
            var x = e.target;
            if (x.getAttribute('class') !== 'header__mobile_searsh_img') {
                $('.header__mobile_up1').removeClass('show');
                $('.header__mobile_searsh').attr('aria-expanded', 'false').addClass('collapsed');
            }
        });
    });

    $('.header__mobile_menu').on('click', function(){
        $('body').click(function (e){
            var x = e.target;
            if (x.getAttribute('class') !== 'mobile_burger') {
                $('.header__mobile_up2').removeClass('show');
                $('.header__mobile_menu').attr('aria-expanded', 'false').addClass('collapsed');
            }
        });
    });

    
    


    

    /* ======= Burger animation ======= */

    $('.header__mobile_menu').on('click', function(){
        $(this).toggleClass('header__mobile_menu_active');
    });



    /* ======= nav-tabs active ======= */

    // $('.choice .nav-link').on('click', function(){
    //     if ($(this).hasClass('active')===false) {
    //         $('.choice .active-li').removeClass('active-li');
    //         $(this).parent().addClass('active-li');

    //     }
        
    // });

    /* ======= nav-tabs active ======= */

    // $('.product-card .nav-link').on('click', function(){
    //     if ($(this).hasClass('active')===false) {
    //         $('.product-card .active-li').removeClass('active-li');
    //         $(this).parent().addClass('active-li');

    //     }
        
    // });







});






//анимация при прокрутке
$(document).ready(function(){
    $('.moveopacity').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < heightOfWindow) {
            $(this).addClass('fadeOpacity');
        }
    });

});

$(window).scroll(function () {
    $('.moveopacity').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < topOfWindow + 0.9*heightOfWindow) {
            $(this).addClass('fadeOpacity');
        }
    });
});
$(document).ready(function(){
    $('.moveleft').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < heightOfWindow) {
            $(this).addClass('fadeLeft');
        }
    });

});

$(window).scroll(function () {
    $('.moveleft').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < topOfWindow + 0.9*heightOfWindow) {
            $(this).addClass('fadeLeft');
        }
    });
});
$(document).ready(function(){
    $('.moveright').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < heightOfWindow) {
            $(this).addClass('fadeRight');
        }
    });

});

$(window).scroll(function () {
    $('.moveright').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var heightOfWindow = window.innerHeight;
        if (imagePos < topOfWindow + 0.9*heightOfWindow) {
            $(this).addClass('fadeRight');
        }
    });
});




    

    
    