<?php

    $prod = 'kamcha_laravel';
    $dev = 'v2';

    function getSubDomain($data) {

        $tmp = explode('.', $data);
        $tmp = array_slice($tmp, 0, -2);
        $str = implode(".", $tmp);

        return $str;
    }

    if ( getSubDomain($_SERVER['HTTP_HOST']) == 'v2' ) {
        if ( exec('cd ~/domains/v2.ikrkam.ru && git pull origin v2') )
            echo 'Деплой успешно прошел V2! <br>'.exec('cd ~/domains/v2 && git show');
    } else {
        if ( exec('cd ~/domains/kamcha_laravel && git pull origin master') )
            echo 'Деплой успешно прошел! <br>'.exec('cd ~/domains/kamcha_laravel && git show');
    }

